<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('/', function () {
    return view('index');
})->name('index');

Route::get('team', function () {
    return view('team');
})->name('team');

Route::get('news', function () {
    return view('news');
})->name('news');

Route::get('connect', function () {
    return view('connect');
})->name('connect');

Route::get('join_us', function () {
    return view('join_us');
})->name('join_us');

Route::get('login', function () {
    return view('login');
})->name('login');

Route::get('privacy-and-policy', function () {
    return view('privacy-and-policy');
})->name('privacy-and-policy');

Route::get('terms-and-conditions', function () {
    return view('terms-and-conditions');
})->name('terms-and-conditions');

//Route::resource('connect-form', 'ConnectController');
//Route::resource('join-form', 'JoinController');
//Route::resource('project-form', 'ProjectDetailsController');
//Route::resource('header-form', 'HeaderFormController');

Route::get('submit-form',[
    "as"=>'submit-form',
    "uses"=>"ConnectController@submitForm"
]);

Route::get('header-form',[
    "as"=>'header-form',
    "uses"=>"HeaderFormController@headerForm"
]);

Route::post('join-us-form',[
    "as"=>'join-us-form',
    "uses"=>"JoinController@joinUsForm"
]);
Route::get('project-form',[
    "as"=>'project-form',
    "uses"=>"ProjectDetailsController@projectForm"
]);

Route::get('projects', function () {
    return view('projects');
})->name('projects');

Route::group(['prefix' => 'project'], function () {

    Route::get('atmos-lucknow', function () {
        return view('project-details');
    })->name('project-details');

    Route::get('rootsat36', function () {
        return view('project-1');
    })->name('project-1');

    Route::get('suntec-city', function () {
        return view('project-2');
    })->name('project-2');

    Route::get('carlow-house', function () {
        return view('project-3');
    })->name('project-3');

    Route::get('ten-ekamai-suites', function () {
        return view('project-4');
    })->name('project-4');

    Route::get('grand-swiss', function () {
        return view('project-5');
    })->name('project-5');

    Route::get('fulcrum-anona', function () {
        return view('project-6');
    })->name('project-6');

    Route::get('kingsland-hotel', function () {
        return view('project-7');
    })->name('project-7');

    Route::get('hotel-du-parc', function () {
        return view('project-8');
    })->name('project-8');

    Route::get('aspira-koh-samui', function () {
        return view('project-9');
    })->name('project-9');

    Route::get('art-patong', function () {
        return view('project-10');
    })->name('project-10');

    Route::get('fulcrum-karin', function () {
        return view('project-11');
    })->name('project-11');

});


Route::group([
    'prefix' => 'atmos'
], function () {

    Route::get('/', function () {
        return view('atmos-lucknow/index');
    })->name('lucknow.index');

    Route::get('project', function () {
        return view('atmos-lucknow/project');
    })->name('lucknow.project');

    Route::get('gallery', function () {
        return view('atmos-lucknow/gallery');
    })->name('lucknow.gallery');

    Route::get('location', function () {
        return view('atmos-lucknow/location');
    })->name('lucknow.location');

    Route::get('amenities', function () {
        return view('atmos-lucknow/amenities');
    })->name('lucknow.amenities');

    Route::get('project-plan', function () {
        return view('atmos-lucknow/project-plan');
    })->name('lucknow.project-plan');

    Route::get('unit-plan', function () {
        return view('atmos-lucknow/unit-plan');
    })->name('lucknow.unit-plan');

    Route::get('news', function () {
        return view('atmos-lucknow/news');
    })->name('lucknow.news');

    Route::get('connect', function () {
        return view('atmos-lucknow/connect');
    })->name('lucknow.connect');

    Route::get('join-us', function () {
        return view('atmos-lucknow/join_us');
    })->name('lucknow.join_us');

});
