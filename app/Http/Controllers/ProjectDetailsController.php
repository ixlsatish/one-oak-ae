<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Validator;

class ProjectDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        echo "store";
        $inputs = $request->all();
        $message = array(
            "first_name.required" => "First Name required",
            "first_name.max" => " Max :max",
            "last_name.required" => " Last Name required",
            "last_name.max" => " Max :max",
            "email.required" => "Email required",
            "mobile_number.required" => "Mobile Number Required",
            "r1.required"=>"Kindly Select One Of The Options"
        );
        $validator = Validator::make($inputs, [
            "first_name" => "required|max:20|alpha",
            "last_name" => "required|max:20|alpha",
            "email" => "required|email",
            "mobile_number" => "required|regex:/[0-9]{10}/",
            "r1"=>"required"

        ], $message);

        if ($validator->fails()) {
//            echo "Validation failed";
            return redirect()->back()->withErrors($validator)->withInput();
        }
        echo "success";
        $filepath="js/video.js";
        $filename="video.js";
//        echo $pathToFile;
//        return response()->download(storage_path("app/public/{$pathToFile}"));

        $headers = array(
            'Content-Type' => 'application/csv',
            'Content-Disposition' => 'attachment; filename=' . $filename,
        );
        return response()->download($filepath,$filename,$headers);

//        echo "success";
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function projectForm(Request $request)
    {
        $inputs = $request->all();
        $message = array(
            "first_name.required" => "Please Enter First Name.",
            "first_name.alpha" => "Please Enter Only Alphabets For First Name.",
            "first_name.max" => " First Name Max :max Characters",
            "first_name.min" => " First Name Min :min Characters",
            "last_name.required" => "Please Enter Last Name.",
            "last_name.alpha" => "Please Enter Only Alphabets For Last Name.",
            "last_name.max" => " Last Name Max :max",
            "last_name.min" => " Last Name Min :min",
            "email.required" => "Please Enter Email.",
            "email.email" => "Please Enter A Valid Email Id.",
            "mobile_number.required" => "Please Enter Mobile Number",
            "mobile_number.digits" => "Please Enter 10 Digit Valid Mobile Number.",
            "radio.required"=>"Kindly Select One Of The Options"
        );
        $validator = Validator::make($inputs, [
            "first_name" => "required|min:1|max:30|alpha",
            "last_name" => "required|min:1|max:30|alpha",
            "email" => "required|email",
            "mobile_number" => "required|digits:10",
            "radio"=>"required"

        ], $message);

        if ($validator->fails()) {
//            echo "Validation failed";
//            return redirect()->back()->withErrors($validator)->withInput();
            return response()->json([
                "code"=>0,
                'message' => $validator->errors()->all(),
                "data"=>[]
            ]);

        }

        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $name = $first_name . " " . $last_name;
        $mobile_code=$request->input('mobile_code');
        $mobile_number=$request->input('mobile_number');
        $con = $mobile_code . " " . $mobile_number; //mobile no
        $email = $request->input('email');
        $remark = $request->input('radio');
        $PWD = "wn9mxO76f34=";
        $UID = "fourqt";
        $f = "m";
        $url = "";
        $project_name = "";
        $src =  $request->input('src');
        $amob = '';
        $city = "";
        $location = "";
        $website = "Website";

        $client = new \GuzzleHttp\Client();

        $sendData = [
//            'UID' => "fourqt",
//            'PWD' => "wn9mxO76f34=",
//            'f' => 'm',
//            'con' => '9028669307',
//            'email' => 'kc@gmail.com',
//            'name' => 'abc',
//            'url'=>'http://ivymarketing.in/1oak/public/',
//            'ch'=>'Website',
//            'location'=>'MUMBAI',
//            'city'=>'mumbai',
//            'src'=>'Website',
//            'Proj'=>'testing project',
//            'Remark'=>'testing remark',
//            'amob'=>''
        ];

//        $url = "http://mirai02.realeasy.in/IVR_Inbound.aspx?UID=fourqt&PWD=wn9mxO76f34=&f=m&con=9028669307&email=kc@gmail.com
//        &name=abc&url=abc.co&location=mum&city=mum&ch=website&src=abc&Projxml&Remark=testing&amob=45";

        $url = "http://mirai02.realeasy.in/IVR_Inbound.aspx?UID=$UID&PWD=$PWD&f=$f&con=$con&email=$email&name=$name&url=$url&Remark=$remark&Proj=$project_name&src=$src&amob=$amob&city=$city&location=$location&ch=$website";

        $res = $client->post($url, [
            'form_params' => $sendData,
        ]);

//        if ($res->getStatusCode() !== 200) {
//            return response()->json([
//                "code" => 0,
//                'message' => "Something Went Wrong please try again",
//                "data" => []
//            ]);
//        }
//        $data = json_decode($res->getBody()->getContents(), true);
//        dd($data);


//            $email = $request->input('email_h');
//            Mail::send('emails.welcome_email', ["name" => $request->input('first_name_h')
//            ], function ($message) use ($email) {
//                $message->subject('Welcome to 1oak.');
//                $message->to($email);
//
//            });

//            $filepath="js/video.js";
//            $filename="video.js";
////        echo $pathToFile;
////        return response()->download(storage_path("app/public/{$pathToFile}"));
//
//            $headers = array(
//                'Content-Type' => 'application/csv',
//                'Content-Disposition' => 'attachment; filename=' . $filename,
//            );
//            response()->download($filepath,$filename,$headers);

            return response()->json([
                "code" => 1,
                'message' => $validator->errors()->first(),
                "data" => []
            ]);

        }



}
