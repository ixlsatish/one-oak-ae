<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Validator;

class HeaderFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        echo "store";
        $inputs = $request->all();
        $message = array(
            "first_name.required" => "First Name required",
            "first_name.max" => " Max :max",
            "last_name.required" => " Last Name required",
            "last_name.max" => " Max :max",
            "email.required" => "Email required",
            "mobile_number.required" => "Mobile Number Required",
            "project.required" => "Select One Project",
            "r1.required" => "Kindly Select One Of The Options"
        );
        $validator = Validator::make($inputs, [
            "first_name" => "required|max:20|alpha",
            "last_name" => "required|max:20|alpha",
            "email" => "required|email",
            "mobile_number" => "required|regex:/[0-9]{10}/",
            "project" => "required",
            "r1" => "required"

        ], $message);

        if ($validator->fails()) {
//            echo "Validation failed";
            return redirect()->back()->withErrors($validator)->withInput();
        }
        echo "success";

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function headerForm(Request $request)
    {

        $inputs = $request->all();

        $message = array(
            "first_name_h.required" => "Please Enter First Name.",
            "first_name_h.alpha" => "Please Enter Only Alphabets For First Name.",
            "first_name_h.max" => " First Name Max :max Characters",
            "first_name_h.min" => " First Name Min :min Characters",
            "last_name_h.required" => "Please Enter Last Name.",
            "last_name_h.alpha" => "Please Enter Only Alphabets For Last Name.",
            "last_name_h.max" => " Last Name Max :max Characters",
            "last_name_h.min" => " Last Name Min :min Characters",
            "email_h.required" => "Please Enter Email.",
            "email_h.email" => "Please Enter A Valid Email Id.",
            "mobile_number_h.required" => "Please Enter Mobile Number",
            "mobile_number_h.digits" => "Please Enter 10 Digit Valid Mobile Number.",
            "project_h.required" => "Please Select A Project",
            "project_h.not_in" => "Please Select A Project",
            "radio_h.required" => "Kindly Select One Of The Options"
        );
        $validator = Validator::make($inputs, [
            "first_name_h" => "required|min:1|max:30|alpha",
            "last_name_h" => "required|min:1|max:30|alpha",
            "email_h" => "required|email",
            "mobile_number_h" => "required|digits:10",
            "project_h" => "required|not_in:Project i am interested in",
            "radio_h" => "required"

        ], $message);

        if ($validator->fails()) {
//            echo "Validation failed";
//            return redirect()->back()->withErrors($validator)->withInput();
            return response()->json([
                "code" => 0,
                'message' => $validator->errors()->all(),
                "data" => []
            ]);

        } else {

            $first_name = $request->input('first_name_h');
            $last_name = $request->input('last_name_h');
            $name = $first_name . " " . $last_name;
            $mobile_code=$request->input('mobile_code_h');
            $mobile_number=$request->input('mobile_number_h');
            $con = $mobile_code . " " . $mobile_number; //mobile no
            $email = $request->input('email_h');
            $remark = $request->input('radio_h');
            $PWD = "wn9mxO76f34=";
            $UID = "fourqt";
            $f = "m";
            $url = "";
            $project_name = $request->input('project_h');
            $src = $request->input('src_h');
            $amob = '';
            $city = "";
            $location = "";
            $website = "Website";

            $client = new \GuzzleHttp\Client();

            $sendData = [
//            'UID' => "fourqt",
//            'PWD' => "wn9mxO76f34=",
//            'f' => 'm',
//            'con' => '9028669307',
//            'email' => 'kc@gmail.com',
//            'name' => 'abc',
//            'url'=>'http://ivymarketing.in/1oak/public/',
//            'ch'=>'Website',
//            'location'=>'MUMBAI',
//            'city'=>'mumbai',
//            'src'=>'Website',
//            'Proj'=>'testing project',
//            'Remark'=>'testing remark',
//            'amob'=>''
            ];

//        $url = "http://mirai02.realeasy.in/IVR_Inbound.aspx?UID=fourqt&PWD=wn9mxO76f34=&f=m&con=9028669307&email=kc@gmail.com
//        &name=abc&url=abc.co&location=mum&city=mum&ch=website&src=abc&Projxml&Remark=testing&amob=45";

            $url = "http://mirai02.realeasy.in/IVR_Inbound.aspx?UID=$UID&PWD=$PWD&f=$f&con=$con&email=$email&name=$name&url=$url&Remark=$remark&Proj=$project_name&src=$src&amob=$amob&city=$city&location=$location&ch=$website";

            $res = $client->post($url, [
                'form_params' => $sendData,
            ]);

            if ($res->getStatusCode() !== 200) {
                return response()->json([
                    "code" => 0,
                    'message' => "Something Went Wrong please try again",
                    "data" => []
                ]);
            }
//        $data = json_decode($res->getBody()->getContents(), true);
//        dd($data);


//            $email = $request->input('email_h');
//            Mail::send('emails.welcome_email', ["name" => $request->input('first_name_h')
//            ], function ($message) use ($email) {
//                $message->subject('Welcome to 1oak.');
//                $message->to($email);
//
//            });

            return response()->json([
                "code" => 1,
                'message' => $validator->errors()->first(),
                "data" => []
            ]);
        }
    }
}
