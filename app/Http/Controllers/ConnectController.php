<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Validator;

class ConnectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        //
        echo "store";
        $inputs = $request->all();
        $message = array(
            "name.required" => "Name required",
            "name.max" => " Max :max",
            "mobile_number.required" => "Mobile required",
            "email.required" => "Email required",
            "message.required" => "Message required",
            "message.max" => "Message max 20"
        );
        $validator = Validator::make($inputs, [
            "name" => "required|max:20|alpha",
            "mobile_number" => "required|numeric|max:10|min:10",
            "email" => "required|email",
            "message" => "required|max:50"
        ], $message);

        if ($validator->fails()) {
//            echo "Validation failed";
//            return redirect()->back()->withErrors($validator)->withInput();
            return response()->json(['error' => $validator->errors()->all()]);

        }
        echo "success";
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function submitForm(Request $request)
    {
        $inputs = $request->all();
        $message = array(
            "name.required" => "Please Enter Full Name.",
            "name.min" => "Full Name Min :min Characters",
            "name.max" => "Full Name Max :max Characters",
            "name.regex" => " Please Enter Valid Full Name.",
            "mobile_number.required" => "Please Enter Mobile Number",
            "mobile_number.digits" => "Please Enter 10 Digit Valid Mobile Number.",
            "message.required" => "Please Enter Message",
            "message.max" => "You Can Enter Max 500 Alphabets.",
        );
        $validator = Validator::make($inputs, [
            "name" => "required|regex:/^[a-zA-Z ]*$/|min:1|max:50",
            "mobile_number" => "required|digits:10",
            "uploaded_file" => "mimetypes:application/pdf|max:2000",
            "message" => "required|max:500"
        ], $message);

        if ($validator->fails()) {
//            echo "Validation failed";
//            return redirect()->back()->withErrors($validator)->withInput();
            return response()->json([
                "code" => 0,
                'message' => $validator->errors()->all(),
                "data" => []
            ]);

        } else {
            $name = $request->input('name');
            $mobile_code=$request->input('mobile_code');
            $mobile_number=$request->input('mobile_number');
            $con = $mobile_code . " " . $mobile_number; //mobile no

//            $email = $request->input('email');
            $email = "";
            $remark = $request->input('message');
            $PWD = "wn9mxO76f34=";
            $UID = "fourqt";
            $f = "m";
            $url = "";
            $project_name = "";
            $src = $request->input('src');
            $amob = '';
            $city = "";
            $location = "";
            $website = "Website";

            $client = new \GuzzleHttp\Client();

            $sendData = [
//            'UID' => "fourqt",
//            'PWD' => "wn9mxO76f34=",
//            'f' => 'm',
//            'con' => '9028669307',
//            'email' => 'kc@gmail.com',
//            'name' => 'abc',
//            'url'=>'http://ivymarketing.in/1oak/public/',
//            'ch'=>'Website',
//            'location'=>'MUMBAI',
//            'city'=>'mumbai',
//            'src'=>'Website',
//            'Proj'=>'testing project',
//            'Remark'=>'testing remark',
//            'amob'=>''
            ];

//        $url = "http://mirai02.realeasy.in/IVR_Inbound.aspx?UID=fourqt&PWD=wn9mxO76f34=&f=m&con=9028669307&email=kc@gmail.com
//        &name=abc&url=abc.co&location=mum&city=mum&ch=website&src=abc&Projxml&Remark=testing&amob=45";

            $url = "http://mirai02.realeasy.in/IVR_Inbound.aspx?UID=$UID&PWD=$PWD&f=$f&con=$con&email=$email&name=$name&url=$url&Remark=$remark&Proj=$project_name&src=$src&amob=$amob&city=$city&location=$location&ch=$website";

            $res = $client->post($url, [
                'form_params' => $sendData,
            ]);

            if ($res->getStatusCode() !== 200) {
                return response()->json([
                    "code" => 0,
                    'message' => "Something Went Wrong please try again",
                    "data" => []
                ]);
            }
//        $data = json_decode($res->getBody()->getContents(), true);
//        dd($data);
//        $email = $request->input('email');
//        Mail::send('emails.welcome_email', ["name" => $request->input('first_name')
//        ], function ($message) use ($email) {
//            $message->subject('Welcome to 1oak.');
//            $message->to($email);
//
//        });
            return response()->json([
                "code" => 1,
                'message' => "success",
                "data" => []
            ]);


        }
    }
}
