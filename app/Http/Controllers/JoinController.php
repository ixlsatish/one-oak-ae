<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Validator;

class JoinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        echo "store";
        $inputs = $request->all();
        $message = array(
            "name.required" => "Name required",
            "name.max" => " Max :max",
            "mobile_number.required" => "Mobile required",
            "email.required" => "Email required",
            "message.required" => "Message required",
            "message.max" => "Message max 20",

        );
        $validator = Validator::make($inputs, [
            "name" => "required|max:20|alpha",
            "mobile_number" => "required|numeric|max:10|min:10",
            "email" => "required|email",
            "file" => "mimetypes:application/pdf|max:2000",
            "message" => "required|max:50"

        ], $message);




//        dd($request->all());
        if ($validator->fails()) {
//            echo "Validation failed";
            return redirect()->back()->withErrors($validator)->withInput();
        }
        echo "success";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function joinUsForm(Request $request)
    {
        //

        $inputs = $request->all();

        $rule=[
            "name" => "required|regex:/^[a-zA-Z ]*$/|min:1|max:50",
            "mobile_number" => "required|digits:10",
            "email" => "required|email",
//            "uploaded_file" => "mimetypes:application/pdf|max:2000",
            "message" => "required|max:500"
        ];

        if($request->input('uploaded_file')!="undefined")
        {
            $rule["uploaded_file"]= "mimetypes:application/pdf|max:2000";
        }

        $message = array(
            "name.required" => "Please Enter Full Name.",
            "name.max" => " Max :max",
            "name.regex" => " Please Enter Valid Full Name.",
            "mobile_number.required" => "Please Enter Mobile Number",
            "mobile_number.digits" => "Please Enter 10 Digit Valid Mobile Number.",
            "email.required" => "Please Enter Email.",
            "email.email" => "Please Enter A Valid Email.",
            "message.required" => "Please Enter Message",
            "message.max" => "You Can Enter Max 500 Alphabets.",
            "uploaded_file.mimetypes"=>"The uploaded file must be of type .PDF",

        );
        $validator = Validator::make($inputs, $rule, $message);

//        if($request->input('uploaded_file')!="undefined")
//        {
//            $validator2 = Validator::make($inputs, [
//
//                "uploaded_file" => "mimetypes:application/pdf|max:2000"]);
//            $validator->errors()->add('uploaded_file', 'This is the error message');
//        }
        if ($validator->fails()) {
//            echo "Validation failed";
//            return redirect()->back()->withErrors($validator)->withInput();
            return response()->json([
                "code"=>0,
                'message' => $validator->errors()->all(),
                "data"=>[]
            ]);

        }

        $destinationPath="";
        $fileName="";
        $upload_check = 0;
        $has_file=0;
        if ($request->hasFile('uploaded_file')) {
$has_file=1;
            $destination = 'uploads/';

            $filePath = 'resume/';

            $destinationPath = $destination . $filePath;

            $file = $request->file('uploaded_file');

            $fileName = $request->input('mobile_number') . '_' . mt_rand(1111, 9999) . '_' . $file->getClientOriginalName();



            if ($file->move($destinationPath, $fileName)) {
                $upload_check = 1;

            }
        }
        $mobile_code=$request->input('mobile_code');
        $mobile_number=$request->input('mobile_number');
        $contact = $mobile_code . " " . $mobile_number; //mobile no
                $parameters = [
                    "name1" => $request->input('name'),
                    "mobile_number" => $contact,
                    "email" => $request->input('email'),
                    "message1" => $request->input('message')
                ];
                $email = "contact@oneoak.in";
                Mail::send('emails.welcome_email', $parameters, function ($message) use ($email, $destinationPath, $fileName,$upload_check,$has_file) {
                    $message->subject('Welcome to 1oak.');
                    $message->to($email);
//                    dd($has_file,$upload_check);
                    if($has_file==1 && $upload_check == 1)
                    {
                        $message->attach(public_path('uploads/resume/' . $fileName));
                    }

                });




        return response()->json([
            "code" => 1,
            'message' => "Success",
            "data" => []
        ]);
    }

}
