// function resizeHeaderOnScroll() {
//     console.log('daasasd');
//     const distanceY = window.pageYOffset || document.documentElement.scrollTop,
//         shrinkOn = 200,
//         headerEl = document.getElementById('js-header');
//
//     if (distanceY > shrinkOn) {
//         headerEl.classList.add("smaller");
//     } else {
//         headerEl.classList.remove("smaller");
//     }
// }
//
// window.addEventListener('scroll', resizeHeaderOnScroll);

document.getElementById('innerframe_chat').style["display"]="none";


$(".show-joinus-form").click(function(){
    $('.joinus-form').show('');
    $(".joinus-content").hide();
});
$("#back-joinus").click(function(){
    $('.joinus-form').hide('fast');
    $(".joinus-content").show('slow');
});

$("#seasoned-btn").click(function(){
    $('.show-seasoned').show('');
    $(".show-internship").hide('fast');
});

$("#internship-btn").click(function(){
    $('.show-seasoned').hide('fast');
    $(".show-internship").show();
});

// $('.show_hide').click(function(){
//     $(".spread").toggle();
//     $(this).val( $(this).val() == 'Hide Spreads' ? 'Show Spreads' : 'Hide Spreads' );
// });


$("#read-more").click(function(){
    $(".home-content").addClass("show-readmore");
    $("#read-more").hide();
    $("#hideread-more").show();
});

$("#hideread-more").click(function(){
    $(".home-content").removeClass("show-readmore");
    $("#read-more").show();
    $("#hideread-more").hide();
});

$(".mobi-chat").click(function(){
    $(".footer-content").css("z-index","99");
    $("footer").css("z-index","9999");
});
$("#chat-panel").click(function(){
    $(".footer-content").css("z-index","")
    $("footer").css("z-index","")
});

// $("#chat-panel, .mobi-chat").click(function(){
//     $(".chat__box-main").toggleClass("open-chat");
//     $(".register-main").removeClass("register-open");
//     $(".footer-content").css("z-index","99")
// });
//
// $(".mobi-chat").click(function(){
//     $("#innerframe_chat").style.display = "block";
//     // $(".footer-content").css("z-index","99")
// });

// $(".mobi-chat").on('click',function(){
//     $(".innerframe").toggle();
// });

$(".mobi-chat-btn").click(function(){
    $(".footer-content").css("z-index","auto")
});

$(".register-btn, .mobi-register").click(function(){
    $(".register-main").toggleClass("register-open");
    $(".chat-main").removeClass("open-chat");
});

$(".mobi-register").click(function(){
    $("header").css("z-index","9999")
});

$(".close__button").click(function(){
    $("header").css("z-index","1030");
    $(".register-main").removeClass("register-open");
});

$("#home, #team, #projects, #news, #connect, #joinus, #login").click(function(){
    $(".register-main").removeClass("register-open");
    $(".chat-main").removeClass("open-chat");
});


// Nav bar Start
$('#home, #home > a').mouseover(function(){
    $('#nav-bar').addClass('home-nav');
    $('#nav-bar').removeClass('team-nav projects-nav news-atmos-nav connect-atmos-nav joinus-atmos-nav login-nav');
});
$('#team, #team > a').mouseover(function(){
    $('#nav-bar').addClass('team-nav');
    $('#nav-bar').removeClass('home-nav projects-nav news-atmos-nav connect-atmos-nav joinus-atmos-nav login-nav');
});
$('#projects, #projects > a').mouseover(function(){
    $('#nav-bar').addClass('projects-nav');
    $('#nav-bar').removeClass('home-nav team-nav news-atmos-nav connect-atmos-nav joinus-atmos-nav login-nav');
});
$('#connect, #connect > a').mouseover(function(){
    $('#nav-bar').addClass('connect-atmos-nav');
    $('#nav-bar').removeClass('home-nav team-nav news-atmos-nav projects-nav joinus-atmos-nav login-nav');
});
$('#news, #news > a').mouseover(function(){
    $('#nav-bar').addClass('news-atmos-nav');
    $('#nav-bar').removeClass('home-nav team-nav connect-atmos-nav projects-nav joinus-atmos-nav login-nav');
});
$('#joinus, #joinus > a').mouseover(function(){
    $('#nav-bar').addClass('joinus-atmos-nav');
    $('#nav-bar').removeClass('home-nav team-nav news-atmos-nav connect-atmos-nav projects-nav login-nav');
});
$('#login, #login > a').mouseover(function(){
    $('#nav-bar').addClass('login-nav');
    $('#nav-bar').removeClass('home-nav team-nav news-atmos-nav connect-atmos-nav projects-nav joinus-atmos-nav');
});
$( "#nav-bar" ).mouseout(function(){
    $("#nav-bar").removeClass("home-nav team-nav projects-nav news-atmos-nav connect-atmos-nav joinus-atmos-nav login-nav");
});
// Nav bar End

// Atmos Nav bar Start
$('#home, #home > a').mouseover(function(){
    $('#nav-bar').addClass('home-nav');
    $('#nav-bar').removeClass('gallery-nav location-nav amenities-nav project-plan-nav unit-plan-nav news-atmos-nav connect-atmos-nav joinus-atmos-nav login-nav');
});

$('#gallery, #gallery > a').mouseover(function(){
    $('#nav-bar').addClass('gallery-nav');
    $('#nav-bar').removeClass('home-nav location-nav amenities-nav project-plan-nav unit-plan-nav news-atmos-nav connect-atmos-nav joinus-atmos-nav login-nav');
});
$('#location, #location > a').mouseover(function(){
    $('#nav-bar').addClass('location-nav');
    $('#nav-bar').removeClass('home-nav gallery-nav amenities-nav project-plan-nav unit-plan-nav news-atmos-nav connect-atmos-nav joinus-atmos-nav login-nav');
});

$('#amenities, #amenities > a').mouseover(function(){
    $('#nav-bar').addClass('amenities-nav');
    $('#nav-bar').removeClass('home-nav gallery-nav location-nav project-plan-nav unit-plan-nav news-atmos-nav connect-atmos-nav joinus-atmos-nav login-nav');
});
$('#project-plan, #project-plan > a').mouseover(function(){
    $('#nav-bar').addClass('project-plan-nav');
    $('#nav-bar').removeClass('home-nav gallery-nav location-nav amenities-nav unit-plan-nav news-atmos-nav connect-atmos-nav joinus-atmos-nav login-nav');
});
$('#unit-plan, #unit-plan > a').mouseover(function(){
    $('#nav-bar').addClass('unit-plan-nav');
    $('#nav-bar').removeClass('home-nav gallery-nav location-nav amenities-nav project-plan-nav news-atmos-nav connect-atmos-nav joinus-atmos-nav login-nav');
});
$('#connect, #connect > a').mouseover(function(){
    $('#nav-bar').addClass('connect-atmos-nav');
    $('#nav-bar').removeClass('home-nav gallery-nav location-nav amenities-nav project-plan-nav unit-plan-nav news-atmos-nav joinus-atmos-nav login-nav');
});
$('#news, #news > a').mouseover(function(){
    $('#nav-bar').addClass('news-atmos-nav');
    $('#nav-bar').removeClass('home-nav gallery-nav location-nav amenities-nav project-plan-nav unit-plan-nav connect-atmos-nav joinus-atmos-nav login-nav');
});
$('#joinus, #joinus > a').mouseover(function(){
    $('#nav-bar').addClass('joinus-atmos-nav');
    $('#nav-bar').removeClass('home-nav gallery-nav location-nav amenities-nav project-plan-nav unit-plan-nav news-atmos-nav connect-atmos-nav login-nav');
});
$('#login, #login > a').mouseover(function(){
    $('#nav-bar').addClass('login-nav');
    $('#nav-bar').removeClass('home-nav gallery-nav location-nav amenities-nav project-plan-nav unit-plan-nav news-atmos-nav connect-atmos-nav joinus-atmos-nav');
});
$( "#nav-bar" ).mouseout(function(){ //all nav css add here
    $('#nav-bar').removeClass('home-nav gallery-nav location-nav amenities-nav project-plan-nav unit-plan-nav news-atmos-nav connect-atmos-nav joinus-atmos-nav login-nav');
});
// Atmos Nav bar End



// Project Images Hover
$('.project-no1').hover(function(){
    $('.all-project').addClass('project-no1-hover');
    $('.all-project').removeClass('project-no2-hover project-no3-hover project-no4-hover project-no5-hover project-no6-hover project-no7-hover project-no8-hover');
});
$('.project-no2').hover(function(){
    $('.all-project').addClass('project-no2-hover');
    $('.all-project').removeClass('project-no1-hover project-no3-hover project-no4-hover project-no5-hover project-no6-hover project-no7-hover project-no8-hover');
});
$('.project-no3').hover(function(){
    $('.all-project').addClass('project-no3-hover');
    $('.all-project').removeClass('project-no1-hover project-no2-hover project-no4-hover project-no5-hover project-no6-hover project-no7-hover project-no8-hover');
});
$('.project-no4').hover(function(){
    $('.all-project').addClass('project-no4-hover');
    $('.all-project').removeClass('project-no1-hover project-no2-hover project-no3-hover project-no5-hover project-no6-hover project-no7-hover project-no8-hover');
});
$('.project-no5').hover(function(){
    $('.all-project').addClass('project-no5-hover');
    $('.all-project').removeClass('project-no1-hover project-no2-hover project-no3-hover project-no4-hover project-no6-hover project-no7-hover project-no8-hover');
});
$('.project-no6').hover(function(){
    $('.all-project').addClass('project-no6-hover');
    $('.all-project').removeClass('project-no1-hover project-no2-hover project-no3-hover project-no4-hover project-no5-hover project-no7-hover project-no8-hover');
});
$('.project-no7').hover(function(){
    $('.all-project').addClass('project-no7-hover');
    $('.all-project').removeClass('project-no1-hover project-no2-hover project-no3-hover project-no4-hover project-no5-hover project-no6-hover project-no8-hover');
});
$('.project-no8').hover(function(){
    $('.all-project').addClass('project-no8-hover');
    $('.all-project').removeClass('project-no1-hover project-no2-hover project-no3-hover project-no4-hover project-no5-hover project-no6-hover project-no7-hover');
});

$( ".all-project" ).mouseout(function(){
    $(".all-project").removeClass("project-no1-hover project-no2-hover project-no3-hover project-no4-hover project-no5-hover project-no6-hover project-no7-hover project-no8-hover");
});

// Project Images Hover End


// Team Start
$("#management-member-01").click(function(){
    $('.management-list').hide();
    $("#management-details01").fadeIn();
});

$("#management-member-02").click(function(){
    $('.management-list').hide();
    $("#management-details02").fadeIn();
});

$("#management-member-03").click(function(){
    $('.management-list').hide();
    $("#management-details03").fadeIn();
});

$("#management-member-04").click(function(){
    $('.management-list').hide();
    $("#management-details04").fadeIn();
});

$("#management-member-05").click(function(){
    $('.management-list').hide();
    $("#management-details05").fadeIn();
});

$("#advisory-member-01").click(function(){
    $('.advisory-list').hide();
    $("#advisory-details01").fadeIn();
});

$("#management-member-02, #management-member-03, #management-member-04, #management-member-05, #management-member-06").click(function(){
    $('.management-list').hide();
    $("#management-details").fadeIn();
});

$(".goto-management-team-list").click(function(){
    $('.management-list').fadeIn();
    $("#management-details, #management-details01, #management-details02, #management-details03, #management-details04, #management-details05, #management-details07").hide();
});

$("#advisory-member-02, #advisory-member-03, #advisory-member-04, #advisory-member-05, #advisory-member-06").click(function(){
    $('.advisory-list').hide();
    $("#advisory-details").fadeIn();
});

$(".goto-advisoryteam-list").click(function(){
    $('.advisory-list').fadeIn();
    $("#advisory-details, #advisory-details01").hide();
});

$("#management-btn").click(function(){ // Add Management Details
    $('.management-list').css("display","block");
    $('#management-details, #management-details01, #management-details02, #management-details03, #management-details04, #management-details05, #advisory-details01, .advisory-list').css("display","none");
});

$("#advisory-btn").click(function(){ // Advisory Details
    $('.advisory-list').css("display","block");
    $('#advisory-details, #management-details01, #management-details02, #management-details03, #management-details04, #management-details05, #advisory-details01').css("display","none")
});

$(".i-agree-btn").click(function(){
    $('.disclaimer-main').fadeOut();
});

$(".navigation__button").click(function(){ // On click Menu Function
    $('.mobile-nav').removeClass("slideOutRight");
    $('.mobile-nav').addClass("slideInRight");
    $('.mobile-nav').show()
    $('body').addClass('overflow-hidden')
});

$("#menu-close, .menu-close").click(function(){ // On click Close Function
    $('.mobile-nav').addClass("slideOutRight");
    $('.mobile-nav').removeClass("slideInRight");
    $('.mobile-nav').delay(2000).fadeIn('slow');
    $('body').removeClass('overflow-hidden')
});

$("#m-amenities-close").click(function(){
    $('.amenities-popup').fadeOut();
    $('#m-premium-appliances, #m-wardrobes, #m-fitness, #m-wimming, #mosques, #landscaped-gardens, #security, #elegant-lobby').addClass("m-amenities");
});

$(".m-premium-appliances").click(function(){
    $('.amenities-popup').fadeIn();
    $('#m-premium-appliances').removeClass("m-amenities");
});
$(".m-wardrobes").click(function(){
    $('.amenities-popup').fadeIn();
    $('#m-wardrobes').removeClass("m-amenities");
});
$(".m-fitness").click(function(){
    $('.amenities-popup').fadeIn();
    $('#m-fitness').removeClass("m-amenities");
});
$(".m-wimming").click(function(){
    $('.amenities-popup').fadeIn();
    $('#m-wimming').removeClass("m-amenities");
});
$(".mosques").click(function(){
    $('.amenities-popup').fadeIn();
    $('#mosques').removeClass("m-amenities");
});
$(".landscaped-gardens").click(function(){
    $('.amenities-popup').fadeIn();
    $('#landscaped-gardens').removeClass("m-amenities");
});
$(".elegant-lobby").click(function(){
    $('.amenities-popup').fadeIn();
    $('#elegant-lobby').removeClass("m-amenities");
});
$(".security").click(function(){
    $('.amenities-popup').fadeIn();
    $('#security').removeClass("m-amenities");
});



// Team End

// News Start

$(".back-news-list").click(function(){
    $('.new1-tab, .new2-tab, .new3-tab, .new4-tab, .new5-tab, .new6-tab, .new7-tab').hide();
    $('#new-tab01, #new-tab02, #new-tab03, #new-tab04, #new-tab05, #new-tab06, #new-tab07, .nav-link').removeClass("active show");
    $('#news-tab').show();
});

$(".mobile-news-tab").click(function(){
    $('#news-tab').hide();
    $('.nav-link').removeClass("active show");
    $('#new-tab01, #new-tab02, #new-tab03, #new-tab04, #new-tab05, #new-tab06, #new-tab07, .nav-link').removeClass("active show");
});

// News ENd





// We can attach the `fileselect` event to all file inputs on the page
$(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

// We can watch for our custom `fileselect` event like this
$(document).ready( function() {
    $(':file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }

    });
});



// Mobile

$(".nav-link").click(function(){
    $("#v-pills-tabContent").fadeIn("")
    $(".news-tab").hide();
});


if ($(window).width() < 767) {  // add active class in mobile view
//            console.log('test');
    $('nav, header, footer, .main-body').removeClass('videoloader');
    $('#gallery-tab01, #location-tab02, #amenities-tab03, #floorplans-tab04, #unitplans-tab04, #virtualtour-tab05, #download-tab06').addClass('active show');
}


