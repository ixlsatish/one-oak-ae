@extends('layouts.app')

@section('styles')
@endsection

@section('video')
    <div class="disclaimer-main" id="disclaimer-main" style="display: none">
        <div class="disclaimer-page-copy">

            <h1>Disclaimer</h1><br>
            <div class="scroll">
                <p>Thank you for visiting our website. We are currently updating our website towards compliance of the requirements of the Real Estate (Regulation and Development) Act, 2016; and the Rules and Regulations framed, thereunder.</p>
                <p>We are also in the process of applying for and obtaining registration of our ongoing/upcoming real estate projects under the provisions of the Real Estate (Regulation and Development) Act, 2016 with the relevant authorities.</p>
                <p>In the interim, please note that this website is only representational and informative.. By accessing this website, the viewer confirms that the information including brochures and images on this website are solely for informational purposes only and the viewer has not solely relied on this information for making any booking/purchase in any project of the Company. No information, images or material which is currently available / displayed on the website shall be deemed to constitute any advertisement, invitation, solicitation, offer or sale of any of our product offerings and we shall not accept any such bookings of flats or offices or premises or confirm allotment of any flats or offices or premises, based on reliance of such currently available information, images and material on the website; and we further shall not be responsible for any consequences of any action taken by any person or authority relying on material/information or otherwise. Please contact our sales team at <a href="mailto:sales@oneoak.ae" class="text-hover color-black">sales@oneoak.ae</a> for the updated sales and marketing information and collaterals.</p>
                <p>By the visitor proceeding to enter and view the contents of this website, the visitor is confirming the aforesaid.</p>
            </div>
            <button class="btn i-agree-btn float-right" id="i-agree">
                Yes, I agree
            </button>

        </div>
    </div>
    <div><button type="button" name="skip" value="SKIP" id="skip" class="skip color-white2 skip-btn">Skip</button> </div>
    <video class="craft-beer-landing-vidbg testing" muted autoplay id="video-bg">
        <source src="" type="video/mp4" id="video_source">
    </video>
@endsection

@section('content')


    <section class="home-page fadeInLeft animated delay-4">
        @if (count($errors) > 0)
            <div class="alert alert-danger font-bold" style="text-transform: capitalize;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif
        <div class="home-logo section-pl-12 position-relative">
            <div class="position-absolute w-50 h-100"></div>
            <img src="{{asset('images/logo.png')}}">
        </div>
        <h1 class="section-title mt-4 title-line section-pl-12">
            Live More.
        </h1>
        <div class="text-left content section-pl-12 home-content">
            <p>
                1OAK offers a boutique real estate experience that combines the delivery of aspirational homes at
                exceptional value, making the dream of owning a home a reality.
            </p>
            <p>
                Supported by Singapore-based Greenfield Advisory Pte. Ltd. our financial and investment expertise is underpinned
                by extensive real-estate experience across India, South-East Asia and Europe.
            </p>
            <p>
                This unique blend of expert financial advisory and boutique experience sets 1OAK apart as a one of a
                kind real estate firm.
            </p>
            <p>
                From first time buyers to established investors, 1OAK is the trusted partner that inspires and enables
                you to Live More, today.
            </p>
            <div id="read-more" class="float-left text-decoration text-hover cursor-pointer ">
                Read More
            </div>
            <div id="hideread-more" class="float-left text-decoration text-hover cursor-pointer ">
                Hide
            </div>
        </div>
    </section>


@endsection

@section('scripts')
    <script src="{{asset('js/video.js')}}" rel="script"></script>

    <!-- Onload remove register open class Start -->
    <script>
        $(document).ready(function() {
            $("header").css("z-index","9999");
        });
    </script>
    <!-- Onload remove register open class End -->

@endsection
