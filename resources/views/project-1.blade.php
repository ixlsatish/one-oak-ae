@extends('layouts.app')
@section('content')
    <div class="page-bg corella-bg page-container align-center fadeIn animated delay-0 mdtp-flex-none">
        <section class="project-details-page bg-color-black-op6 center-middle align-items position-relative fadeInLeft animated delay-4 mdtp-flex-none">

            {{--<div class="w-100 float-left ">--}}
            <div class="inner-logo text-center position-absolute pr-4">
                <a href="{{route('index')}}"><img src="{{asset('images/logo2.png')}}" alt="logo"></a>
            </div>
            <div class="col-md-6 pl-0">
                <div class="section-pl-25">
                    <div class="w-100 float-left mdtp-w-70 mdtp-float-right">
                        <div class="w-100 float-left">
                            <h1 class="section-title mt-2 float-left pr-2">
                                Rootsat36
                            </h1>
                        </div>
                        <h2 class="font15">DUBAI </h2>
                        <p>Atmos is a new landmark redefining the Lucknow’s high rise residential market. Strategically located in the heart of Gomti Nagar, ATMOS is a premium 24 storey development with expansive 3 and 4 bedroom residences. Each house provides its residents absolute privacy, while allowing them to be close to nature.</p>
                        {{--<p>--}}
                            {{--Pioneering real estate by sculpting aspirational homes. Lorem ipsum dolor sit amet,--}}
                            {{--consectetur adipiscing elit. Integer accumsan nec tellus a scelerisque. Proin pharetra--}}
                            {{--ornare mauris, et maximus enim rutrum et.--}}
                        {{--</p>--}}
                    </div>
                    {{--tab id="exp01" --same-- content aria-labelledby="exp01"--}}
                    {{--tab href="#tab01" and aria-controls="tab01" --same-- content id="tab01"--}}
                    <div class="nav project-details flex-column nav-pills mt-5 pt-3 pt-0-sm float-left-mdtp mt-mdtp-3 desktop-show"
                         id="project-details" role="tablist" aria-orientation="vertical">
                        {{---------------------- Tab 01 Start ---------------------}}
                        <a class="nav-link active" id="gallery-tab" data-toggle="pill" href="#gallery-tab01" role="tab"
                           aria-controls="gallery-tab01" aria-selected="true">
                            Gallery
                        </a>
                        {{---------------------- Tab 02 Start ---------------------}}
                        <a class="nav-link" id="location-tab" data-toggle="pill" href="#location-tab02" role="tab"
                           aria-controls="location-tab02" aria-selected="false">
                            Location
                        </a>
                        {{---------------------- Tab 03 Start ---------------------}}
                        <a class="nav-link" id="amenities-tab" data-toggle="pill" href="#amenities-tab03" role="tab"
                           aria-controls="amenities-tab03" aria-selected="false">
                            Amenities
                        </a>
                        {{---------------------- Tab 04 Start ---------------------}}
                        <a class="nav-link" id="floorplans-tab" data-toggle="pill" href="#floorplans-tab04" role="tab"
                           aria-controls="floorplans-tab04" aria-selected="false">
                            Project Plan
                        </a>

                        {{---------------------- Tab 05 Start ---------------------}}
                        <a class="nav-link" id="unitplans-tab" data-toggle="pill" href="#unitplans-tab04" role="tab"
                           aria-controls="unitplans-tab04" aria-selected="false">
                            Unit Plan
                        </a>
                        {{---------------------- Tab 05 Start hidden for now ---------------------}}
                        {{--<a class="nav-link" id="virtualtour-tab" data-toggle="pill" href="#virtualtour-tab05" role="tab"--}}
                           {{--aria-controls="virtualtour-tab05" aria-selected="false">--}}
                            {{--Virtual Tour--}}
                        {{--</a>--}}
                        {{---------------------- Tab 06 End hidden for now---------------------}}
                        {{--<a class="nav-link" id="download-tab" data-toggle="pill" href="#download-tab06" role="tab"--}}
                           {{--aria-controls="download-tab06" aria-selected="false">--}}
                            {{--Download Brochure--}}
                        {{--</a>--}}
                        {{---------------------- Tab 06 End ---------------------}}
                    </div>
                </div>
            </div>
            <div class="col-md-6 pl-0 pr-0 pl-mdtp-2 pr-mdtp-2">
                <div class="w-100 float-left">
                    <div class="tab-content" id="v-pills-tabContent">
                        {{----------------------Gallery Tab Content 01 Start ---------------------}}
                        <div class="tab-pane fade show active" id="gallery-tab01" role="tabpanel"
                             aria-labelledby="gallery-tab">
                            <div class="w-100 float-left gallery-main">
                                <h4 class="float-left w-100 mobi-show"> Gallery</h4>
                                <div class="w-100 float-left" id="slider">
                                    <div id="myCarousel" class="carousel slide">
                                        <!-- main slider carousel items -->
                                        <div class="carousel-inner">
                                            <div class="active item carousel-item" data-slide-number="0">
                                                <img src="{{asset('images/projects-img/corella-project/project-img01.jpg')}}"
                                                     alt="project" class="w-100 img-fluid">
                                            </div>
                                            <div class="item carousel-item" data-slide-number="1">
                                                <img src="{{asset('images/projects-img/corella-project/project-img02.jpg')}}"
                                                     alt="project" class="w-100 img-fluid">
                                            </div>
                                            <div class="item carousel-item" data-slide-number="2">
                                                <img src="{{asset('images/projects-img/corella-project/project-img03.jpg')}}"
                                                     alt="project" class="w-100 img-fluid">
                                            </div>
                                            <div class="item carousel-item" data-slide-number="3">
                                                <img src="{{asset('images/projects-img/corella-project/project-img04.jpg')}}"
                                                     alt="project" class="w-100 img-fluid">
                                            </div>
                                            <div class="item carousel-item" data-slide-number="4">
                                                <img src="{{asset('images/projects-img/corella-project/project-img05.jpg')}}"
                                                     alt="project" class="w-100 img-fluid">
                                            </div>
                                            {{--<a class="carousel-control left pt-3" href="#myCarousel"--}}
                                               {{--data-slide="prev"><i class="fa fa-chevron-left"></i></a>--}}
                                            {{--<a class="carousel-control right pt-3" href="#myCarousel" data-slide="next"><i--}}
                                                        {{--class="fa fa-chevron-right"></i></a>--}}
                                            <a class="carousel-control-prev prev previous" href="#myCarousel"
                                               data-slide="prev"
                                               id="previous">
                                                <span class="carousel-control-prev-icon"></span>
                                            </a>
                                            <a class="carousel-control-next next" href="#myCarousel" data-slide="next"
                                               id="next">
                                                <span class="carousel-control-next-icon"></span>
                                            </a>
                                        </div>
                                        <!-- main slider carousel nav controls -->


                                        <ul class="carousel-indicators list-inline">
                                            <li class="list-inline-item active">
                                                <a id="carousel-selector-0" class="selected" data-slide-to="0"
                                                   data-target="#myCarousel">
                                                    <img src="{{asset('images/projects-img/corella-project/project-img01.jpg')}}"
                                                         class="img-fluid">
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a id="carousel-selector-1" data-slide-to="1" data-target="#myCarousel">
                                                    <img src="{{asset('images/projects-img/corella-project/project-img02.jpg')}}"
                                                         class="img-fluid">
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a id="carousel-selector-2" data-slide-to="2" data-target="#myCarousel">
                                                    <img src="{{asset('images/projects-img/corella-project/project-img03.jpg')}}"
                                                         class="img-fluid">
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a id="carousel-selector-3" data-slide-to="3" data-target="#myCarousel">
                                                    <img src="{{asset('images/projects-img/corella-project/project-img04.jpg')}}"
                                                         class="img-fluid">
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a id="carousel-selector-3" data-slide-to="4" data-target="#myCarousel">
                                                    <img src="{{asset('images/projects-img/corella-project/project-img05.jpg')}}"
                                                         class="img-fluid">
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>

                        {{----------------------Location Tab Content 02 Start ---------------------}}
                        <div class="tab-pane fade" id="location-tab02" role="tabpanel" aria-labelledby="location-tab">
                            <h4 class="float-left mobi-show w-100 mt-4"> Location</h4>
                            {{--<div class="w-100 float-left h-100">--}}
                            <div class="h-50 w-100 float-left" style="overflow: hidden">
                                <a href="https://www.google.com/maps/search/+one+world+lucknow/@26.8228218,81.019701,15.75z" target="_blank">
                                    <img src="{{asset('images/location-img.jpg')}}" alt="location" class="w-100">
                                </a>
                            </div>
                            <div class="w-100 float-left pt-1" style="height: calc(50% - 10px); margin-bottom: 5px; overflow: hidden; overflow-y: auto">
                                <div class="w-100 float-left pb-2">
                                    Located in the premier Gomti Nagar Extension.
                                </div>
                                <div class="w-100 float-left pb-2">
                                    In close proximity to - The International Cricket Stadium, the upcoming IT City, Medanta Awadh Hospital, the airport, numerous reputed schools, Lucknow's most premium brands of hotels, Connaught Place, cancer hospital, Ambedkar Udyan and Jeneshwar Mishra Park..
                                </div>
                                <div class="w-100 float-left pb-2">
                                    The Project has the most unparalleled location benefits
                                </div>
                                <div class="w-100 float-left pb-2">
                                    Housed in the exclusive Lake District of One World Project - the most sought after neighbourhoods to reside in Lucknow
                                </div>
                                <div class="w-100 float-left pb-2">Adjacent to Amar Shaheed Path - Lucknow’s main ring road</div>
                                <div class="w-100 float-left pb-2">Easy access from Lucknow - Gorakhpur, Lucknow - Varanasi, Lucknow - Rae Bareli and Lucknow - Kanpur Highways</div>
                            </div>
                            {{--</div>--}}
                        </div>

                        {{----------------------Amenities Tab Content 03 Start ---------------------}}
                        <div class="pt-3 pb-3 pr-3 float-left tab-pane fade" id="amenities-tab03" role="tabpanel"
                             aria-labelledby="amenities-tab">
                            <div class="w-100 float-left amenities-main border-left mobi pt-2 pb-2 pl-4  pl-2-sm">
                                <h4 class="float-left mobi-show w-100 mt-2"> Amenities</h4>
                                <ul class="nav nav-pills w-100">
                                    <li class="m-premium-appliances">
                                        <a class="amenitie-iconbox" data-toggle="pill" href="#amenities01"
                                           class="active">
                                            <div class="amenities-icon amenities-icon01"></div>
                                            <span>Premium<br>Home Appliances</span>
                                        </a>
                                    </li>
                                    <li class="m-wardrobes">
                                        <a class="amenitie-iconbox" data-toggle="pill" href="#amenities02">
                                            <div class="amenities-icon amenities-icon02"></div>
                                            <span>Built-in<br>Wardrobes</span>
                                        </a>
                                    </li>
                                    <li class="m-fitness">
                                        <a class="amenitie-iconbox" data-toggle="pill" href="#amenities03">
                                            <div class="amenities-icon amenities-icon03"></div>
                                            <span>Fitness<br>Centre</span>
                                        </a>
                                    </li>
                                    <li class="m-wimming">
                                        <a class="amenitie-iconbox" data-toggle="pill" href="#amenities04">
                                            <div class="amenities-icon amenities-icon04"></div>
                                            <span>Swimming<br>Pool</span>
                                        </a>
                                    </li>
                                    <li class="mosques">
                                        <a class="amenitie-iconbox" data-toggle="pill" href="#amenities05">
                                            <div class="amenities-icon amenities-icon05"></div>
                                            <span>Mosques</span>
                                        </a>
                                    </li>
                                    <li class="landscaped-gardens">
                                        <a class="amenitie-iconbox" data-toggle="pill" href="#amenities06">
                                            <div class="amenities-icon amenities-icon06"></div>
                                            <span>Landscaped<br>Gardens</span>
                                        </a>
                                    </li>
                                    <li class="elegant-lobby">
                                        <a class="amenitie-iconbox" data-toggle="pill" href="#amenities07">
                                            <div class="amenities-icon amenities-icon07"></div>
                                            <span>Elegant<br>Lobby</span>
                                        </a>
                                    </li>
                                    <li class="security">
                                        <a class="amenitie-iconbox" data-toggle="pill" href="#amenities08">
                                            <div class="amenities-icon amenities-icon08"></div>
                                            <span>24 Hour<br>Security</span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content amenities-tab-show-content pl-3">
                                    <div id="amenities01" class="tab-pane fade in active show">
                                        <h3>Premium Home Appliances</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae felis
                                            non neque congue mattis. Etiam porta lectus risus, non sagittis eros
                                            pellentesque id. </p>
                                    </div>
                                    <div id="amenities02" class="tab-pane fade">
                                        <h3>Built-in Wardrobes</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae felis
                                            non neque congue mattis. Etiam porta lectus risus, non sagittis eros
                                            pellentesque id. </p>
                                    </div>
                                    <div id="amenities03" class="tab-pane fade">
                                        <h3>Fitness Centre</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae felis
                                            non neque congue mattis. Etiam porta lectus risus, non sagittis eros
                                            pellentesque id. </p>
                                    </div>
                                    <div id="amenities04" class="tab-pane fade">
                                        <h3>Swimming Pool</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae felis
                                            non neque congue mattis. Etiam porta lectus risus, non sagittis eros
                                            pellentesque id. </p>
                                    </div>
                                    <div id="amenities05" class="tab-pane fade">
                                        <h3>Mosques</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae felis
                                            non neque congue mattis. Etiam porta lectus risus, non sagittis eros
                                            pellentesque id. </p>
                                    </div>
                                    <div id="amenities06" class="tab-pane fade">
                                        <h3>Landscaped Gardens</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae felis
                                            non neque congue mattis. Etiam porta lectus risus, non sagittis eros
                                            pellentesque id. </p>
                                    </div>
                                    <div id="amenities07" class="tab-pane fade">
                                        <h3>Elegant Lobby</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae felis
                                            non neque congue mattis. Etiam porta lectus risus, non sagittis eros
                                            pellentesque id. </p>
                                    </div>
                                    <div id="amenities08" class="tab-pane fade">
                                        <h3>24 Hour Security</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae felis
                                            non neque congue mattis. Etiam porta lectus risus, non sagittis eros
                                            pellentesque id. </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{----------------------Floor Plans Tab Content 04 Start ---------------------}}
                        <div class="tab-pane fade" id="floorplans-tab04" role="tabpanel"
                             aria-labelledby="floorplans-tab">
                            <div class="w-100 float-left floorplans-main">
                                <h4 class="float-left w-100 mobi-show"> Project Plan</h4>
                                <div class="tab-content">

                                    <div id="site-plan" class="tab-pane fade">
                                        <div id="bedroom-1" class="carousel slide" data-ride="carousel">
                                            <!-- Indicators -->
                                            <ul class="carousel-indicators">
                                                <li data-target="#bedroom-1" data-slide-to="0" class="active"></li>
                                            </ul>
                                            <!-- The slideshow -->
                                            <div class="carousel-inner floor_swipe">
                                                <div class="carousel-item active">
                                                    <img src="{{asset('images/floor-plans/site-plan.jpg')}}" alt="site-plan" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        Carefully planned infrastructure with well-designed and tree-lined roads, lush gardens, and amenities meant to pamper the residents and their guests.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="master-plan" class="tab-pane fade active show">
                                        <div id="bedroom-2" class="carousel slide" data-ride="carousel">
                                            <!-- Indicators -->
                                            <ul class="carousel-indicators">
                                                <li data-target="#bedroom-2" data-slide-to="0" class="active"></li>
                                            </ul>
                                            <!-- The slideshow -->
                                            <div class="carousel-inner floor_swipe">
                                                <div class="carousel-item active">
                                                    <img src="{{asset('images/floor-plans/master-plan.jpg')}}" alt="master-plan" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        Nestled in the heart of 300 acres of planned township, the project will be close to fine dining restaurants, multiplexes, cafes’, boutiques and shopping malls, making it the perfect address.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="floor-plan" class="tab-pane fade">
                                        <div id="floor" class="carousel slide" data-ride="carousel">
                                            <!-- Indicators -->
                                            <ul class="carousel-indicators">
                                                <li data-target="#floor" data-slide-to="0" class="active"></li>
                                                <li data-target="#floor" data-slide-to="1"></li>
                                                <li data-target="#floor" data-slide-to="2"></li>
                                                <li data-target="#floor" data-slide-to="3"></li>
                                                <li data-target="#floor" data-slide-to="4"></li>
                                            </ul>
                                            <!-- The slideshow -->
                                            <div class="carousel-inner floor_swipe" id="floor">
                                                <div class="carousel-item active">
                                                    <img src="{{asset('images/floor-plans/3bhk-full.jpg')}}" alt="3bhk-full" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        Exclusive amenities such as indoor swimming pool and gymnasium, in addition to Club Lounge.
                                                    </div>
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{asset('images/floor-plans/earth-villa-lower-full.jpg')}}" alt="earth-villa-lower" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        The exclusive experience of low density living with 4 residences per floor affording greater privacy to its denizens.
                                                    </div>
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{asset('images/floor-plans/earth-villa-upper-full.jpg')}}" alt="earth-villa-upper" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        Exclusive amenities such as indoor swimming pool and gymnasium, in addition to Club Lounge.
                                                    </div>
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{asset('images/floor-plans/sky-villa-lower-full.jpg')}}" alt="sky-villa-lower" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        The exclusive experience of low density living with 4 residences per floor affording greater privacy to its denizens.
                                                    </div>
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{asset('images/floor-plans/sky-villa-upper-full.jpg')}}" alt="sky-villa-upper" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        Exclusive amenities such as indoor swimming pool and gymnasium, in addition to Club Lounge.
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Left and right controls -->
                                            <a class="carousel-control-prev prev previous" href="#floor"
                                               data-slide="prev"
                                               id="previous">
                                                <span class="carousel-control-prev-icon"></span>
                                            </a>
                                            <a class="carousel-control-next next" href="#floor" data-slide="next"
                                               id="next">
                                                <span class="carousel-control-next-icon"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <ul class="nav floorplans-tabs">
                                    <li><a data-toggle="tab" href="#master-plan" class="active show"> Master Plan</a></li>
                                    <li><a data-toggle="tab" href="#site-plan">Site Plan</a></li>
                                    <li class="border-right-0"><a data-toggle="tab"  href="#floor-plan">Floor Plan</a></li>
                                </ul>

                            </div>
                        </div>


                        {{----------------------Floor Plans Tab Content 04 Start ---------------------}}
                        <div class="tab-pane fade" id="unitplans-tab04" role="tabpanel"
                             aria-labelledby="unitplans-tab">
                            <div class="w-100 float-left floorplans-main">
                                <h4 class="float-left w-100 mobi-show"> Unit Plan</h4>
                                <div class="tab-content">

                                    <div id="bhk3" class="tab-pane fade active show">
                                        <div id="bhk" class="carousel slide" data-ride="carousel">
                                            <!-- Indicators -->
                                            <ul class="carousel-indicators">
                                                <li data-target="#bhk" data-slide-to="0" class="active"></li>
                                                <li data-target="#bhk" data-slide-to="1" ></li>
                                            </ul>
                                            <!-- The slideshow -->
                                            <div class="carousel-inner floor_swipe" id="bhk">
                                                <div class="carousel-item active">
                                                    <img src="{{asset('images/floor-plans/3bhk01.jpg')}}" alt="3bhk image" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        Optimally planned residences such that each room seamlessly flows into the other, to create efficient space utilization and spaciousness.
                                                    </div>
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{asset('images/floor-plans/3bhk02.jpg')}}" alt="3bhk image" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        Optimally planned residences such that each room seamlessly flows into the other, to create efficient space utilization and spaciousness.
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Left and right controls -->
                                            <a class="carousel-control-prev prev previous" href="#bhk" data-slide="prev"
                                               id="previous">
                                                <span class="carousel-control-prev-icon"></span>
                                            </a>
                                            <a class="carousel-control-next next" href="#bhk" data-slide="next"
                                               id="next">
                                                <span class="carousel-control-next-icon"></span>
                                            </a>
                                        </div>
                                    </div>

                                    <div id="bhk3-premium" class="tab-pane fade">
                                        <div id="premium-bhk3" class="carousel slide" data-ride="carousel">
                                            <!-- Indicators -->
                                            <ul class="carousel-indicators">
                                                <li data-target="#premium-bhk3" data-slide-to="0" class="active"></li>
                                                <li data-target="#premium-bhk3" data-slide-to="1" ></li>
                                            </ul>
                                            <!-- The slideshow -->
                                            <div class="carousel-inner floor_swipe" id="premium-bhk3">
                                                <div class="carousel-item active">
                                                    <img src="{{asset('images/floor-plans/3bhk-premium1.jpg')}}" alt="3bhk-premium1" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        Expansive, elegantly designed residences with natural daylight and airy, open floor plans create seamless flow.
                                                    </div>
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{asset('images/floor-plans/3bhk-premium2.jpg')}}" alt="3bhk-premium2" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        Expansive, elegantly designed residences with natural daylight and airy, open floor plans create seamless flow.
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Left and right controls -->
                                            <a class="carousel-control-prev prev previous" href="#premium-bhk3" data-slide="prev"
                                               id="previous">
                                                <span class="carousel-control-prev-icon"></span>
                                            </a>
                                            <a class="carousel-control-next next" href="#premium-bhk3" data-slide="next"
                                               id="next">
                                                <span class="carousel-control-next-icon"></span>
                                            </a>
                                        </div>
                                    </div>


                                    <div id="earth-villa" class="tab-pane fade">
                                        <div id="earth" class="carousel slide" data-ride="carousel">
                                            <!-- Indicators -->
                                            <ul class="carousel-indicators">
                                                <li data-target="#earth" data-slide-to="0" class="active"></li>
                                                <li data-target="#earth" data-slide-to="1" ></li>
                                            </ul>
                                            <!-- The slideshow -->
                                            <div class="carousel-inner floor_swipe" id="earth">
                                                <div class="carousel-item active">
                                                    <img src="{{asset('images/floor-plans/earth-villa-lower.jpg')}}" alt="earth-villa-lower" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        Luxurious dining and living spaces with double height ceiling overlooking the greens.
                                                    </div>
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{asset('images/floor-plans/earth-villa-upper.jpg')}}" alt="earth-villa-upper" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        Luxurious dining and living spaces with double height ceiling overlooking the greens.
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Left and right controls -->
                                            <a class="carousel-control-prev prev previous" href="#earth" data-slide="prev" id="previous">
                                                <span class="carousel-control-prev-icon"></span>
                                            </a>
                                            <a class="carousel-control-next next" href="#earth" data-slide="next" id="next">
                                                <span class="carousel-control-next-icon"></span>
                                            </a>
                                        </div>
                                    </div>

                                    <div id="sky-villa" class="tab-pane fade">
                                        <div id="sky" class="carousel slide" data-ride="carousel">
                                            <!-- Indicators -->
                                            <ul class="carousel-indicators">
                                                <li data-target="#sky" data-slide-to="0" class="active"></li>
                                                <li data-target="#sky" data-slide-to="1"></li>
                                            </ul>
                                            <!-- The slideshow -->
                                            <div class="carousel-inner floor_swipe" id="sky">
                                                <div class="carousel-item active">
                                                    <img src="{{asset('images/floor-plans/sky-villa-lower.jpg')}}" alt="sky-villa-lower"  class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        Features luxurious private terrace & 18 feet glass window that open out to uninterrupted views of the 1World skyline to the North and the Gomti River to the South.
                                                    </div>
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{asset('images/floor-plans/sky-villa-upper.jpg')}}" alt="sky-villa-upper"  class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        Features luxurious private terrace & 18 feet glass window that open out to uninterrupted views of the 1World skyline to the North and the Gomti River to the South.
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Left and right controls -->
                                            <a class="carousel-control-prev prev previous" href="#sky" data-slide="prev" id="previous">
                                                <span class="carousel-control-prev-icon"></span>
                                            </a>
                                            <a class="carousel-control-next next" href="#sky" data-slide="next" id="next">
                                                <span class="carousel-control-next-icon"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <ul class="nav unitplans-tabs">
                                    <li><a data-toggle="tab" href="#bhk3" class="active show">3 BHK</a></li>
                                    <li><a data-toggle="tab" href="#bhk3-premium">3 BHK Premium</a></li>
                                    <li><a data-toggle="tab" href="#earth-villa">Earth Villa</a></li>
                                    <li class="border-right-0"><a data-toggle="tab"  href="#sky-villa">SKY Villa</a></li>
                                </ul>

                            </div>
                        </div>

                        {{---------------------- Virtual Tour Tab Content 05 Start ---------------------}}
                        <div class="tab-pane fade d-none" id="virtualtour-tab05" role="tabpanel"
                             aria-labelledby="virtualtour-tab">
                            <div class="w-100 float-left virtualtour-main">
                                <h4 class="float-left w-100 mobi-show mt-3"> Virtual Tour</h4>
                                <div class="tab-content">
                                    <div id="virtualtour-content" class="tab-pane fade active show h-100">
                                        <img src="{{asset('images/virtualtour-img.jpg')}}" alt="logo"
                                             class="w-100 h-100">
                                    </div>
                                    <div id="vt_project-walkthrough" class="tab-pane fade h-100">
                                        <img src="{{asset('images/virtualtour-img.jpg')}}" alt="logo"
                                             class="w-100 h-100">
                                    </div>
                                </div>

                                <ul class="nav virtualtour-tabs">
                                    <li><a data-toggle="tab" href="#virtualtour-content" class="active show">virtual
                                            tour</a></li>
                                    <li><a data-toggle="tab" href="#vt_project-walkthrough">Project Walkthrough</a></li>
                                </ul>
                            </div>
                        </div>

                        {{---------------------- Virtual Tour Tab Content 05 Start ---------------------}}
                        <div class="tab-pane fade d-none" id="download-tab06" role="tabpanel" aria-labelledby="download-tab">
                            <div class="w-100 float-left download-main border-left mobi">
                                <h4 class="float-left w-100 mobi-show mb-0"> Download Brochure </h4>
                                <div class="w-100 float-left pt-2">
                                    <form method="post" name="download_form" id="download_form">
                                        <div><a href="{{ asset('videos/12174002.mp4') }}" download id="download"></a>
                                        </div>
                                        <div class="w-50 float-left pr-1 pt-sm-1 pr-0-sm"><input
                                                    data-parsley-required="true"
                                                    data-parsley-maxlength="20"
                                                    data-parsley-maxlength-message="First Name Should Be Less Than 20 Characters"
                                                    data-parsley-pattern="^[A-Za-z]*$"
                                                    data-parsley-pattern-message="Enter Characters Only" type="text"
                                                    class="form-control" placeholder="First Name" tabindex="1"
                                                    name="first_name" id="first_name"></div>
                                        <div class="w-50 float-left pl-1 pt-sm-1 pl-sm-0"><input
                                                    data-parsley-required="true"
                                                    data-parsley-maxlength="20"
                                                    data-parsley-maxlength-message="Last Name Should Be Less Than 20 Characters"
                                                    data-parsley-pattern="^[A-Za-z]*$"
                                                    data-parsley-pattern-message="Enter Characters Only" type="text"
                                                    class="form-control" placeholder="Last Name" tabindex="2"
                                                    name="last_name" id="last_name"></div>
                                        <div class="w-100 float-left pt-2"><input data-parsley-required="true"
                                                                                  data-parsley-required-message="Please Enter Email Id"
                                                                                  data-parsley-type="email"
                                                                                  data-parsley-type-message="Please Enter A Valid Email"
                                                                                  type="text" class="form-control"
                                                                                  placeholder="Your Email ID"
                                                                                  tabindex="3" name="email" id="email">
                                        </div>

                                        <div class="w-100 float-left pt-2">
                                            <div class="w-25 float-left pr-1 styled-select">
                                                <input data-parsley-required="true" class="form-control" tabindex="4"
                                                       id="mobile_code" value="+91" readonly>
                                            </div>
                                            <div class="w-75 float-left pl-1"><input data-parsley-required="true"
                                                                                     data-parsley-required-message="Please Enter Mobile Number"
                                                                                     data-parsley-type="number"
                                                                                     data-parsley-type-message="Please Enter Valid Number"
                                                                                     data-parsley-minlength="10"
                                                                                     data-parsley-minlength-message="Please Enter Valid Mobile Number"
                                                                                     data-parsley-maxlength="10"
                                                                                     data-parsley-maxlength-message="Please Enter No More Than 10 Digits"
                                                                                     type="text" class="form-control"
                                                                                     placeholder="Phone Number"
                                                                                     tabindex="5" name="mobile_number"
                                                                                     id="mobile_number" maxlength="10">
                                            </div>
                                        </div>
                                        {{--<div class="w-100 float-left pt-2">--}}
                                        {{--<div class="w-100 float-left styled-select">--}}
                                        {{--<select class="color-gray padding" tabindex="6">--}}
                                        {{--<option value="volvo">Project i am interested in</option>--}}
                                        {{--<option value="saab">Lorem ipsum</option>--}}
                                        {{--<option value="opel">dolor sit amet</option>--}}
                                        {{--</select>--}}
                                        {{--</div>--}}
                                        {{--</div>--}}

                                        <div class="w-100 float-left pt-3 font10 text-center">
                                            <label class="rad color-white text-uppercase pr-2">
                                                <input type="radio" name="r1" value="a" id="book-a-visit" checked/>
                                                <i class="mr-1" style="margin-top: -2px;"></i> <span class="pt-1">Book a site visit</span>
                                            </label>
                                            <label class="rad color-white text-uppercase">
                                                <input data-parsley-required="true"
                                                       data-parsley-required-message="Please Select Atleast One Option"
                                                       type="radio" name="r1" value="b" id="request-call-back"/>
                                                <i class="mr-1" style="margin-top: -2px;"></i> Request a call back
                                            </label>
                                        </div>

                                        <div class="w-100 float-left text-center pt-2">
                                            {{csrf_field()}}
                                            <button class="btn" type="button" id="project_form">
                                                Download
                                            </button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            {{--</div>--}}

            <a href="{{route('project-10')}}" class="project-next">
                <span class="desktop-show">Art Patong</span>
                <span class="mobi-show">Next Project ></span>
                <div class="on-hover"></div>
            </a>

            <a href="{{route('project-details')}}" class="project-prev arrow-left0">
                <span class="desktop-show">ATMOS</span>
                <span class="mobi-show ">< Prev Project</span>
                <div class="on-hover"></div>
            </a>
        </section>

        <script type="text/javascript">

            $(document).ready(function () {
//                var country;
//                $.getJSON("http://freegeoip.net/json/", function (data) {
//                    country = data.country_name;
//                    var ip = data.ip;
////                    console.log(country);
//                });
//                if(country='India')
//                {
//                    document.getElementById('mobile_code').value = "+91";
//                    document.getElementById('mobile_code').setAttribute("disabled","disabled");
//                    document.getElementById('mobile_code').setAttribute("checked","checked");
//                }
//                else if(country='Dubai')
//                {
//                    document.getElementById('mobile_code').value = "+972";
//                    document.getElementById('mobile_code').setAttribute("disabled","disabled");
//                    document.getElementById('mobile_code').setAttribute("checked","checked");
//
//                }

                $("#download").click();
                $("#project_form").click(function (e) {

                    $("#project_form").attr("disabled", "disabled");
                    setTimeout(function() {
                        $("#project_form").removeAttr("disabled");
                    }, 6000);
                    var first_name = $('#first_name').val();
                    var last_name = $('#last_name').val();
                    var email = $('#email').val();
                    var mobile_code = $('#mobile_code').val();
                    var mobile_number = $('#mobile_number').val();
                    var radio = $('input[type=radio][name=r1]:checked').attr('id');

                    var ua = navigator.userAgent.toLowerCase();
                    var isAndroid = ua.indexOf("android") > -1;
                    var isIphone = ua.indexOf("iphone") > -1;
                    var isIpod = ua.indexOf("ipod") > -1;
                    var isIpad = ua.indexOf("ipad") > -1;
                    var isBB = ua.indexOf("blackberry") > -1;
                    var isBB2 = ua.indexOf("rim") > -1;
                    var isBB = ua.indexOf("blackberry") > -1;
                    var isBB2 = ua.indexOf("RIM") > -1;
                    var isSymbian = ua.indexOf("symbian") > -1;
                    var isNokia = ua.indexOf("nokia") > -1;
                    if (isNokia || isSymbian || isBB || isBB2 || isIphone || isIpod || isIpad || isAndroid) {
                        var src = "Mobile Website";
                    } else
                        src = "Website";

                    var text = "";
                    $.ajax(
                        {
                            url: '{{route('project-form')}}',
                            data: {
                                first_name: first_name,
                                last_name: last_name,
                                email: email,
                                mobile_code: mobile_code,
                                mobile_number: mobile_number,
                                radio: radio,
                                src:src
                            },
                            type: 'GET',
                            success: function (data) {


                                console.log('success', data);
                                if (data.code === 1) {
//                                    for (i = 0; i < data.message.length; i++) {
//                                        text += data.message[i] + "<br>";
//                                    }
                                    toastr.success("Thank you for your interest. Our team will contact you soon.", {timeOut: 5000});
                                    document.getElementById('download').click();
                                    var frm = document.getElementsByName('download_form')[0];
                                    frm.reset();

                                    var link = document.createElement('a');
                                    link.href = url;
                                    link.download = 'file.pdf';
                                    link.dispatchEvent(new MouseEvent('click'));
                                }

                                else {
                                    for (i = 0; i < data.message.length; i++) {
                                        text += data.message[i] + "<br>";
                                    }
                                    toastr.error(text, {timeOut: 5000})
                                    //toastr.error('You Got Error', 'Inconceivable!', {timeOut: 5000})

                                }
                            },
                            error: function (error) {
                                toastr.error('You Got Error', {timeOut: 5000});
//                                console.log('error');
                            }
                        }
                    );
                });
            });


            //code for floor slider


            var start_x = 0;
            $('.floor_swipe').on('dragstart', function (e) {
                start_x = event.clientX;
            }).on('dragend', function (e) {

                var end_x = event.clientX;
                var direction = start_x - end_x;
                console.log(start_x,end_x,direction);
                if (direction < 0) {
                    $('.previous').trigger('click');
                } else {
                    $('.next').trigger('click');
                }
            });

            $('.floor_swipe').on('touchstart', function (e) {
                start_x = event.changedTouches[0].clientX;
            }).on('touchend', function (e) {

                var end_x = event.changedTouches[0].clientY;
                var direction = start_x - end_x;
                if (direction < 0) {
                    $('.previous').trigger('click');
                } else {
                    $('.next').trigger('click');
                }
            });

        </script>
    </div>
@endsection

@section('scripts')
    <!-- Onload remove register open class Start -->
    <script>
        $(document).ready(function() {
            $('.register-main').removeClass('register-open');
        });
    </script>
    <!-- Onload remove register open class End -->
@endsection