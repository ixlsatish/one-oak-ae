<?php
$segment_1 = Request::segment(1);
?>


{{-- Website Nav Start --}}
<nav class="videoloader fadeInRight animated delay-3">
    <ul id="nav-bar" class="">
        <li id="home"><a href="{{route('index')}}" class="{{ $segment_1 == '' ? 'active' : '' }}">1OAK</a></li>
        {{--<li id="projects"><a href="{{route('projects')}}" class="{{ $segment_1 == 'projects' ? 'active' : '' }}">Projects</a></li>--}}
        <li id="team"><a href="{{route('team')}}"  class="{{ $segment_1 == 'team' ? 'active' : '' }}">team</a></li>
        <li id="news"> <a href="{{route('news')}}" class="{{ $segment_1 == 'news' ? 'active' : '' }}">news</a></li>
        <li id="connect"> <a href="{{route('connect')}}" class="{{ $segment_1 == 'connect' ? 'active' : '' }}">connect</a></li>
        <li id="joinus"> <a href="{{route('join_us')}}" class="{{ $segment_1 == 'join_us' ? 'active' : '' }}">join us</a></li>

        {{--<li id="login"><a href="{{route('login')}}">Customer Login</a></li>--}}
        {{--<li id="login">Customer Login</li>--}}
    </ul>
    <div class="social-media desktop-social-icons">
        <ul>
            <a href="https://www.facebook.com/1OAKrealtors/" target="_blank"> <li class="fb-icon"></li> </a>
            <a href="https://www.instagram.com/1oakrealtors/" target="_blank"> <li class="ig-icon"></li> </a>
            <a href="https://twitter.com/1OAKRealtors/" target="_blank"> <li class="tw-icon mr-0"></li> </a>
        </ul>
    </div>
</nav>
{{-- Website Nav End --}}

{{--Mobile Nav Start --}}
<nav class="videoloader animated mobile-nav">
    <div class="mobi-show menu-close"></div>
    <div class="mobile-bg">
        <label for="navi-toggle" class="close__button" id="menu-close">
            <span class="navigation__icon"><b class="brand-primary">&nbsp;</b></span>
        </label>
        <div class="w-100 float-left mt-3 ml-4">
            <div class="float-left text-hover">
                <a href="http://oneoak.in/" class="color-white cursor-pointer">INDIA</a>
            </div>
            <div class="float-left pr-3 pl-3">|</div>
            <div class="float-left text-hover active brand-primary">UAE</a>
            </div>

        </div>
        <ul id="nav-bar" class="">

            <a href="{{route('index')}}"><li class="{{ $segment_1 == '' ? 'active' : '' }}" id="mobi-home">1OAK</li></a>
            {{--<a href="{{route('projects')}}"><li class="{{ $segment_1 == 'projects' ? 'active' : '' }}" id="mobi-projects">Projects</li></a>--}}
            <a href="{{route('team')}}"><li class="{{ $segment_1 == 'team' ? 'active' : '' }}" id="mobi-team">team</li></a>
            <a href="{{route('news')}}"><li class="{{ $segment_1 == 'news' ? 'active' : '' }}" id="mobi-news"> news</li></a>
            <a href="{{route('connect')}}"><li class="{{ $segment_1 == 'connect' ? 'active' : '' }}" id="mobi-connect"> connect</li></a>
            <a href="{{route('join_us')}}"><li class="{{ $segment_1 == 'join_us' ? 'active' : '' }}" id="mobi-joinus"> join us</li></a>
            {{--<li  class="{{ $segment_1 == 'login' ? 'active' : '' }} mb-4" id="mobi-login">Customer<br>Login</li>--}}


            <div class="float-left pt-2 pb-2 mr-3">
                <a href="https://www.facebook.com/1OAKrealtors/" target="_blank"> <div class="fb-icon social-media-icon"></div> </a>
            </div>
            <div class="float-left pt-2 pb-2 mr-3">
                <a href="https://www.instagram.com/1oakrealtors/" target="_blank"> <div class="ig-icon social-media-icon"></div> </a>
            </div>
            <div class="float-left pt-2 pb-2">
                <a href="https://twitter.com/1OAKRealtors/" target="_blank"> <div class="tw-icon social-media-icon"></div> </a>
            </div>
        </ul>
        {{--<div class="mobile_social_icons">--}}
          {{----}}
        {{--</div>--}}

        <div class="w-100 position-absolute color-white font10 pl-4 pb-2" style="bottom: 0px; padding-left: 2.1rem">
            <div class="mobile_social_icons">
                <a href="{{route('privacy-and-policy')}}"><li class="{{ $segment_1 == 'privacy-and-policy' ? 'active' : '' }} font11" id="mobi-pp" style="line-height: 20px; list-style: none">Privacy Policy</li></a>
                <a href="{{route('terms-and-conditions')}}"><li class="{{ $segment_1 == 'terms-and-conditions' ? 'active' : '' }} font11" id="mobi-tnc" style="line-height: 20px; list-style: none">Terms & Conditions</li></a>

            <div class="w-100 pb-1">
                Copyright 1OAK 2018
            </div>
            </div>
        </div>
    </div>
</nav>
{{--Mobile Nav End --}}


<label for="navi-toggle" class="navigation__button">
    <span class="navigation__icon"></span>
</label>