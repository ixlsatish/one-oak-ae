<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-132883428-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-132883428-1');
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', '1OAK-One of a Kind') }}</title>
    <link rel="icon" type="image/png" href="{{ asset('images/favicon.png') }}">
    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">--}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/parsley.css') }}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script>
        var HOST = '{{asset('')}}';
    </script>



    @yield('styles')

</head>

<body class="page-container align-center">
{{--<div class="div2" style="width:100%;height:100%;background-color:green;position:relative;z-index:99999999;"></div>--}}

@yield('video')

@include('layouts.nav-atmos')
@include('layouts.header')
<div class="main-body page-container align-center videoloader">

    @yield('content')
</div>
@include('layouts.footer')
<div class="d-none">
    <img class="lazy-load-image" src="" data-src="{{asset('images/news-bg.jpg')}}" alt="news bg">
    <img class="lazy-load-image" src="" data-src="{{asset('images/team-bg.jpg')}}" alt="Team bg">
    <img class="lazy-load-image" src="" data-src="{{asset('images/connect-bg.jpg')}}" alt="connect bg">
    <img class="lazy-load-image" src="" data-src="{{asset('images/bg.jpg')}}" alt="main bg">
    <img class="lazy-load-image" src="" data-src="{{asset('images/tnc-bg.jpg')}}" alt="terms and conditions bg">
    <img class="lazy-load-image" src="" data-src="{{asset('images/pp-bg.jpg')}}" alt="privacy and policy bg">
</div>
@yield('scripts')
<script type="text/javascript">
    window.onload = function () {
        $.each($('.lazy-load-image'),function(a,b){
            var c = $(b);
            c.attr('src',c.data('src'));
        });
    }
//    $('#newtest').on("scroll", function(){
//        if
//        ($(document).scrollTop() > 50){
//            $("#newtest").addClass("newtest");
//        }
//        else
//        {
//            $("header").removeClass("shrink");
//        }
//    });
</script>
<script src="{{ asset('js/common.js') }}"></script>

<script src="{{ asset('js/bootstrap.js') }}"></script>
<script src="{{ asset('js/parsley.min.js') }}"></script>
<script src="{{ asset('js/toastr.min.js') }}"></script>

{{--<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>--}}
<script src="{{ asset('js/chat.js') }}"></script>

</body>
</html>
