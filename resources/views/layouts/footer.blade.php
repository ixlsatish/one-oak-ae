<?php
$segment_1 = Request::segment(1);
?>


<footer class="w-100 fixed-bottom videoloader fadeInUp animated delay-2">
    <div class="footer-left-content float-left font10">

        <div class="color-white float-left pl-1 pt-2 pr-2 pb-2">Copyright 1OAK 2018</div>
        <div class="p-2 color-white float-left">
            <a href="{{route('privacy-and-policy')}}" class="{{ $segment_1 == 'privacy-and-policy' ? 'active' : '' }}">Privacy Policy</a>
        </div>
        <div class="p-2 color-white float-left">
            <a href="{{route('terms-and-conditions')}}"  class="{{ $segment_1 == 'terms-and-conditions' ? 'active' : '' }}">terms & conditions</a>
        </div>
    </div>

    <div class="footer-content pr-3 position-relative pr-mdtp-0">
        {{--<div class="chat-main">--}}
            {{--<div class="chat-btn float-left mobi-chat-btn">--}}
                {{--Live support chat--}}
                {{--support chat--}}
            {{--</div>--}}
            {{--<div class="chat-content color-gray">--}}
                {{--<div class="w-100 float-left bg-color-black color-white p-2 extralight">--}}
                    {{--You are online with Reiss Somers--}}
                {{--</div>--}}
                {{--<div class="chat-content-box">--}}
                    {{--<ul>--}}
                        {{--<li>--}}
                            {{--<div class="chat-dp">--}}
                                {{--<img src="{{asset('images/dp-img.jpg')}}">--}}
                            {{--</div>--}}
                            {{--<div class="chat-text-box">--}}
                                {{--Reiss: Hello there, how can I assist you?--}}

                                {{--<div class="timer-typing">--}}
                                    {{--<div class="timer">9.40am</div>--}}
                                    {{--<div class="typing d-none">Reiss is typing...</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li class="user">--}}
                            {{--<div class="chat-dp">--}}
                                {{--<img src="{{asset('images/user-dp.png')}}">--}}
                            {{--</div>--}}
                            {{--<div class="chat-text-box">--}}
                                {{--You: Hello, I’d like to know the price of a 1BHK please?--}}
                                {{--<div class="timer-typing">--}}
                                    {{--<div class="timer">9.40am</div>--}}
                                    {{--<div class="typing d-none">Reiss is typing...</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<div class="chat-dp">--}}
                                {{--<img src="{{asset('images/dp-img.jpg')}}">--}}
                            {{--</div>--}}
                            {{--<div class="chat-text-box">--}}
                                {{--Reiss: Hello there, how can I assist you?--}}
                                {{--<img src="{{asset('images/loading.gif')}}">--}}
                                {{--<div class="timer-typing">--}}
                                    {{--<div class="timer d-none">9.40am</div>--}}
                                    {{--<div class="typing">Reiss is typing...</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="w-75 float-left send-input">--}}
                    {{--<input type="text" class="form-control" placeholder="ENTER MASSAGE">--}}
                    {{--<textarea rows="3" cols="0" placeholder="ENTER MASSAGE"></textarea>--}}
                {{--</div>--}}
                {{--<div class="w-25 float-right">--}}
                    {{--<div class="btn send float-right font10">--}}
                        {{--SEND--}}
                    {{--</div>--}}
                {{--</div>--}}

            {{--</div>--}}
        {{--</div>--}}

        <div class="chat-main">
            {{--<div class="frame chat__box-main open-chat">--}}
            <div class="frame chat__box-main">
                <div class="panel panel-primary" style="z-index: 1000"><div id="chat-panel" class="panel-heading">Support Chat</div></div>

                <div class="innerframe" id="innerframe_chat">
                    <div class="color-black p-2">Hi, we are here to help you. Please enter your queries. </div>
                    <ul id="messages"></ul>
                    <div id="message-box">
                        <div class="msj-rta macro" style="margin:auto">
                            <div class="text text-l" style="background:white">
                                <input class="mytext" placeholder="Type a message"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="social-media float-right" style="visibility: hidden!important; opacity: 0!important;">
          <ul>
             <a href="https://www.facebook.com/1OAKrealtors/" target="_blank"> <li class="fb-icon"></li> </a>
              <a href="https://www.instagram.com/1oakrealtors/" target="_blank"> <li class="ig-icon"></li> </a>
               <a href="https://twitter.com/1OAKRealtors/" target="_blank"> <li class="tw-icon mr-0"></li> </a>
              <a href="https://www.facebook.com/1OAKrealtors/" target="_blank"> <li class="fb-icon"></li> </a>
              <a href="https://twitter.com/1OAKRealtors/" target="_blank"> <li class="tw-icon mr-0"></li> </a>
         </ul>
       </div>
    </div>

    <div class="mobile-footer">
        <div class="w-50 float-left text-center border-right mobi-chat pt-3"> support chat </div>
        <div class="w-50 float-left text-center mobi-register"> register your interest </div>
    </div>


</footer>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="{{asset('js/chat.js')}}"></script>