{{--<div class="landscape-popup page-container align-center">--}}
    {{--<div class="m-auto">--}}

    {{--</div>--}}
{{--</div>--}}

{{--Project Details Amenities--}}

<div class="amenities-popup popup-bg">
    <label for="navi-toggle" class="close__button" id="m-amenities-close">
        <span class="navigation__icon"><b class="brand-primary">&nbsp;</b></span>
    </label>

    <div class="align-items center-middle h-100 text-center">
        <div class="m-auto p-3 m-amenities" id="m-premium-appliances">
            <h4 class="brand-primary text-left"> Premium Home Appliances</h4>
            <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae felis
                non neque congue mattis. Etiam porta lectus risus, non sagittis eros
                pellentesque id. </p>
        </div>
        <div class="m-auto p-3 m-amenities" id="m-wardrobes">
            <h4 class="brand-primary text-left">Built-in Wardrobes</h4>
            <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae felis
                non neque congue mattis. Etiam porta lectus risus, non sagittis eros
                pellentesque id. </p>
        </div>
        <div class="m-auto p-3 m-amenities" id="m-fitness">
            <h4 class="brand-primary text-left">Fitness Centre</h4>
            <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae felis
                non neque congue mattis. Etiam porta lectus risus, non sagittis eros
                pellentesque id. </p>
        </div>
        <div class="m-auto p-3 m-amenities" id="m-wimming">
            <h4 class="brand-primary text-left">Swimming Pool</h4>
            <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae felis
                non neque congue mattis. Etiam porta lectus risus, non sagittis eros
                pellentesque id. </p>
        </div>
        <div class="m-auto p-3 m-amenities" id="mosques">
            <h4 class="brand-primary text-left">Mosques</h4>
            <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae felis
                non neque congue mattis. Etiam porta lectus risus, non sagittis eros
                pellentesque id. </p>
        </div>
        <div class="m-auto p-3 m-amenities" id="landscaped-gardens">
            <h4 class="brand-primary text-left">Landscaped Gardens</h4>
            <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae felis
                non neque congue mattis. Etiam porta lectus risus, non sagittis eros
                pellentesque id. </p>
        </div>
        <div class="m-auto p-3 m-amenities" id="elegant-lobby">
            <h4 class="brand-primary text-left">Elegant Lobby</h4>
            <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae felis
                non neque congue mattis. Etiam porta lectus risus, non sagittis eros
                pellentesque id. </p>
        </div>
        <div class="m-auto p-3 m-amenities" id="security">
            <h4 class="brand-primary text-left">24 Hour Security</h4>
            <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae felis
                non neque congue mattis. Etiam porta lectus risus, non sagittis eros
                pellentesque id. </p>
        </div>
    </div>
</div>
{{--Project Details Amenities--}}

<header class="w-100 fixed-top videoloader fadeInDown animated delay-1" id="js-header">
    <div class="header-content pr-5">
        <div class="register-main register-open" id="register-open">
            <div class="register-btn float-left mobi-xsl">
                Register your interest
            </div>
            <div class="register-content p-2 pt-sm-0">
                <form data-parsley-validate="true" method="post" name="register_form" id="register_form">
                <label for="navi-toggle" class="close__button mobi-show">
                    <span class="navigation__icon"><b class="brand-primary">&nbsp;</b></span>
                </label>
                <div class="w-100 position-relative mobi-show pt-4 text-center">
                    <img src="{{asset('images/m-logo.png')}}">
                </div>
                <h5 class="w-100 text-center color-white text-uppercase pt-2 mobi-show">Register your interest</h5>
                <div class="w-100 text-center-sm text-left pt-2">
                    All fields are mandatory
                </div>
                <div class="w-100 float-left">
                    {{--<div class="w-50 float-left pr-1 pt-sm-1 pr-0-sm">--}}
                    {{--</div>--}}
                    <div class="w-50 float-left pr-1 pt-sm-1 pr-0-sm"><input data-parsley-required="true"
                                                                             data-parsley-maxlength="20"
                                                                             data-parsley-maxlength-message="Name Should Be Less Than 20 Characters"
                                                                             data-parsley-pattern="^[A-Za-z]*$"
                                                                             data-parsley-pattern-message="Enter Characters Only" type="text" class="form-control" placeholder="First Name" tabindex="1" name="first_name_h" id="first_name_h">
                    </div>
                    <div class="w-50 float-left pl-1 pt-sm-1 pl-sm-0"><input data-parsley-required="true"
                                                                             data-parsley-minlength="20"
                                                                             data-parsley-minlength-message="Name Should Be Less Than 20 Characters"
                                                                             data-parsley-pattern="^[A-Za-z]*$"
                                                                             data-parsley-pattern-message="Enter Characters Only" type="text" class="form-control" placeholder="Last Name" tabindex="2" name="last_name_h" id="last_name_h"></div>
                    <div class="w-100 float-left pt-2"><input data-parsley-required="true"
                                                              data-parsley-required-message="Please Enter Email Id"
                                                              data-parsley-type="email"
                                                              data-parsley-type-message="Please Enter A Valid Email" type="text" class="form-control" placeholder="EMAIL ID" tabindex="3" name="email_h" id="email_h"></div>

                    <div class="w-100 float-left pt-2">
                        <div class="w-25 float-left styled-select">
                            <input data-parsley-required="true" class="form-control" tabindex="4" id="mobile_code_h" value="+91">
                        </div>
                        <div class="w-75 float-left pl-1"><input data-parsley-required="true"
                                                                 data-parsley-required-message="Please Enter Mobile Number"
                                                                 data-parsley-type="number"
                                                                 data-parsley-type-message="Please Enter Valid Number"
                                                                 data-parsley-minlength="10"
                                                                 data-parsley-minlength-message="Please Enter Valid Mobile Number"
                                                                 data-parsley-maxlength="10"
                                                                 data-parsley-maxlength-message="Please Enter No More Than 10 Digits" type="text" class="form-control" placeholder="Phone Number" tabindex="5" name="mobile_number_h" id="mobile_number_h" maxlength="10"></div>
                    </div>
                    <div class="w-100 float-left pt-2">
                        <div class="w-100 float-left styled-select">
                            <select data-parsley-required="true" class="color-gray padding" tabindex="6" name="project_h" id="project_h">
                                <option value="" selected>PROJECT OF INTEREST</option>
                                <option value="atmos">ATMOS</option>
                                <option value="rootsat36">Rootsat36</option>
                                <option value="aspira-koh-samui">Aspira Koh Samui</option>
                                <option value="carlow-house">Carlow House</option>
                                <option value="ten-ekamai-suites">Ten Ekamai Suites</option>
                                <option value="grand-swiss">Grand Swiss</option>
                                <option value="kingsland-hotel">Kingsland Hotel</option>
                                <option value="hotel-du-parc">Hotel Du Parc</option>
                                <option value="fulcrum-karin">Fulcrum Karin</option>
                                <option value="fulcrum-anona">Fulcrum Anona</option>
                                <option value="suntec-city">Suntec City</option>
                                <option value="art-patong">Art Patong</option>
                            </select>
                        </div>
                    </div>

                    <div class="w-100 float-left pt-3 font10">
                        <label class="rad color-white text-uppercase pr-2">
                            <input  type="radio" name="r1_h" value="a" id="book_a_visit" checked/>
                            <i class="mr-1" style="margin-top: -2px;"></i> <span class="pt-1">Book a site visit</span>
                        </label>
                        <label class="rad color-white text-uppercase">
                            <input data-parsley-required="true" data-parsley-required-message="Please Select Atleast One Option" type="radio" name="r1_h" value="b" id="request_a_callback" />
                            <i class="mr-1" style="margin-top: -2px;"></i> Request a call back
                        </label>
                    </div>

                    <div class="w-100 float-left text-center pt-2">
                        {{csrf_field()}}
                        <button class="btn" type="button"  id="header_form">
                            Submit
                        </button>
                    </div>

                </div>
                </form>
            </div>
        </div>
        <div class="language-main float-right ml-3 mt-3">
            <div class="float-left text-hover">
                <a href="http://www.oneoak.in" class="color-white cursor-pointer">INDIA</a>
            </div>
            <div class="float-left pr-3 pl-3">|</div>
            <div class="float-left text-hover active brand-primary">UAE</div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
//            var country;
//            $.getJSON("http://freegeoip.net/json/", function (data) {
//                country = data.country_name;
//                var ip = data.ip;
////                console.log(country);
//            });
//            if(country='India')
//            {
//                document.getElementById('mobile_code_h').value = "+91";
//                document.getElementById('mobile_code_h').setAttribute("disabled","disabled");
//                document.getElementById('mobile_code_h').setAttribute("checked","checked");
//            }
//            else if(country='Dubai')
//            {
//                document.getElementById('mobile_code_h').value = "+972";
//                document.getElementById('mobile_code_h').setAttribute("disabled","disabled");
//                document.getElementById('mobile_code_h').setAttribute("checked","checked");
//
//            }
            $("#header_form").click(function (e) {
                $("#header_form").attr("disabled", "disabled");
                setTimeout(function() {
                    $("#header_form").removeAttr("disabled");
                }, 6000);
                var first_name_h=$('#first_name_h').val();
                var last_name_h=$('#last_name_h').val();
                var email_h=$('#email_h').val();
                var mobile_code_h=$('#mobile_code_h').val();
                var mobile_number_h=$('#mobile_number_h').val();
                var project_h=$('#project_h option:selected').text();
//                alert(project_h);

                var radio_h=$('input[type=radio][name=r1_h]:checked').attr('id');

                var ua = navigator.userAgent.toLowerCase();
                var isAndroid = ua.indexOf("android") > -1;
                var isIphone = ua.indexOf("iphone") > -1;
                var isIpod = ua.indexOf("ipod") > -1;
                var isIpad = ua.indexOf("ipad") > -1;
                var isBB = ua.indexOf("blackberry") > -1;
                var isBB2 = ua.indexOf("rim") > -1;
                var isBB = ua.indexOf("blackberry") > -1;
                var isBB2 = ua.indexOf("RIM") > -1;
                var isSymbian = ua.indexOf("symbian") > -1;
                var isNokia = ua.indexOf("nokia") > -1;
                if (isNokia || isSymbian || isBB || isBB2 || isIphone || isIpod||isIpad||isAndroid) {
                    var src_h="Mobile Website";
                } else
                    src_h="Website";

                var messages="";
                $.ajax(
                    {
                        url: '{{route('header-form')}}',
                        data: {first_name_h:first_name_h,last_name_h:last_name_h,email_h:email_h,mobile_code_h:mobile_code_h,mobile_number_h:mobile_number_h,project_h:project_h,radio_h:radio_h,src_h:src_h},
                        type: 'GET',
                        success: function (data) {
                            console.log('success',data);
                            if(data.code==1) {
//                                    for (i = 0; i < data.message.length; i++) {
//                                        text += data.message[i] + "<br>";
//                                    }
                                toastr.success("Thank you for your interest. Our team will contact you soon.",  {timeOut: 5000});
                                var frm = document.getElementsByName('register_form')[0];
                                frm.reset();
                            }

                            else
                            {
                                for (i = 0; i < data.message.length; i++) {
                                    messages+= data.message[i] + "<br>";
                                }
                                toastr.error(messages,  {timeOut: 5000})
                                //toastr.error('You Got Error', 'Inconceivable!', {timeOut: 5000})

                            }
                        },
                        error: function (error) {
                            toastr.error('You Got Error',  {timeOut: 5000});
//                            console.log('error');
                        }
                    }
                );
            });
        });

    </script>
</header>


