<div class="tab-pane fade" id="floorplans-tab04" role="tabpanel"
     aria-labelledby="floorplans-tab">
    <div class="w-100 float-left floorplans-main">
        <h4 class="float-left w-100 mobi-show"> Project Plan</h4>
        <div class="tab-content">

            <div id="site-plan" class="tab-pane fade">
                <div id="bedroom-1" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#bedroom-1" data-slide-to="0" class="active"></li>
                    </ul>
                    <!-- The slideshow -->
                    <div class="carousel-inner floor_swipe">
                        <div class="carousel-item active">
                            <img src="{{asset('images/floor-plans/floorplans-01.jpg')}}" alt="3bhk"
                                 class="w-100">
                        </div>
                    </div>
                </div>
            </div>

            <div id="master-plan" class="tab-pane fade active show">
                <div id="bedroom-2" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#bedroom-2" data-slide-to="0" class="active"></li>
                    </ul>
                    <!-- The slideshow -->
                    <div class="carousel-inner floor_swipe">
                        <img src="{{asset('images/floor-plans/floorplans-01.jpg')}}" alt="3bhk"
                             class="w-100">
                    </div>
                </div>
            </div>

            <div id="floor-plan" class="tab-pane fade">
                <div id="floor" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#floor" data-slide-to="0" class="active"></li>
                        <li data-target="#floor" data-slide-to="1"></li>
                        <li data-target="#floor" data-slide-to="2"></li>
                        <li data-target="#floor" data-slide-to="3"></li>
                        <li data-target="#floor" data-slide-to="4"></li>
                    </ul>
                    <!-- The slideshow -->
                    <div class="carousel-inner floor_swipe" id="floor">
                        <div class="carousel-item active">
                            <img src="{{asset('images/floor-plans/floorplans-01.jpg')}}" alt="3bhk"
                                 class="w-100">
                        </div>
                        <div class="carousel-item">
                            <img src="{{asset('images/floor-plans/floorplans-02.jpg')}}" alt="master-plan"
                                 class="w-100">
                        </div>
                        <div class="carousel-item">
                            <img src="{{asset('images/floor-plans/floorplans-03.jpg')}}" alt="earth-villa-lower"
                                 class="w-100">
                        </div>
                        <div class="carousel-item">
                            <img src="{{asset('images/floor-plans/floorplans-04.jpg')}}" alt="earth-villa-upper"
                                 class="w-100">
                        </div>
                        <div class="carousel-item">
                            <img src="{{asset('images/floor-plans/floorplans-05.jpg')}}" alt="site-plan"
                                 class="w-100">
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="carousel-control-prev prev previous" href="#floor"
                       data-slide="prev"
                       id="previous">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next next" href="#floor" data-slide="next"
                       id="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>
                </div>
            </div>
        </div>

        <ul class="nav floorplans-tabs">
            <li><a data-toggle="tab" href="#master-plan" class="active show"> Master Plan</a></li>
            <li><a data-toggle="tab" href="#site-plan">Site Plan</a></li>
            <li class="border-right-0"><a data-toggle="tab"  href="#floor-plan">Floor Plan</a></li>
        </ul>

    </div>
</div>