<?php
$segment_1 = Request::segment(1);
$segment_2 = Request::segment(2);
?>


{{-- Website Nav Start --}}
<nav class="videoloader fadeInRight animated delay-3 atmos-nav">
    <ul id="nav-bar" class="">
        <li id="home"><a href="{{route('lucknow.index')}}" class="{{ $segment_1 == '' ? 'active' : '' }}">Atmos</a></li>
        <li id="gallery"><a href="{{route('lucknow.gallery')}}" class="{{ $segment_1 == 'gallery' ? 'active' : '' }}">Gallery</a>
        </li>
        <li id="location"><a href="{{route('lucknow.location')}}"
                             class="{{ $segment_1 == 'location' ? 'active' : '' }}">Location</a></li>
        <li id="amenities"><a href="{{route('lucknow.amenities')}}"
                              class="{{ $segment_1 == 'amenities' ? 'active' : '' }}">Amenities</a></li>
        <li id="project-plan"><a href="{{route('lucknow.project-plan')}}"
                                 class="{{ $segment_1 == 'project-plan' ? 'active' : '' }}">Project Plan</a></li>
        <li id="unit-plan"><a href="{{route('lucknow.unit-plan')}}"
                              class="{{ $segment_1 == 'unit-plan' ? 'active' : '' }}">Unit Plan</a></li>
        <li id="news"><a href="{{route('lucknow.news')}}" class="{{ $segment_1 == 'news' ? 'active' : '' }}">news</a>
        </li>
        <li id="connect"><a href="{{route('lucknow.connect')}}" class="{{ $segment_1 == 'connect' ? 'active' : '' }}">Connect</a>
        </li>
        <li id="joinus"><a href="{{route('lucknow.join_us')}}" class="{{ $segment_1 == 'join_us' ? 'active' : '' }}">Join
                Us</a></li>
        {{--<li id="login"><a href="{{route('login')}}">Customer Login</a></li>--}}
        {{--<li id="login">Customer Login</li>--}}
    </ul>
</nav>
{{-- Website Nav End --}}

{{--Mobile Nav Start --}}
<nav class="videoloader animated mobile-nav">
    <div class="mobi-show menu-close"></div>
    <div class="mobile-bg">
        <label for="navi-toggle" class="close__button" id="menu-close">
            <span class="navigation__icon"><b class="brand-primary">&nbsp;</b></span>
        </label>
        {{--<div class="w-100 float-left mt-3 ml-4">
            <div onclick="location.href='{{route('index')}}'" class="float-left text-hover active brand-primary" style="text-decoration: underline">ENG</div>
            <div class="float-left pr-3 pl-3">|</div>
            <div onclick="location.href='{{route('thai-language.index')}}'" class="float-left text-hover">THAI</div>
        </div>--}}
        <div class="w-100 float-left mt-3 ml-4">
            <div class="float-left text-hover active brand-primary">INDIA</div>
            <div class="float-left pr-3 pl-3">|</div>
            <div class="float-left text-hover">
                <a href="javascript:void(0)" class="color-white cursor-pointer">UAE</a>
            </div>
        </div>

        <ul id="nav-bar" class="">
            <a href="{{route('lucknow.index')}}">
                <li class="{{ $segment_1.'/'.$segment_2 == 'atmos_lucknow/' ? 'active' : '' }}" id="mobi-atmos-home">Atmos</li>
            </a>
            <a href="{{route('lucknow.gallery')}}">
                <li class="{{ $segment_1.'/'.$segment_2  == 'atmos_lucknow/gallery' ? 'active' : '' }}" id="mobi-gallery">Gallery</li>
            </a>
            <a href="{{route('lucknow.location')}}">
                <li class="{{ $segment_1.'/'.$segment_2 == 'atmos_lucknow/location' ? 'active' : '' }}" id="mobi-location">Location</li>
            </a>
            <a href="{{route('lucknow.amenities')}}">
                <li class="{{ $segment_1.'/'.$segment_2 == 'atmos_lucknow/amenities' ? 'active' : '' }}" id="mobi-amenities">Amenities</li>
            </a>
            <a href="{{route('lucknow.project-plan')}}">
                <li class="{{ $segment_1.'/'.$segment_2 == 'atmos_lucknow/project-plan' ? 'active' : '' }}" id="mobi-project-plan">Project Plan</li>
            </a>
            <a href="{{route('lucknow.unit-plan')}}">
                <li class="{{ $segment_1.'/'.$segment_2 == 'atmos_lucknow/unit-plan' ? 'active' : '' }}" id="mobi-unit-plan">Unit Plan</li>
            </a>
            <a href="{{route('lucknow.news')}}">
                <li class="{{ $segment_1.'/'.$segment_2 == 'atmos_lucknow/news' ? 'active' : '' }}" id="mobi-news"> news</li>
            </a>
            <a href="{{route('lucknow.connect')}}">
                <li class="{{ $segment_1.'/'.$segment_2 == 'atmos_lucknow/connect' ? 'active' : '' }}" id="mobi-connect"> Connect</li>
            </a>
            <a href="{{route('lucknow.join_us')}}">
                <li class="{{ $segment_1.'/'.$segment_2 == 'atmos_lucknow/join_us' ? 'active' : '' }}" id="mobi-joinus"> Join Us</li>
            </a>
            {{--<li  class="{{ $segment_1 == 'login' ? 'active' : '' }} mb-4" id="mobi-login">Customer<br>Login</li>--}}

            {{--<div class="float-left w-100 pt-2 pb-2">--}}
            {{--<div class="fb-icon social-media-icon"></div>--}}
            {{--</div>--}}
            {{--<div class="float-left w-100 pt-2 pb-2">--}}
            {{--<div class="ig-icon social-media-icon"></div>--}}
            {{--</div>--}}
            {{--<div class="float-left w-100 pt-2 pb-2">--}}
            {{--<div class="tw-icon social-media-icon"></div>--}}
            {{--</div>--}}
        </ul>

        <div class="w-100 position-absolute color-white font10 pl-4 pb-2" style="bottom: 0px; padding-left: 2.1rem">
            <div class="mobile_social_icons">
                <a href="{{route('privacy-and-policy')}}">
                    <li class="{{ $segment_1 == 'privacy-and-policy' ? 'active' : '' }} font11" id="mobi-pp"
                        style="line-height: 20px; list-style: none">Privacy Policy
                    </li>
                </a>
                <a href="{{route('terms-and-conditions')}}">
                    <li class="{{ $segment_1 == 'terms-and-conditions' ? 'active' : '' }} font11" id="mobi-tnc"
                        style="line-height: 20px; list-style: none">Terms & Conditions
                    </li>
                </a>
                <div class="w-100 pb-1">
                    Copyright Atmos 2018
                </div>
            </div>
        </div>
    </div>
</nav>
{{--Mobile Nav End --}}


<label for="navi-toggle" class="navigation__button">
    <span class="navigation__icon"></span>
</label>