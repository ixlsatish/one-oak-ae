@extends('layouts.app')

@section('video')
    <video class="craft-beer-landing-vidbg testing" muted autoplay loop>
        <source src="{{ asset('videos/1OAK-ProjectsPage_2.mp4') }}" type="video/mp4">
    </video>
@endsection

@section('content')
    <section class="projects-page pt-4 pr-2 pb-4 center-middle align-items pr-mdtp-0">
        <div class="inner-logo text-center position-absolute pr-4">
            <a href="{{route('index')}}"><img src="{{asset('images/logo2.png')}}"></a>
        </div>
        {{--<div class="col-md-12 pl-0 pr-mdtp-0">--}}
            {{--<div class="w-100 section-pl-12 pl-mdtp-0">--}}
                {{--<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">--}}
                    {{----}}
                    {{----}}
                    {{--<div class="carousel-inner project-main center-middle align-items" role="listbox">--}}
                        {{--<div class="carousel-item active">--}}
                            {{--<ul align="center" class="all-project float-left-mdtp">--}}
                                {{--<li class="project-no1 zoomIn animated project-delay1">--}}
                                    {{--<a href="{{route('project-details')}}">--}}
                                        {{--<div class="project-name project-no1">--}}
                                            {{--<div class="text-center font14 project-no1">--}}
                                                {{--Desert heights--}}
                                            {{--</div>--}}
                                            {{--<div class="text-center font11 project-no1">--}}
                                                {{--Dubai Marina--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no1">--}}
                                        {{--<img src="{{asset('images/desert-heights-project.jpg')}}" class="project-overlay project-no1">--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="project-no2 zoomIn animated project-delay2">--}}
                                    {{--<a href="{{route('project-details')}}">--}}
                                        {{--<div class="project-name project-no2">--}}
                                            {{--<div class="text-center font14 project-no2">--}}
                                                {{--Accent heights--}}
                                            {{--</div>--}}
                                            {{--<div class="text-center font11 project-no2">--}}
                                                {{--Sheikh Zayed Road--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no2">--}}
                                        {{--<img src="{{asset('images/accent-heights-project.jpg')}}" class="project-overlay project-no2">--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="project-no3 zoomIn animated project-delay3">--}}
                                    {{--<a href="{{route('project-details')}}">--}}
                                        {{--<div class="project-name project-no3">--}}
                                            {{--<div class="text-center font14 project-no3">--}}
                                                {{--country annex--}}
                                            {{--</div>--}}
                                            {{--<div class="text-center font11 project-no3">--}}
                                                {{--Dubai Silicon Oasis--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no3">--}}
                                        {{--<img src="{{asset('images/country-annex-project.jpg')}}" class="project-overlay project-no3">--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="project-no4 zoomIn animated project-delay4">--}}
                                    {{--<a href="{{route('project-details')}}">--}}
                                        {{--<div class="project-name project-no4">--}}
                                            {{--<div class="text-center font14 project-no4">--}}
                                                {{--The loft--}}
                                            {{--</div>--}}
                                            {{--<div class="text-center font11 project-no4">--}}
                                                {{--Dubai studio city--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no4">--}}
                                        {{--<img src="{{asset('images/the-loft-project.jpg')}}" class="project-overlay project-no4">--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="project-no5 zoomIn animated project-delay5">--}}
                                    {{--<a href="{{route('project-details')}}">--}}
                                        {{--<div class="project-name project-no5">--}}
                                            {{--<div class="text-center font14 project-no5">--}}
                                                {{--Alexa towers--}}
                                            {{--</div>--}}
                                            {{--<div class="text-center font11 project-no5">--}}
                                                {{--Barsha Heights--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no5">--}}
                                            {{--<img src="{{asset('images/alexa-towers-project.jpg')}}" class="project-overlay project-no5">--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="project-no6 zoomIn animated project-delay6">--}}
                                    {{--<a href="{{route('project-details')}}">--}}
                                        {{--<div class="project-name project-no6">--}}
                                            {{--<div class="text-center font14 project-no6">--}}
                                                {{--stardust--}}
                                            {{--</div>--}}
                                            {{--<div class="text-center font11 project-no6">--}}
                                                {{--Jumeirah Lakes towers--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no6">--}}
                                            {{--<img src="{{asset('images/stardust-project.jpg')}}" class="project-overlay project-no6">--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="project-no7 zoomIn animated project-delay7">--}}
                                    {{--<a href="{{route('project-details')}}">--}}
                                        {{--<div class="project-name project-no7">--}}
                                            {{--<div class="text-center font14 project-no7">--}}
                                                {{--mujin--}}
                                            {{--</div>--}}
                                            {{--<div class="text-center font11 project-no7">--}}
                                                {{--Business Bay--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no7">--}}
                                            {{--<img src="{{asset('images/mujin-project.jpg')}}" class="project-overlay project-no7">--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="project-no8 zoomIn animated project-delay8">--}}
                                    {{--<a href="{{route('project-details')}}">--}}
                                        {{--<div class="project-name project-no8">--}}
                                            {{--<div class="text-center font14 project-no8">--}}
                                                {{--ATMOS--}}
                                            {{--</div>--}}
                                            {{--<div class="text-center font11 project-no8">--}}
                                                {{--Lucknow--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no8">--}}
                                            {{--<img src="{{asset('images/corella-project.jpg')}}" class="project-overlay project-no8">--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                        {{--<div class="carousel-item">--}}
                            {{--<ul align="center" class="all-project more-four-project">--}}
                                {{--<li class="project-no1 zoomIn animated project-delay1">--}}
                                    {{--<a href="{{route('project-details')}}">--}}
                                        {{--<div class="project-name project-no1">--}}
                                            {{--<div class="text-center font14 project-no1">--}}
                                                {{--Desert heights--}}
                                            {{--</div>--}}
                                            {{--<div class="text-center font11 project-no1">--}}
                                                {{--Dubai Marina--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no1">--}}
                                        {{--<img src="{{asset('images/desert-heights-project.jpg')}}" class="project-overlay project-no1">--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="project-no2 zoomIn animated project-delay2">--}}
                                    {{--<a href="{{route('project-details')}}">--}}
                                        {{--<div class="project-name project-no2">--}}
                                            {{--<div class="text-center font14 project-no2">--}}
                                                {{--Accent heights--}}
                                            {{--</div>--}}
                                            {{--<div class="text-center font11 project-no2">--}}
                                                {{--Sheikh Zayed Road--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no2">--}}
                                        {{--<img src="{{asset('images/accent-heights-project.jpg')}}" class="project-overlay project-no2">--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="project-no3 zoomIn animated project-delay3">--}}
                                    {{--<a href="{{route('project-details')}}">--}}
                                        {{--<div class="project-name project-no3">--}}
                                            {{--<div class="text-center font14 project-no3">--}}
                                                {{--country annex--}}
                                            {{--</div>--}}
                                            {{--<div class="text-center font11 project-no3">--}}
                                                {{--Dubai Silicon Oasis--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no3">--}}
                                        {{--<img src="{{asset('images/country-annex-project.jpg')}}" class="project-overlay project-no3">--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="project-no4 zoomIn animated project-delay4">--}}
                                    {{--<a href="{{route('project-details')}}">--}}
                                        {{--<div class="project-name project-no4">--}}
                                            {{--<div class="text-center font14 project-no4">--}}
                                                {{--The loft--}}
                                            {{--</div>--}}
                                            {{--<div class="text-center font11 project-no4">--}}
                                                {{--Dubai studio city--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no4">--}}
                                        {{--<img src="{{asset('images/the-loft-project.jpg')}}" class="project-overlay project-no4">--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="project-no5 zoomIn animated project-delay5">--}}
                                    {{--<a href="{{route('project-details')}}">--}}
                                        {{--<div class="project-name project-no5">--}}
                                            {{--<div class="text-center font14 project-no5">--}}
                                                {{--Alexa towers--}}
                                            {{--</div>--}}
                                            {{--<div class="text-center font11 project-no5">--}}
                                                {{--Barsha Heights--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no5">--}}
                                        {{--<img src="{{asset('images/alexa-towers-project.jpg')}}" class="project-overlay project-no5">--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                        {{--<div class="carousel-item">--}}
                            {{--<ul align="center" class="all-project below-three-project">--}}
                                {{--<li class="project-no1 zoomIn animated project-delay1">--}}
                                    {{--<a href="{{route('project-details')}}">--}}
                                        {{--<div class="project-name project-no1">--}}
                                            {{--<div class="text-center font14 project-no1">--}}
                                                {{--Desert heights--}}
                                            {{--</div>--}}
                                            {{--<div class="text-center font11 project-no1">--}}
                                                {{--Dubai Marina--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no1">--}}
                                        {{--<img src="{{asset('images/desert-heights-project.jpg')}}" class="project-overlay project-no1">--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li class="project-no2 zoomIn animated project-delay2">--}}
                                    {{--<a href="{{route('project-details')}}">--}}
                                        {{--<div class="project-name project-no2">--}}
                                            {{--<div class="text-center font14 project-no2">--}}
                                                {{--Accent heights--}}
                                            {{--</div>--}}
                                            {{--<div class="text-center font11 project-no2">--}}
                                                {{--Sheikh Zayed Road--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no2">--}}
                                        {{--<img src="{{asset('images/accent-heights-project.jpg')}}" class="project-overlay project-no2">--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">--}}
                        {{--<span class="carousel-control-prev-icon" aria-hidden="true"></span>--}}
                        {{--<span class="sr-only">Previous</span>--}}
                    {{--</a>--}}
                    {{--<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">--}}
                        {{--<span class="carousel-control-next-icon" aria-hidden="true"></span>--}}
                        {{--<span class="sr-only">Next</span>--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}



        <div class="col-md-12 pl-0 pr-mdtp-0">
            <div class="w-100 section-pl-12 pl-mdtp-0">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">

                    <ul class="carousel-indicators" style="bottom: -20px; width: auto">
                        <li data-target="#floor" data-slide-to="0" class="active"></li>
                        <li data-target="#floor" data-slide-to="1" class=""></li>
                    </ul>

                    <div class="carousel-inner project-main center-middle align-items" role="listbox">
                        <div class="carousel-item active">
                            <ul align="center" class="all-project float-left-mdtp">
                                <li class="project-no1 zoomIn animated project-delay1">
                                    <a href="{{route('project-1')}}">
                                        <div class="project-name project-no3">
                                            <div class="text-center font14 project-no3">
                                                Rootsat36
                                            </div>
                                            <div class="text-center font11 project-no3">
                                                DUBAI
                                            </div>
                                        </div>
                                        <img src="{{asset('images/project-normal-img.png')}}"
                                             class="project-normal project-no3">
                                        <img src="{{asset('images/country-annex-project.jpg')}}"
                                             class="project-overlay project-no3">
                                    </a>
                                </li>
                                <li class="project-no2 zoomIn animated project-delay2">
                                    <a href="{{route('project-details')}}">
                                        <div class="project-name project-no8">
                                            <div class="text-center font14 project-no8">
                                                ATMOS
                                            </div>
                                            <div class="text-center font11 project-no8">
                                                Lucknow
                                            </div>
                                        </div>
                                        <img src="{{asset('images/project-normal-img.png')}}"
                                             class="project-normal project-no8">
                                        <img src="{{asset('images/corella-project.jpg')}}"
                                             class="project-overlay project-no8">
                                    </a>
                                </li>
                                <li class="project-no3 zoomIn animated project-delay3">
                                    <a href="{{route('project-9')}}">
                                        <div class="project-name project-no1">
                                            <div class="text-center font14 project-no1">
                                                Aspira Koh Samui
                                            </div>
                                            <div class="text-center font11 project-no1">
                                                Samui
                                            </div>
                                        </div>
                                        <img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no1">
                                        <img src="{{asset('images/desert-heights-project.jpg')}}" class="project-overlay project-no1">
                                    </a>
                                </li>
                                <li class="project-no4 zoomIn animated project-delay4">
                                    <a href="{{route('project-3')}}">
                                        <div class="project-name project-no3">
                                            <div class="text-center font14 project-no3">
                                                Carlow House
                                            </div>
                                            <div class="text-center font11 project-no3">
                                                London
                                            </div>
                                        </div>
                                        <img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no3">
                                        <img src="{{asset('images/country-annex-project.jpg')}}" class="project-overlay project-no3">
                                    </a>
                                </li>
                                <li class="project-no5 zoomIn animated project-delay5">
                                    <a href="{{route('project-4')}}">
                                        <div class="project-name project-no4">
                                            <div class="text-center font14 project-no4">
                                                Ten Ekamai Suites
                                            </div>
                                            <div class="text-center font11 project-no4">
                                                Bangkok
                                            </div>
                                        </div>
                                        <img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no4">
                                        <img src="{{asset('images/the-loft-project.jpg')}}" class="project-overlay project-no4">
                                    </a>
                                </li>
                                <li class="project-no6 zoomIn animated project-delay6">
                                    <a href="{{route('project-5')}}">
                                        <div class="project-name project-no5">
                                            <div class="text-center font14 project-no5">
                                                Grand Swiss
                                            </div>
                                            <div class="text-center font11 project-no5">
                                                Bangkok
                                            </div>
                                        </div>
                                        <img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no5">
                                        <img src="{{asset('images/alexa-towers-project.jpg')}}" class="project-overlay project-no5">
                                    </a>
                                </li>
                                <li class="project-no7 zoomIn animated project-delay7">
                                    <a href="{{route('project-7')}}">
                                        <div class="project-name project-no7">
                                            <div class="text-center font14 project-no7">
                                                Kingsland Hotel
                                            </div>
                                            <div class="text-center font11 project-no7">
                                                London
                                            </div>
                                        </div>
                                        <img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no7">
                                        <img src="{{asset('images/mujin-project.jpg')}}" class="project-overlay project-no7">
                                    </a>
                                </li>
                                <li class="project-no8 zoomIn animated project-delay8">
                                    <a href="{{route('project-8')}}">
                                        <div class="project-name project-no8">
                                            <div class="text-center font14 project-no8">
                                                Hotel Du Parc
                                            </div>
                                            <div class="text-center font11 project-no8">
                                                Baden
                                            </div>
                                        </div>
                                        <img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no8">
                                        <img src="{{asset('images/corella-project.jpg')}}" class="project-overlay project-no8">
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="carousel-item">
                            <ul align="center" class="all-project more-four-project">
                                <li class="project-no1 zoomIn animated project-delay1">
                                    <a href="{{route('project-11')}}">
                                        <div class="project-name project-no1">
                                            <div class="text-center font14 project-no1">
                                                Fulcrum Karin
                                            </div>
                                            <div class="text-center font11 project-no1">
                                                Bangkok
                                            </div>
                                        </div>
                                        <img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no1">
                                        <img src="{{asset('images/desert-heights-project.jpg')}}" class="project-overlay project-no1">
                                    </a>
                                </li>
                                <li class="project-no2 zoomIn animated project-delay2">
                                    <a href="{{route('project-6')}}">
                                        <div class="project-name project-no6">
                                            <div class="text-center font14 project-no6">
                                                Fulcrum Anona
                                            </div>
                                            <div class="text-center font11 project-no6">
                                                Bangkok
                                            </div>
                                        </div>
                                        <img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no6">
                                        <img src="{{asset('images/stardust-project.jpg')}}" class="project-overlay project-no6">
                                    </a>
                                </li>
                                <li class="project-no3 zoomIn animated project-delay3">
                                    <a href="{{route('project-2')}}">
                                        <div class="project-name project-no2">
                                            <div class="text-center font14 project-no2">
                                                Suntec City
                                            </div>
                                            <div class="text-center font11 project-no2">
                                                Singapore
                                            </div>
                                        </div>
                                        <img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no2">
                                        <img src="{{asset('images/accent-heights-project.jpg')}}" class="project-overlay project-no2">
                                    </a>
                                </li>
                                <li class="project-no4 zoomIn animated project-delay4">
                                    <a href="{{route('project-10')}}">
                                        <div class="project-name project-no2">
                                            <div class="text-center font14 project-no2">
                                                Art Patong
                                            </div>
                                            <div class="text-center font11 project-no2">
                                                Phuket
                                            </div>
                                        </div>
                                        <img src="{{asset('images/project-normal-img.png')}}" class="project-normal project-no2">
                                        <img src="{{asset('images/accent-heights-project.jpg')}}" class="project-overlay project-no2">
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    {{--<a class="carousel-control left pt-3" href="#myCarousel"--}}
                    {{--data-slide="prev"><i class="fa fa-chevron-left"></i></a>--}}
                    {{--<a class="carousel-control right pt-3" href="#myCarousel" data-slide="next"><i--}}
                    {{--class="fa fa-chevron-right"></i></a>--}}

                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <!-- Onload remove register open class Start -->
    <script>
        $(document).ready(function() {
            $('.register-main').removeClass('register-open');
        });
    </script>
    <!-- Onload remove register open class End -->
@endsection