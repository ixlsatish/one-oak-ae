@extends('layouts.app')

@section('video')
{{--    <div class="home-bg" style="background-image: url({{asset('images/fulcrum-website-images-14.jpg')}});"></div>--}}
    <div class="home-bg" style="background-image: url({{asset('images/news-bg.jpg')}});"></div>
@endsection

@section('content')
    <div class="page-bg corella-bg page-container align-center fadeIn animated delay-0 mdtp-flex-none">
        <section class="project-details-page bg-color-black-op6 center-middle align-items position-relative fadeInLeft animated delay-4 mdtp-flex-none">

            {{--<div class="w-100 float-left ">--}}
            <div class="inner-logo text-right position-absolute pr-4">
                <a href="{{route('index')}}"><img src="{{asset('images/logo2.png')}}" alt="logo"></a>
            </div>
            <div class="col-md-6 pl-0">
                <div class="section-pl-25">
                    <div class="w-100 float-left">
                        <h1 class="section-title mt-2 float-left pr-2">
                            Carlow House
                        </h1>
                    </div>
                    <div class="content-scrollbar w-100 float-left mdtp-w-70 mdtp-float-right mb-4" style="overflow: auto">
                        <h2 class="font15">CATEGORY </h2>
                        <p>
                            LUXURY LOFT-STYLE SERVICE APARTMENTS
                        </p>
                        <p></p>

                        <h2 class="font15">PROPERTY & LOCATION </h2>
                        <p>
                            Situated in the heart of London near to Regent Park, the property holds 85 units 1 & 2 bed apartments. It has a magnificent four-storey atrium with a daytime concierge. The location of the property is perfectly placed for a high end living – both, short & long terms stay. It has amazing connectivity and is walking distance to London’s most prestigious universities and urban developments.
                        </p>
                        <p>Fulcrum owns and operates over 50% of the units in the property and the residual units has been sold to individual owners.</p>
                        <p></p>

                        <h2 class="font15">INVESTMENT HIGHLIGHT </h2>
                        <p>
                        <ul>
                            <li>Investment Size: Over GBP 12 Million</li>
                            <li> ~16,000 SF</li>
                            <li> Rental yield : Over 9%</li>
                            <li> Averge Holding Period: 8 – 10 Years</li>
                        </ul>
                        </p>
                    </div>
                    {{--tab id="exp01" --same-- content aria-labelledby="exp01"--}}
                    {{--tab href="#tab01" and aria-controls="tab01" --same-- content id="tab01"--}}
                    {{--<div class="nav project-details flex-column nav-pills mt-5 pt-3 pt-0-sm float-left-mdtp mt-mdtp-3 desktop-show"--}}
                    {{--id="project-details" role="tablist" aria-orientation="vertical">--}}
                    {{---------------------- Tab 01 Start ---------------------}}
                    {{--<a class="nav-link active" id="gallery-tab" data-toggle="pill" href="#gallery-tab01" role="tab"--}}
                    {{--aria-controls="gallery-tab01" aria-selected="true">--}}
                    {{--Gallery--}}
                    {{--</a>--}}
                    {{---------------------- Tab 02 Start ---------------------}}
                    {{--<a class="nav-link" id="location-tab" data-toggle="pill" href="#location-tab02" role="tab"--}}
                    {{--aria-controls="location-tab02" aria-selected="false">--}}
                    {{--Location--}}
                    {{--</a>--}}
                    {{---------------------- Tab 03 Start ---------------------}}
                    {{--<a class="nav-link" id="amenities-tab" data-toggle="pill" href="#amenities-tab03" role="tab"--}}
                    {{--aria-controls="amenities-tab03" aria-selected="false">--}}
                    {{--Amenities--}}
                    {{--</a>--}}
                    {{---------------------- Tab 04 Start ---------------------}}
                    {{--<a class="nav-link" id="floorplans-tab" data-toggle="pill" href="#floorplans-tab04" role="tab"--}}
                    {{--aria-controls="floorplans-tab04" aria-selected="false">--}}
                    {{--Project Plan--}}
                    {{--</a>--}}

                    {{---------------------- Tab 05 Start ---------------------}}
                    {{--<a class="nav-link" id="unitplans-tab" data-toggle="pill" href="#unitplans-tab04" role="tab"--}}
                    {{--aria-controls="unitplans-tab04" aria-selected="false">--}}
                    {{--Unit Plan--}}
                    {{--</a>--}}
                    {{---------------------- Tab 05 Start hidden for now ---------------------}}
                    {{--<a class="nav-link" id="virtualtour-tab" data-toggle="pill" href="#virtualtour-tab05" role="tab"--}}
                    {{--aria-controls="virtualtour-tab05" aria-selected="false">--}}
                    {{--Virtual Tour--}}
                    {{--</a>--}}
                    {{---------------------- Tab 06 End hidden for now---------------------}}
                    {{--<a class="nav-link" id="download-tab" data-toggle="pill" href="#download-tab06" role="tab"--}}
                    {{--aria-controls="download-tab06" aria-selected="false">--}}
                    {{--Download Brochure--}}
                    {{--</a>--}}
                    {{---------------------- Tab 06 End ---------------------}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="col-md-6 pl-0 pr-0 pl-mdtp-2 pr-mdtp-2">
                <div class="w-100 float-left">
                    <div class="tab-content" id="v-pills-tabContent">
                        {{----------------------Gallery Tab Content 01 Start ---------------------}}
                        <div class="tab-pane fade show active" id="gallery-tab01" role="tabpanel"
                             aria-labelledby="gallery-tab">
                            <div class="w-100 float-left gallery-main">
                                <h4 class="float-left w-100 mobi-show"> Gallery</h4>
                                <div class="w-100 float-left" id="slider">
                                    <div id="myCarousel" class="carousel slide">
                                        <!-- main slider carousel items -->
                                        <div class="carousel-inner">
                                            <div class="active item carousel-item" data-slide-number="0">
                                                <img src="{{asset('images/projects-img/CARLOW-HOUSE/1.jpg')}}"
                                                     alt="project" class="w-100 img-fluid">
                                            </div>
                                            <div class="item carousel-item" data-slide-number="1">
                                                <img src="{{asset('images/projects-img/CARLOW-HOUSE/2.jpg')}}"
                                                     alt="project" class="w-100 img-fluid">
                                            </div>

                                            <a class="carousel-control-prev prev previous" href="#myCarousel" data-slide="prev" id="previous">
                                                <span class="carousel-control-prev-icon"></span>
                                            </a>
                                            <a class="carousel-control-next next" href="#myCarousel" data-slide="next" id="next">
                                                <span class="carousel-control-next-icon"></span>
                                            </a>
                                        </div>
                                        <!-- main slider carousel nav controls -->


                                        <ul class="carousel-indicators list-inline">
                                            <li class="list-inline-item active">
                                                <a id="carousel-selector-0" class="selected" data-slide-to="0"
                                                   data-target="#myCarousel">
                                                    <img src="{{asset('images/projects-img/CARLOW-HOUSE/1.jpg')}}"
                                                         class="img-fluid">
                                                </a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a id="carousel-selector-1" data-slide-to="1" data-target="#myCarousel">
                                                    <img src="{{asset('images/projects-img/CARLOW-HOUSE/2.jpg')}}"
                                                         class="img-fluid">
                                                </a>
                                            </li>


                                            {{--<li class="list-inline-item active">--}}
                                            {{--<a id="carousel-selector-4"  data-slide-to="4" data-target="#myCarousel">--}}
                                            {{--<img src="{{asset('images/projects-img/corella-project/project-img01.jpg')}}" class="img-fluid">--}}
                                            {{--</a>--}}
                                            {{--</li>--}}
                                            {{--<li class="list-inline-item">--}}
                                            {{--<a id="carousel-selector-5" data-slide-to="5" data-target="#myCarousel">--}}
                                            {{--<img src="{{asset('images/projects-img/corella-project/project-img02.jpg')}}" class="img-fluid">--}}
                                            {{--</a>--}}
                                            {{--</li>--}}
                                            {{--<li class="list-inline-item">--}}
                                            {{--<a id="carousel-selector-6" data-slide-to="6" data-target="#myCarousel">--}}
                                            {{--<img src="{{asset('images/projects-img/corella-project/project-img04.jpg')}}" class="img-fluid">--}}
                                            {{--</a>--}}
                                            {{--</li>--}}
                                            {{--<li class="list-inline-item active">--}}
                                            {{--<a id="carousel-selector-7"  data-slide-to="7" data-target="#myCarousel">--}}
                                            {{--<img src="{{asset('images/projects-img/corella-project/project-img01.jpg')}}" class="img-fluid">--}}
                                            {{--</a>--}}
                                            {{--</li>--}}
                                            {{--<li class="list-inline-item">--}}
                                            {{--<a id="carousel-selector-8" data-slide-to="8" data-target="#myCarousel">--}}
                                            {{--<img src="{{asset('images/projects-img/corella-project/project-img02.jpg')}}" class="img-fluid">--}}
                                            {{--</a>--}}
                                            {{--</li>--}}
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                </div>
            </div>
            {{--</div>--}}

            <a href="{{route('project-9')}}" class="project-next">
                <span class="desktop-show">Aspira Koh Samui</span>
                <span class="mobi-show">Next Project ></span>
                <div class="on-hover"></div>
            </a>

            <a href="{{route('project-4')}}" class="project-prev arrow-left2">
                <span class="desktop-show">Ten Ekamai Suites</span>
                <span class="mobi-show">< Prev Project</span>
                <div class="on-hover"></div>
            </a>
        </section>

        <script type="text/javascript">


            $(document).ready(function () {
//                var country;
//                $.getJSON("http://freegeoip.net/json/", function (data) {
//                    country = data.country_name;
//                    var ip = data.ip;
////                    console.log(country);
//                });
//                if(country='India')
//                {
//                    document.getElementById('mobile_code').value = "+91";
//                    document.getElementById('mobile_code').setAttribute("disabled","disabled");
//                    document.getElementById('mobile_code').setAttribute("checked","checked");
//                }
//                else if(country='Dubai')
//                {
//                    document.getElementById('mobile_code').value = "+972";
//                    document.getElementById('mobile_code').setAttribute("disabled","disabled");
//                    document.getElementById('mobile_code').setAttribute("checked","checked");
//
//                }

                $("#download").click();
                $("#project_form").click(function (e) {
                    var first_name = $('#first_name').val();
                    var last_name = $('#last_name').val();
                    var email = $('#email').val();
                    var mobile_code = $('#mobile_code').val();
                    var mobile_number = $('#mobile_number').val();
                    var radio = $('input[type=radio][name=r1]:checked').attr('id');

                    var ua = navigator.userAgent.toLowerCase();
                    var isAndroid = ua.indexOf("android") > -1;
                    var isIphone = ua.indexOf("iphone") > -1;
                    var isIpod = ua.indexOf("ipod") > -1;
                    var isIpad = ua.indexOf("ipad") > -1;
                    var isBB = ua.indexOf("blackberry") > -1;
                    var isBB2 = ua.indexOf("rim") > -1;
                    var isBB = ua.indexOf("blackberry") > -1;
                    var isBB2 = ua.indexOf("RIM") > -1;
                    var isSymbian = ua.indexOf("symbian") > -1;
                    var isNokia = ua.indexOf("nokia") > -1;
                    if (isNokia || isSymbian || isBB || isBB2 || isIphone || isIpod || isIpad || isAndroid) {
                        var src = "Mobile Website";
                    } else
                        src = "Website";

                    var text = "";
                    $.ajax(
                        {
                            url: '{{route('project-form')}}',
                            data: {
                                first_name: first_name,
                                last_name: last_name,
                                email: email,
                                mobile_code: mobile_code,
                                mobile_number: mobile_number,
                                radio: radio,
                                src:src
                            },
                            type: 'GET',
                            success: function (data) {


                                console.log('success', data);
                                if (data.code === 1) {
//                                    for (i = 0; i < data.message.length; i++) {
//                                        text += data.message[i] + "<br>";
//                                    }
                                    toastr.success("Thank you for your interest. Our team will contact you soon.", {timeOut: 5000});
                                    document.getElementById('download').click();
                                    var frm = document.getElementsByName('download_form')[0];
                                    frm.reset();

                                    var link = document.createElement('a');
                                    link.href = url;
                                    link.download = 'file.pdf';
                                    link.dispatchEvent(new MouseEvent('click'));
                                }

                                else {
                                    for (i = 0; i < data.message.length; i++) {
                                        text += data.message[i] + "<br>";
                                    }
                                    toastr.error(text, {timeOut: 5000})
                                    //toastr.error('You Got Error', 'Inconceivable!', {timeOut: 5000})

                                }
                            },
                            error: function (error) {
                                toastr.error('You Got Error', {timeOut: 5000});
//                                console.log('error');
                            }
                        }
                    );
                });
            });


            //code for floor slider


            var start_x = 0;
            $('.floor_swipe').on('dragstart', function (e) {
                start_x = event.clientX;
            }).on('dragend', function (e) {

                var end_x = event.clientX;
                var direction = start_x - end_x;

                if (direction < 0) {
                    $('.previous').trigger('click');
                } else {
                    $('.next').trigger('click');
                }
            });

            $('.floor_swipe').on('touchstart', function (e) {
                start_x = event.changedTouches[0].clientX;
            }).on('touchend', function (e) {

                var end_x = event.changedTouches[0].clientY;
                var direction = start_x - end_x;

                if (direction < 0) {
                    $('.previous').trigger('click');
                } else {
                    $('.next').trigger('click');
                }
            });

        </script>
    </div>
@endsection

@section('scripts')
    <!-- Onload remove register open class Start -->
    <script>
        $(document).ready(function() {
            $('.register-main').removeClass('register-open');
        });
    </script>
    <!-- Onload remove register open class End -->
@endsection