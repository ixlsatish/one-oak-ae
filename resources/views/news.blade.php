@extends('layouts.app')


@section('content')
    <div class="page-bg news-bg page-container align-center fadeIn animated delay-0">
        <section id="newtest"
                 class="news-page bg-color-black-op6 pt-5 pr-2 pb-5 pb-0-sm center-middle align-items fadeInLeft animated delay-4 mobi">
            {{--<div class="w-100 float-left ">--}}
            <div class="inner-logo text-center position-absolute pr-4">
                <a href="{{route('index')}}"><img src="{{asset('images/logo2.png')}}"></a>
            </div>
            <div class="col-md-6 pl-0">
                <div class="content-scrollbar section-pl-25">
                    <h1 class="section-title">
                        News
                    </h1>
                    {{--tab id="exp01" --same-- content aria-labelledby="exp01"--}}
                    {{--tab href="#tab01" and aria-controls="tab01" --same-- content id="tab01"--}}
                    <div class="nav flex-column nav-pills" id="news-tab" role="tablist" aria-orientation="vertical">
                        {{---------------------- Tab 01 Start ---------------------}}
                        <a class="nav-link active" id="new1-tab" data-toggle="pill" href="#new-tab01" role="tab"
                           aria-controls="new-tab01" aria-selected="true">
                            <h2>
                                Dubai developers bullish about 2018 outlook - Khaleej Times
                            </h2>
                            <p>
                                Shrugging off concerns about oversupply, Dubai's top real estate developers sounded bullish about the sector's outlook for 2018. As Expo 2020 nears, demand and prices are projected to rise, with investors flocking to cash in on the higher yields that Dubai offers.
                            </p>
                             <p class="brand-primary brand-primary-hover mb-0 mobi-show">Read More</p>
                        </a>
                        {{---------------------- Tab 02 Start ---------------------}}
                        <a class="nav-link " id="new2-tab" data-toggle="pill" href="#new-tab02" role="tab"
                           aria-controls="new-tab02" aria-selected="false">
                            <h2>

                                Affordable housing set to pick up pace in 2018 - Khaleej Times
                            </h2>
                            <p>
                                Population growth and employment opportunities are the twin drivers of growth in this market segment
                            </p>
                             <p class="brand-primary brand-primary-hover mb-0 mobi-show">Read More</p>
                        </a>
                        {{---------------------- Tab 03 Start ---------------------}}
                        <a class="nav-link " id="new3-tab" data-toggle="pill" href="#new-tab03" role="tab"
                           aria-controls="new-tab03" aria-selected="false">
                            <h2>

                                Dubai's real estate continues to shift towards affordable ...
                            </h2>
                            <p>
                                Dubai's real estate sector outperformed many other economic sectors of the emirate last year, with 6 per cent growth in the transaction value...
                            </p>
                             <p class="brand-primary brand-primary-hover mb-0 mobi-show">Read More</p>
                        </a>
                        {{---------------------- Tab 04 Start ---------------------}}
                        {{--<a class="nav-link " id="new4-tab" data-toggle="pill" href="#new-tab04" role="tab"--}}
                           {{--aria-controls="new-tab04" aria-selected="false">--}}
                            {{--<h2>--}}
                                {{--1OAK launches a new...--}}
                            {{--</h2>--}}
                            {{--<p>--}}
                                {{--Pioneering real estate by sculpting aspirational homes. Lorem ipsum dolor vsit amet,--}}
                                {{--consectetur adipiscing elit...--}}
                            {{--</p>--}}
                        {{--</a>--}}
                        {{---------------------- Tab 05 Start ---------------------}}
                        {{--<a class="nav-link " id="new5-tab" data-toggle="pill" href="#new-tab05" role="tab"--}}
                           {{--aria-controls="new-tab05" aria-selected="false">--}}
                            {{--<h2>--}}
                                {{--1OAK launches a new...--}}
                            {{--</h2>--}}
                            {{--<p>--}}
                                {{--Pioneering real estate by sculpting aspirational homes. Lorem ipsum dolor vsit amet,--}}
                                {{--consectetur adipiscing elit...--}}
                            {{--</p>--}}
                        {{--</a>--}}
                        {{---------------------- Tab 05 End ---------------------}}
                    </div>
                </div>
            </div>
            <div class="col-md-6 pr-2">
                <div class="w-100 float-left">
                    <div class="tab-content" id="v-pills-tabContent">
                        {{---------------------- Tab Content 01 Start ---------------------}}
                        <div class="content-scrollbar pr-3 tab-pane fade active show" id="new-tab01" role="tabpanel"
                             aria-labelledby="new1-tab">
                            <div class="w-100 float-left pb-2">
                                <img src="{{asset('images/news/upward-trajectory_uae.jpg')}}" class="w-100">
                            </div>
                            <div class="w-100 float-left">
                                <h2 class="mb-0">
                                    Dubai developers bullish about 2018 outlook - Khaleej Times
                                </h2>
                                <p class="font11 mb-2">
                                    khaleejtimes,  December 06 2017
                                </p>
                                <p>
                                    Shrugging off concerns about oversupply, Dubai's top real estate developers sounded bullish about the sector's outlook for 2018. As Expo 2020 nears, demand and prices are projected to rise, with investors flocking to cash in on the higher yields that Dubai offers.
                                </p>
                                <p>
                                    The senior executives were speaking on the sidelines of the Khaleej Times Infrastructure & Real Estate Excellence Awards held in Dubai on Tuesday.
                                </p>
                                <p>
                                    "As we get closer to Expo 2020, it will create more jobs and more people will come in for investment. In addition to Dubai being a safe haven in the region, it is always a magnet for investments. We'll see improvement next year in terms of demand and prices," said Muhammad Binghatti, CEO of Binghatti Developers.
                                </p>
                                <p>
                                    "It is always encouraging when supply comes from big developers such as Emaar, Dubai Properties and Nakheel as it reflects the trend and direction of the market," he added.

                                </p>
                                <p>
                                    <b>Oversupply no concern</b>
                                    Rejecting any concerns about oversupply, Binghatti said when the right developer brings a right product into the market, there is no need to be concerned.
                                </p>
                                <p>
                                    "It is no secret that there is huge cash in the market, but investors are waiting for the right opportunity. With many real estate funds coming to invest in Dubai, in addition to individual investors, there is going to be a lot of movement in the market. Dubai provides yields between eight to 12 per cent, which you can hardly find anywhere else in the world."
                                </p>
                                <p>
                                    Binghatti anticipates most demand next year to be for the mid segment.
                                </p>
                                <p>
                                    Niall McLoughlin, senior vice-president, corporate communication and marketing, Damac Group, is also sanguine about Dubai's property market for next year.
                                </p>
                                <p>
                                    "We anticipate the 2017 trend to continue in 2018. We projected Dh7 billion worth of sales for this year and are on track to beat that. We have seen stabilisation in the market and it is behaving in a mature way. When a developer comes to the market, he needs to bring in differentiated product. You have to bring in products with different price points. We have been lucky that products we brought to the market have been received very well," said McLoughlin.
                                </p>
                                <p>
                                    The developer aims to hand over 3,000 units this year and around the same number next year.
                                </p>
                                <p>"We sold more units in 2017 than in 2016. If you go to other major developers, you see the same trend. We have recorded increase in sales; even our competitors have seen an increase in sales, so we are quite bullish. You need a right product in the right location to drive prices," added McLoughlin.
                                </p>
                                <p>He revealed that there has been an increase in the number of customers from China and CIS countries.</p>
                                <p>Sandeep Jaiswal, deputy CEO, sales and marketing, Azizi Developments, believes 2018 will be promising with Expo 2020 around the corner. "Next year, we are looking at double digit growth in all aspects. That has been the case this year as well," he said. </p>
                                <p><b>Higher returns than London</b>"Dubai is one of the best markets in the world in terms of returns. In London, property offers around three to four per cent returns; in India, it's 2.5 to three per cent returns on rental. In Dubai, you are guaranteed to get minimum seven to eight per cent yields. If you're selling at the right location, it can even go up to 10 to 12 per cent," added Jaiswal.
                                    </p>
                                <p>Azizi Developments aims to deliver more than 1,000 units in 2017 and at least 1,500 units for the next year.</p>

                                <p class="font-italic">
                                    {{--Brotin Banerjee is managing director and chief executive officer, Tata Housing.--}}
                                </p>
                                <div class="float-left text-hover2 cursor-pointer font10 text-uppercase brand-primary">
                                    <a href="https://www.khaleejtimes.com/business/real-estate/dubais-real-estate-continues-to-shift-towards-affordable-homes" target="_blank" class="brand-primary brand-primary-hover">Visit Link</a>
                                </div>
                                <button class="btn float-right mobi-ipad-show back-news-list">
                                    BACK
                                </button>
                            </div>
                        </div>
                        {{---------------------- Tab Content 02 Start ---------------------}}
                        <div class="content-scrollbar pr-3 tab-pane fade" id="new-tab02" role="tabpanel"
                             aria-labelledby="new2-tab">
                            <div class="w-100 float-left pb-2">
                                <img src="{{asset('images/news/predictions_uae.jpg')}}" class="w-100">
                            </div>
                            <div class="w-100 float-left">
                                <h2 class="mb-0">
                                    Affordable housing set to pick up pace in 2018 - Khaleej Times
                                </h2>
                                <p class="font11 mb-2">
                                    khaleejtimes,  january 10 2018. 06 56 AM
                                </p>
                                <p>
                                    Dubai is often hailed as the queen of luxury housing. After all, it has some of the world's tallest and most expensive properties, securing a spot among top housing markets globally. As the business hub in the Middle East, Dubai surely has a real estate market to match and it is rapidly growing, and it's only going to grow stronger in the coming years.
                                </p>
                                <p>
                                    However, Dubai's appeal is not limited to its luxury high-end priorities. Led by a surge in young and middle-income working class population, Dubai is witnessing an incremental demand for affordable housing similar to that seen in major international markets in the late 90s into early 2000s.
                                </p>
                                <p>
                                    This has not gone unnoticed by domestic and regional real estate developers. Numerous developers have identified affordable housing as a leading growth segment, reporting double-digit sales growth. Many are seeking to capitalise on this upswing by offering generous and flexible payment schemes, thus bringing the prized asset within the reach of many and turning tenants into owners. This trend is expected to pick up significant pace in 2018.
                                </p>
                                <p>
                                    In the past year, the regulators too have displayed immense support for the affordable housing segment. The Dubai Land Development (DLD) plans to incentivise developers to build affordable housing options in central locations, thereby addressing the gap in the market for such offerings.
                                </p>
                                <p>
                                    Underpinning the appeal of affordable housing are the twin drivers of population growth and employment opportunities. Dubai plays host to vast number of opportunities for international investors, and as a result of which the sector is in many ways fragmented and unaffordable - for the local population.

                                </p>
                                <p>
                                    International investors still represent a clear majority of the Dubai property transactions; a number significantly higher than that seen in more developed markets. This balance is shifting, aided by a surge in population and more people entering into the workforce.

                                </p>
                                <p>
                                    The Expo 2020 is a critical catalyst in generating employment opportunities and attracting top talent from various parts of the world. Standard Chartered expects 300,000 new jobs to be created between 2018 and 2021, resulting from hosting the international event in Dubai. With so many new jobs being created, all signs are pointing towards a rise in demand for affordable housing in 2018 and beyond.

                                </p>
                                <p>
                                    Furthermore, according to the Dubai Municipality, Dubai's population is projected to grow to over five million by 2030; it is bigger than major international cities such as Sydney and Singapore. In short, this trend signals a new reality - the next few years will present a large, localised population that is shielded from the vagaries of foreign investments in local property markets, and where demand for affordable housing is high and rising.
                                </p>
                                <p>
                                    It is little surprise, then, that real estate developers are responding with a number of affordable residential projects in the pipeline. This trend is expected to continue to shape in 2018 and the effect will be felt for years to come, simply because for developers seeking critical mass and for investors providing a reasonable return on their investments.
                                <p>
                                    Furthermore, Dubai currently offers yields which are already between eight to 12 per cent, one of the highest in the world. By comparison, properties in London offer around three to four per cent returns while in Mumbai, a 2.5 to three per cent return on rentals is common.

                                </p>
                                <P>Further boosting the appeal of the affording housing segment are the attractive extended payment plans being offered by developers. However, with significant number of projects in the pipeline, it will be critical to see if both parties honour their side of the commitment, with developers delivering projects on time and investors honouring their payments as promised, which could determine the future of the sustainability of affordable freehold housing in Dubai, at least in the medium term.
                                </P>
                                <div class="float-left text-hover2 cursor-pointer font10 text-uppercase brand-primary">
                                    <a href="https://www.khaleejtimes.com/business/real-estate/affordable-housing-set-to-pick-up-pace-in-2018" target="_blank" class="brand-primary brand-primary-hover">Visit Link</a>
                                </div>
                                <button class="btn float-right mobi-ipad-show back-news-list">
                                    BACK
                                </button>
                            </div>
                        </div>
                        {{---------------------- Tab Content 03 Start ---------------------}}
                        <div class="content-scrollbar pr-3 tab-pane fade" id="new-tab03" role="tabpanel"
                             aria-labelledby="new3-tab">
                            <div class="w-100 float-left pb-2">
                                <img src="{{asset('images/news/turnaround-year_uae.jpg')}}" class="w-100">
                            </div>
                            <div class="w-100 float-left">
                                <h2 class="mb-0">
                                    Dubai's real estate continues to shift towards affordable ...
                                </h2>
                                <p class="font11 mb-2">
                                    khaleejtimes,  March 20 2018. 02.25 PM
                                </p>
                                <p>
                                    Dubai's real estate sector outperformed many other economic sectors of the emirate last year, with 6 per cent growth in the transaction value reaching Dh285 billion in 2017, up from Dh268.7 billion recorded in 2016, according to the Dubai Land Department.
                                </p>
                                <p>
                                    The value of sales of land, buildings and units in the Dubai real estate market totalled Dh114 billion through 49,000 transactions, while mortgages for the same three categories reached Dh138.5 billion through 15,700 transactions.
                                </p>
                                <p>
                                    In terms of the number of transactions, Dubai Land Department recorded a 14 per cent growth in the number of land and property transactions to 69,000. This in a layman's term, means that the average value of transactions have become more affordable, reflecting a shift towards smaller units with more reasonable ticket prices.
                                </p>
                                <p>
                                    Now, the question remains, what is an affordable home? What price threshold are we talking about?
                                </p>
                                <p>

                                    Affordable home is a very relative term. For example, the price of an affordable home in Dubai is completely different from what it is in Ajman or Umm Al Quwain.
                                </p>
                                <p>
                                    Generally, if a studio apartment is priced at below Dh400,000 - we could call it an affordable home. Fortunately, price of a studio apartment has come down to that level. An affordable 1-bedroom apartment is selling at Dh600,000 to Dh850,000 now depending on the location.
                                </p>
                                <p>
                                    We have seen the price of a 2-bedroom townhouse starting from Dh1 million while the three-bedroom variant starts trading from Dh1.3 million. Affordable home prices also vary from location within the same city. For example, price of an apartment at Dubai South will be much lower than the one located at Business Bay.
                                </p>
                                <p>
                                    For the last 15 years since the announcement of freehold properties in Dubai in 2002, developers have been building luxury properties in the emirate so much so, that there came a point of saturation in luxury properties. This was in sharp contrast to the development of affordable homes built in the city from the 1970s through to the 1990s - when residential apartments were targeted to house the middle income groups.
                                </p>
                                <p>
                                    While we might believe that affordable homes are relatively new term, Dubai government built a number of affordable home clusters in Deira, Karama, Al Ghusais, Satwa and Abu Hail neighbourhoods that hosted families of public sector employees working for different sectors like Dubai Municipality, Dubai Health Authority, Dubai Police and other government departments as well as people working for the private sector companies. These homes helped families to live in the emirate with lower cost of housing. There were non-freehold and were built much prior to the unparalleled property boom that Dubai witnessed. Most of these properties are still in existence and continue to support the emirate's middle class families.
                                </p>
                                <p>
                                    However, the freehold property boom in 2005-2008, shifted the game towards luxury properties - something that is now changing towards affordable homes, a clear sign of the maturity attained by the market.
                                </p>
                                <p>
                                    The continuous development of luxury properties and the lack of deliveries of affordable homes had gradually pushed the demand for middle-income homes and pushed up the rents so much so that the middle income families started migrating to the neighbouring emirates to save themselves of higher rent-related inflationary pressure, resulting in a daily traffic congestion on all inter-state highways between Dubai and Sharjah.
                                </p>
                                <p>

                                    This led to an imminent gap in Dubai's residential property market. After the recovery led by some immense planning, regulatory reforms and massive infrastructure expansion by the Dubai government, the industry realised the importance and opportunity in the affordable home segment. In order to make Dubai's real estate sector sustainable, developers need to continue exerting effort in delivering more affordable homes so that the backbone of the economy, the mid-income segment continues to call Dubai it's home.
                                </p>
                                <p>
                                    The retention of the middle class and a possible reverse migration of the middle income families from neighbouring emirates will result in a very good multiplier effect on Dubai's economy. The retail activities and domestic consumption will grow manifolds from the spending of these families.
                                </p>
                                <p>
                                    Such an equilibrium will also reduce traffic congestion between Dubai and neighbouring emirates and thus reduce gasoline consumption resulting in reduced carbon emission. Most importantly, this will help save at least 3-4 hours of time spent on traffic every working day and help a person utilise time in better productive cause and adding to the happiness index of the population.
                                </p>
                                <p>
                                    However, this would require a massive affordable housing programme and that would require a strong public-private partnership. Government could look into subsidising land price for areas earmarked for affordable homes that could be passed on to the middle income families.
                                </p>
                                <p>
                                    The success of any affordable home will largely depend on developers ability to bring down cost of acquiring property intelligently. It must be done through value engineering, exquisite planning and careful material selection and not by just reducing the size, deteriorating quality or cutting down on amenities.
                                </p>
                                <p>
                                    As the market matures, we will continue to see a strong case and increase in demand for affordable homes in the emirate. It will continue to grow into one of the biggest opportunity in the real estate sector which promises to not just boost the real estate sector but drive overall economic growth.
                                </p>


                                <div class="float-left text-hover2 cursor-pointer font10 text-uppercase brand-primary">
                                    <a href="https://www.khaleejtimes.com/business/real-estate/dubais-real-estate-continues-to-shift-towards-affordable-homes" target="_blank" class="brand-primary brand-primary-hover">Visit Link</a>
                                </div>
                                <button class="btn float-right mobi-ipad-show back-news-list">
                                    BACK
                                </button>
                            </div>
                        </div>
                        {{---------------------- Tab Content 04 Start ---------------------}}
                        {{--<div class="content-scrollbar pr-3 tab-pane fade" id="new-tab04" role="tabpanel"--}}
                             {{--aria-labelledby="new43-tab">--}}
                            {{--<div class="w-100 float-left pb-2">--}}
                                {{--<img src="{{asset('images/news-img.jpg')}}" class="w-100">--}}
                            {{--</div>--}}
                            {{--<div class="w-100 float-left">--}}
                                {{--<h2 class="mb-0">--}}
                                    {{--1OAK launches a new property in Dubai--}}
                                {{--</h2>--}}
                                {{--<p class="font11 mb-2">--}}
                                    {{--Khaleej Times, 14 Januray 2018, 10.20 am--}}
                                {{--</p>--}}
                                {{--<p>--}}
                                    {{--Pioneering real estate by sculpting aspirational homes. Lorem ipsum dolor sit amet,--}}
                                    {{--consectetur adipiscing elit. Nam luctus convallis rhoncus. Ut sit amet sodales arcu.--}}
                                    {{--Maecenas maximus purus eget arcu pulvinar, ac pharetra leo facilisis. Vestibulum nec--}}
                                    {{--sem ullamcorper, ullamcorper nibh ac, luctus massa. Vestibulum in varius turpis. Ut--}}
                                    {{--lectus augue, bibendum vitae pellentesque ac, egestas nec nibh. Vestibulum auctor--}}
                                    {{--efficitur elementum. Aliquam est augue, luctus at aliquam a, semper in turpis.--}}
                                    {{--Vestibulum quis mauris tempor, volutpat nisi in, molestie leo. Integer imperdiet--}}
                                    {{--convallis nulla. In a velit a purus blandit hendrerit eget sit amet urna. Donec--}}
                                    {{--imperdiet vehicula luctus. Fusce ligula nisi, volutpat fringilla tristique at,--}}
                                    {{--rutrum sit amet risus.--}}
                                {{--</p>--}}
                                {{--<div class="float-left text-hover2 cursor-pointer font10 text-uppercase brand-primary">--}}
                                    {{--Visit Link--}}
                                {{--</div>--}}
                                {{--<button class="btn float-right mobi-ipad-show back-news-list">--}}
                                    {{--BACK--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{---------------------- Tab Content 05 Start ---------------------}}
                        {{--<div class="content-scrollbar pr-3 tab-pane fade" id="new-tab05" role="tabpanel"--}}
                             {{--aria-labelledby="new5-tab">--}}
                            {{--<div class="w-100 float-left">--}}
                                {{--<h2 class="mb-0">--}}
                                    {{--1OAK launches a new property in Dubai--}}
                                {{--</h2>--}}
                                {{--<p class="font11 mb-2">--}}
                                    {{--Khaleej Times, 14 Januray 2018, 10.20 am--}}
                                {{--</p>--}}
                                {{--<p>--}}
                                    {{--Pioneering real estate by sculpting aspirational homes. Lorem ipsum dolor sit amet,--}}
                                    {{--consectetur adipiscing elit. Nam luctus convallis rhoncus. Ut sit amet sodales arcu.--}}
                                    {{--Maecenas maximus purus eget arcu pulvinar, ac pharetra leo facilisis. Vestibulum nec--}}
                                    {{--sem ullamcorper, ullamcorper nibh ac, luctus massa. Vestibulum in varius turpis. Ut--}}
                                    {{--lectus augue, bibendum vitae pellentesque ac, egestas nec nibh. Vestibulum auctor--}}
                                    {{--efficitur elementum. Aliquam est augue, luctus at aliquam a, semper in turpis.--}}
                                    {{--Vestibulum quis mauris tempor, volutpat nisi in, molestie leo. Integer imperdiet--}}
                                    {{--convallis nulla. In a velit a purus blandit hendrerit eget sit amet urna. Donec--}}
                                    {{--imperdiet vehicula luctus. Fusce ligula nisi, volutpat fringilla tristique at,--}}
                                    {{--rutrum sit amet risus.--}}
                                {{--</p>--}}
                                {{--<div class="float-left text-hover2 cursor-pointer font10 text-uppercase brand-primary">--}}
                                    {{--Visit Link--}}
                                {{--</div>--}}
                                {{--<button class="btn float-right mobi-ipad-show back-news-list">--}}
                                    {{--BACK--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
            {{--</div>--}}
        </section>
    </div>

    <script type="text/javascript">
        console.log($(window).width());
        if ($(window).width() < 769) {
//            console.log('test');
            $('#new1-tab').removeClass('active');
            $('#new-tab01').removeClass('active show');
            $('.nav-link').addClass('mobile-news-tab');
        }

        $('#newtest').on("scroll", function () {
            console.log('kjhjhgjhgjh00');
            console.log($(document).scrollTop());
            if
            ($(document).scrollTop() > 50) {
                $("#newtest").addClass("newtest");
            }
            else {
                $("#newtest").removeClass("newtest");

//                $("header").removeClass("shrink");
            }
        });
    </script>
@endsection

@section('scripts')
    <!-- Onload remove register open class Start -->
    <script>
        $(document).ready(function() {
            $('.register-main').removeClass('register-open');
        });
    </script>
    <!-- Onload remove register open class End -->
@endsection