@extends('layouts.app-atmos')

@section('styles')
@endsection


@section('content')
    <div class="page-bg atmos-bg page-container align-center fadeIn animated delay-0 mdtp-flex-none">
        <section class="home-page fadeInLeft animated delay-4">
        @if (count($errors) > 0)
            <div class="alert alert-danger font-bold" style="text-transform: capitalize;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif
        <div class="home-logo section-pl-12 position-relative">
            <div class="position-absolute w-50 h-100"></div>
            <img src="{{asset('images/logo.png')}}">
        </div>
        <h1 class="section-title mt-4 title-line section-pl-12">
            LUCKNOW, INDIA
        </h1>
        <div class="text-left content section-pl-12 home-content">
            <p>Atmos is a new landmark redefining the Lucknow’s high rise residential market. Strategically located in the heart of Gomti Nagar, ATMOS is a premium 24 storey development with expansive 3 and 4 bedroom residences. Each house provides its residents absolute privacy, while allowing them to be close to nature.</p>
            <div id="read-more" class="float-left text-decoration text-hover cursor-pointer ">
                Read More
            </div>
            <div id="hideread-more" class="float-left text-decoration text-hover cursor-pointer ">
                Hide
            </div>
        </div>
    </section>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/video.js')}}" rel="script"></script>

    <!-- Onload remove register open class Start -->
    <script>
        $(document).ready(function() {
            $("header").css("z-index","9999");
        });
    </script>
    <!-- Onload remove register open class End -->
@endsection