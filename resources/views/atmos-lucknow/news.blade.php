@extends('layouts.app-atmos')


@section('content')
    <div class="page-bg news-bg page-container align-center fadeIn animated delay-0">
        <section id="newtest"
                 class="news-page bg-color-black-op6 pt-5 pr-2 pb-5 pb-0-sm center-middle align-items fadeInLeft animated delay-4 mobi">
            {{--<div class="w-100 float-left ">--}}
            <div class="inner-logo text-right position-absolute pr-4">
                <a href="{{route('lucknow.index')}}"><img src="{{asset('images/logo2.png')}}"></a>
            </div>
            <div class="col-md-6 pl-0">
                <div class="content-scrollbar section-pl-25">
                    <h1 class="section-title">
                        News
                    </h1>
                    {{--tab id="exp01" --same-- content aria-labelledby="exp01"--}}
                    {{--tab href="#tab01" and aria-controls="tab01" --same-- content id="tab01"--}}
                    <div class="nav flex-column nav-pills" id="news-tab" role="tablist" aria-orientation="vertical">
                        {{---------------------- Tab 03 Start ---------------------}}
                        <a class="nav-link active" id="new3-tab" data-toggle="pill" href="#new-tab03" role="tab"
                           aria-controls="new-tab03" aria-selected="true">
                            <h2>
                                This could be the turnaround year for real estate. Here's why
                            </h2>
                            <p>
                                India took a long time to get over the socialist hangover of the post-Independence belief that houses...
                            </p>
                            <p class="brand-primary brand-primary-hover mb-0 mobi-show">Read More</p>
                        </a>
                        {{---------------------- Tab 04 Start ---------------------}}
                        <a class="nav-link " id="new4-tab" data-toggle="pill" href="#new-tab04" role="tab"
                           aria-controls="new-tab04" aria-selected="false">
                            <h2>
                                Housing prices rise by up to 22% in 33 cities during April-June FY19: NHB
                            </h2>
                            <p>
                                For under-construction properties, housing prices went up in 39 cities by up to 17 per cent and...
                            </p>
                            <p class="brand-primary brand-primary-hover mb-0 mobi-show">Read More</p>
                        </a>

                    </div>
                </div>
            </div>
            <div class="col-md-6 pr-2">
                <div class="w-100 float-left">
                    <div class="tab-content" id="v-pills-tabContent">
                        {{---------------------- Tab Content 01 Start ---------------------}}
                        <div class="content-scrollbar pr-3 tab-pane fade active show" id="new-tab03" role="tabpanel"
                             aria-labelledby="new3-tab">
                            <div class="w-100 float-left pb-2">
                                <img src="{{asset('images/news/turnaround-year.jpg')}}" class="w-100">
                            </div>
                            <div class="w-100 float-left">
                                <h2 class="mb-0">
                                    This could be the turnaround year for real estate. Here's why
                                </h2>
                                <p class="font11 mb-2">
                                    Economic Times, Jan 14, 2018, 03.10 PM IST
                                </p>
                                <p>
                                    India took a long time to get over the socialist hangover of the post-Independence belief that houses were to be provided by the government. It happened in the early 2000s. Billboards and advertisements offered myriad schemes to own a home in the city: “no EMI until possession”, “pay 20% now and 80% at possession”, “get 3BHK at the price of 2”, “get assured rentals for 2-3 years”, “up to 95% financing available”, “free modular kitchen and wooden flooring”.
                                </p>
                                <p>
                                    A Delhi-based employee of a public sector company, who prefers to be called Roy, was due to retire in 2011. With both his daughters settled, he had hoped to move into a home with his wife of 40 years. As a prospective home buyer, he soon became “hot property” for realty developers. The euphoria was high.
                                </p>
                                <p>
                                    Property agents started hounding him. Roy says he booked a flat from the Amrapali Group in 2010. Eight years on, after paying 80% of the cost, Roy alleges that he still doesn’t own the flat and continues to stay in a rented accommodation. Amrapali has yet to deliver 40,000 flats across its 10 housing projects in Noida and Greater Noida.
                                </p>
                                <p>
                                    “Land being a fixed asset gives the sector a high capacity to leverage. It works on a model where a business can be started by taking loans multiple times the value of the underlying asset. Most developers have built their model based on the high perceived value of the underlying asset and not the proven capacity to execute,” says an official of Noida authority on condition of anonymity.
                                </p>
                                <p>
                                    <b>The Rise & Fall</b> <br>
                                    The macroeconomic environment was also conducive. India’s GDP grew 8.4% in 2010-11. Banks offered easy home loans to buyers, and funds to developers, over leveraging the sector. Projects were launched without the 15-odd necessary clearances and authorities looked away. With historical annual returns of 20% (1991-2014) on real estate, who would have thought the realty will bite one day?
                                </p>
                                <p>
                                    Vis-a-vis real estate, gold, equity and bank FDs gave returns of 10.9%, 15.5% and 8.8% respectively, according to a Resurgent India report. The house of cards fell in 2013-14 as the GDP contracted to 4.7% (the baseline revision later pushed it to 6.9%). As the bubble burst, buyers stopped paying and banks stopped funding. The liquidity crunch saw developers delaying projects: a few went bankrupt and litigation and bad deliveries followed.
                                </p>
                                <p>
                                    Some 40,000 buyers each of the Jaypee Group and Amrapali and 19,000 of Unitech have been reportedly left in the lurch — doubly burdened with EMIs and house rents.
                                </p>
                                <p>
                                    “The banks invested, builders gained and buyers like us are facing losses,” says a hassled home buyer of Unitech. Getamber Anand, CMD of ATS Infrastructure, says: “In the past 7-10 years, many new aggressive players with no experience thought of making easy money. They kept on buying land and launching new projects. However, I would say the intention of developers such as Jaypee was noble but they could not manage cash flow and got caught in a debt trap.”
                                </p>
                                <p>
                                    As on March 31, 2015, the Jaypee Group had an estimated gross debt of Rs 75,000 crore, according to a Credit Suisse report. Despite a fall in housing rates, most buyers remained cautious in 2017. The year saw a massive dip in project launches. According to property consultants Anarock (which bought out Jones Lang LaSalle’s residential and fund management business in India), only 94,000 units were added in top seven cities between Q1 and Q3 of 2017, a 50% drop from the same period in 2016.
                                </p>
                                <p>
                                    According to Anarock, Delhi-NCR has the maximum number of unsold residential units among cities — approximately 2,00,000. In Delhi-NCR, Greater Noida has the maximum share of unsold inventory, followed by Gurugram. In contrast, unsold residential inventory has declined rapidly in Mumbai: in the Mumbai Metropolitan Region it fell to 1,80,000 units at the end of September quarter of 2017 from 192,000 units in the year-ago period, and in Bengaluru, it has dropped to 1.04 lakh units from 1.24 lakh u ..
                                </p>
                                <p>
                                    “This is a period of change that will benefit serious players,” says Manoj Gaur, MD, Gaursons India. “Any developer with a good track record has been selling property fairly easily in this market,” says Anshuman Magazine, chairman, India & Southeast Asia, CBRE, a USbased commercial real estate services and investment firm.
                                </p>
                                <p>
                                    <b>Room for Improvement</b> <br>
                                    India’s booming real estate story is just about 20 years old. After Independence, the dominant thinking was that government will provide housing. Mostly state development authorities did the job till about 2001 with a few exceptions like Hiranandani, DLF and the Ansal Group. Haryana was the first state to promote private real estate developers in 1990s. After Gurugram, Noida caught on and soon high rises sprang up in cities.
                                </p>
                                <p>
                                    “It’s a relatively new industry in which models are put in place by relatively new players,” says the authority official.
                                </p>
                                <p>
                                    The government passed the Real Estate Regulation & Development Act (RERA) in 2016, which makes mandatory registration of all projects with a clear deadline, empowers buyers to cancel booking and get refunds, and calls for punitive action for non-delivery. “RERA has instilled confidence among buyers. A regulation is in place that is making the sector organised. Now projects will have to be completed. All this should result in a more mature real estate market,” says CBRE’s Magazine.
                                </p>
                                <p>
                                    The prime objective of RERA is to safeguard the interest of home buyers by restricting fly-by-night players, increasing the share of organised segment and bringing in transparency.
                                </p>
                                <p>
                                    “An entry barrier has been created for nonserious players because of RERA. Developers are getting organised,” says Manoj Gaur. Now projects will have to be completed, and it will take care of a majority of the problems in the sector, adds Magazine. But experts also feel RERA is useful only till the developer is solvent. India’s insolvency act gives banks the first right to recover dues.
                                </p>
                                <p>
                                    Homebuyers, being unsecured creditors, will get nothing out of the insolvency proceedings. Also, with the onus of delivery entirely on builders, the authorities are out of the ambit. This is a glaring omission, say experts. By the end of July 2017, all states should have implemented and set up RERA authority. However, many are still lagging on that count. Dilution of a few RERA rules by some states has also hurt buyers’ confidence.
                                </p>
                                <P>
                                    Maharashtra is the frontrunner in the process of implementing RERA, having registered over 13,000 projects till October 2017. Under RERA, any project that has not got completion certificate by May 1, 2017, will be treated as ongoing project and will have to be registered as such. But states such as Haryana, Andhra Pradesh, Jharkhand, Chhattisgarh, Telangana and Odisha have yet to get their RERA websites live, notes Anarock.
                                </P>
                                <P>
                                    “RERA has definitely worked in terms of enhancing home buyers’ sentiment but the disparity between the states in the implementation of the law is something that needs to be looked at,” says Jaxay Shah, national president, Credai (Confederation of Real Estate Developers Association of India). Many acknowledge that it is a good law with noble intentions.
                                </P>
                                <p>
                                    “RERA has managed to restore consumer sentiments to a great extent and has helped the sector to tread on the path of revival,” says Navin Raheja, CMD, Raheja Developers. A slew of other reforms has started to clean the muck. Apart from GST and demonetisation, law on benami property and push for affordable housing with infrastructure status have helped.
                                </p>
                                <p>
                                    “As part of the scheme, we have launched an affordable plotting project on the Gurugram-Sohna Road under the Deen Dayal Jan Awas Yojana of the Haryana government. There is a pressing demand for at least 2 crore homes in the urban areas of the country, of which 95% is under the affordable segment,” says Raheja.
                                </p>
                                <p>
                                    ATS, with its ancillary company House Craft, delivered 6-8 million sq ft of projects till November 2017, all units of less than 600 sq ft and in the affordable category. “New housing will come up but more in the affordable segment. Developers will make no-frills house. As GDP grows, this segment too will get a boost,” says Niranjan Hiranandani, managing director, Hiranandani Group. With its mission of housing for all by 2022, the Centre has been pushing the Pradhan Mantri Awas Yojana (PMAY), wit ..
                                </p>
                                <p>
                                    “With the government’s push for housing for all by 2022, the year 2018 will be the year of affordable segment,” says Praveen Jain, CMD, Tulip Infratech. Big new developers with corporate pedigree such as Prestige, Tatas, Mahindra and Godrej are making a foray into real estate. This should further improve the quality of homes built.
                                </p>
                                <p>
                                    “There has been a dearth in project launches which could have a negative effect on the supply of real estate properties. The increasing demand, on the other hand, could mean that prices might increase in the future, due to the mismatch in demand and supply,” says Jaxay Shah of Credai. This leaves buyers with the fundamental question. Will 2018 be a turnaround year?
                                </p>
                                <p>
                                    “There is a shortage of two crore units in the seven metros besides four crore in tier-2, -3 and -4 cities. So one thing is established— there is demand,” says ATS’ Anand.
                                </p>
                                <p>
                                    <b>Home and Dry</b> <br>
                                    Many feel 2018 will be the year of consolidation and demand picking up. “Fence-sitters have realised that prices have bottomed out. 2018 is the year real estate will look better with greater compliances and transparency. Q3 of this year already saw sales kick-off again and I feel from Q1 of FY2019 we will see a turnaround in sales. This consolidation will continue till 2020. From low-turnover, high-margin, we will become high turnover, low-margin,” adds Anand.
                                </p>
                                <p>
                                    It will be the season to buy.
                                </p>
                                <p>
                                    “Even as investors still look at market cautiously, end users will continue to benefit,” says Magazine. But will investors get the same 20% CAGR returns, starting 2018? “When rate goes up, the percentage of returns has to decline. Something that went from 10 to 100 can’t go from 100 to 1,000 in the same time. The appreciation we saw in the past was significant. Now, as the economy matures, you can’t get the same returns,” says Magazine. <br>
                                    “By 2020, the mid and premium segment too should see a recovery,” says Jain of Tulip Infratech. Should investors then wait some more? “We may not see a scintillating market recovery in 2018, but whatever growth we see from here will be sustainable and backed by stronger market fundamentals than ever before. The days of speculative peaks and troughs are safely behind us,” says Anuj Puri, chairman of Anarock.
                                </p>
                                <p>
                                    <b>Reforms have changed the landscape of real estate: Jaxay Shah, national president, Credai</b>
                                </p>
                                <p class="font-italic">
                                    What are the changes that have happened in the real estate sector?
                                </p>
                                <p>
                                    Implementation of reforms like RERA and GST has changed the landscape of the sector, initiating a new era of transparency and accountability, which has enhanced the trust of home buyers to a great extent. This will not only benefit buyers but will also encourage credible developers while clamping down unscrupulous activities. The affordable housing segment’s emergence in 2017 has also contributed majorly to the sector’s growth and has been able to rejuvenate the industry.
                                </p>
                                <p>
                                    <b>Will consolidation happen with RERA?</b>
                                    The implementation of RERA has led to an increase in the number of compliances and has introduced a different set of standard operating procedures. The inability of smaller players to adhere to new rules and regulations could prompt them to either merge with or be acquired by bigger companies.
                                    <b>Is RERA working for the sector? Are there worries?</b> <br>
                                    RERA has definitely worked in terms of enhancing home buyers’ sentiment. The disparity between the states in the implementation of the law, however, is something that needs to be looked at.
                                </p>
                                <p>
                                    <b>Where do you think are residential prices headed?</b>
                                </p>
                                <p>
                                    As opposed to many projections, prices of Indian real estate properties have remained stable and the trend is set to continue in 2018 too. Currently, there has been a dearth in new project launches due to the introduction of RERA, which could have a negative effect on the supply of real estate properties. The increasing demand, on the other hand, could mean that prices might increase in the future due to the mismatch in demand and supply.
                                </p>

                                <p>
                                    <b>Growth prospects for 2018 look bright: Anshuman Magazine, chairman (India & Southeast Asia), CBRE</b>
                                </p>

                                <p>
                                    <b>What is the outlook for 2018?</b><br>
                                    Residential segment has been low in 2017 due to demonetisation, GST, RERA. Sales slowed down; overall launches also declined. It created a lot of liquidity crunch. The impact was felt more in NCR and Mumbai, but the sector has suffered overall. The positive side is that the affordable segment is happening. Going forward, the outlook for residential segment in 2018 is very optimistic. Supply is coming in in the affordable segment, which is where the big demand is. As economy picks up, the movemen ..
                                </p>
                                <p>
                                    <b>Will we again see the kind of returns real estate gave earlier?</b> <br>
                                    Price increase will be at sensible levels, not at historical levels. But you never know: if economy again grows at 9.5% again, real estate prices may again pick up. In the short term, exponential appreciation is ruled out.
                                </p>
                                <p>
                                    <b>Commercial or residential: which will do better?</b> <br>
                                    These are two different markets. Commercial segment has been doing very well. In 2015, some 38 million sq ft office space was taken up in India. In 2016, 46 m sq ft was taken up and in 2017 about 40 m sq ft space was expected to close. It is unparalleled. I expect commercial sector to go up. Because rates are high, the growth will not be like earlier but it will continue to do well at least till 2018.
                                </p>

                                <div class="float-left text-hover2 cursor-pointer font10 text-uppercase brand-primary">
                                    <a href="https://economictimes.indiatimes.com/wealth/real-estate/this-could-be-the-turnaround-year-for-real-estate-heres-why/articleshow/62490034.cms" target="_blank" class="brand-primary brand-primary-hover">Visit Link</a>
                                </div>
                                <button class="btn float-right mobi-ipad-show back-news-list">
                                    BACK
                                </button>
                            </div>
                        </div>
                        {{---------------------- Tab Content 02 Start ---------------------}}
                        <div class="content-scrollbar pr-3 tab-pane fade" id="new-tab04" role="tabpanel"
                             aria-labelledby="new4-tab">
                            <div class="w-100 float-left pb-2">
                                <img src="{{asset('images/news/real-estate-think-t-ock.jpg')}}" class="w-100">
                            </div>
                            <div class="w-100 float-left">
                                <h2 class="mb-0">
                                    Housing prices rise by up to 22% in 33 cities during April-June FY19: NHB
                                </h2>
                                <p class="font11 mb-2">
                                    Economic Times, Jan 03, 2019, 10.01 AM IST
                                </p>
                                <p>
                                    For under-construction properties, housing prices went up in 39 cities by up to 17 per cent and declined in 8 cities by up to 8 per cent and remained stable in 3 cities.
                                </p>
                                <p>
                                    NEW DELHI: Housing prices increased in 33 cities by up to 22 per cent during April-June quarter this fiscal, while rates fell in 14 cities by up to 13 per cent and 3 cities remained stable, according to revised National Housing Bank data with new base year.
                                </p>
                                <p>
                                    For under-construction properties, housing prices went up in 39 cities by up to 17 per cent and declined in 8 cities by up to 8 per cent and remained stable in 3 cities.
                                </p>
                                <p>
                                    The National Housing Bank (NHB), which launched housing prices index 'NHB RESIDEX' in 2007 to track the movement in housing prices on quarterly basis, has revamped the system by changing the base year and releasing separate index for under construction properties.
                                </p>
                                <p>
                                    "NHB RESIDEX has been revamped to include cluster of indices with updated base year, revised methodology and automated processes. The revamped NHB RESIDEX is wider in its geographic coverage...," it said in a statement.
                                </p>
                                <p>
                                    The revamped index captures two housing price indices -- 'HPI@ Assessment Prices for 50 cities and HPI@ Market Prices for Under Construction Properties for 50 cities'. The coverage is spread across 21 states in India.
                                </p>
                                <p>
                                    NHB RESIDEX also includes Composite HPI@Assessment Prices and Composite HPI@Market Prices for Under Construction Properties for 50 cities each.
                                </p>
                                <p>
                                    Till March 2018, HPIs tracked the movement in prices of residential properties on a quarterly basis, taking 2012-13 fiscal as the base year. Now, the base year has been shifted to FY 2017-18.
                                </p>
                                <p>
                                    "Composite HPI@Assessment Prices stood at 83 in June, 2013 and has moved up to 101 in the current quarter i e June, 2018. The index has moved up with a CAGR of 3.8 per cent over the years. The index increased by 2 per cent on YoY (Year-on-Year) basis," NHB said.
                                </p>
                                <p>
                                    City-wise, it said the HPI recorded an overall increase in 33 cities, decrease in 14 cities and no change in 3 cities on y-o-y basis. "Annual growth in HPI ranged from 21.7 per cent in Ranchi to (-) 13.4 per cent in Bhiwadi at the end of the quarter".
                                </p>
                                <p>
                                    Among the 8 Tier-1 cities, Ahmedabad witnessed maximum increase at 12.9 per cent on y-o-y basis, followed by Hyderabad (9.5 per cent), Pune (7.2) per cent and Mumbai (5.2 per cent), Bangalore and Kolkata (2 per cent).
                                </p>
                                <p>
                                    Chennai witnessed no change and Delhi witnessed a fall in index by (-) 4.8 per cent.
                                </p>
                                <p>
                                    Of the 29 Tier-2 cities, NHB said that significant rise in indices was seen in Ranchi (21.7 per cent), followed by Nashik (8.4 per cent), Surat (7.4 per cent) and Vadodara (7.4 per cent), while significant fall in indices was seen in Ludhiana (-12.3 per cent) and Jaipur (-9.1 per cent) on y-o-y basis.
                                </p>
                                <p>
                                    In 13 Tier-3 cities, Gandhinagar (12.8 per cent), Chakan (10.8 per cent) and New Town Kolkata (10.5 per cent) showed significant increase in indices, while Bhiwadi (-13.4 per cent) showed maximum decrease.
                                </p>
                                <p>
                                    In under-construction properties, NHB said that composite HPI@Market Prices stood at 84 in June, 2013 and have steadily moved up to 101 in the current quarter i.e. June, 2018. The index has moved up with a CAGR of 3.6 per cent over the years. On y-o-y basis, the index has witnessed a rise of 3.1 per cent.
                                </p>
                                <p>
                                    City-wise, the HPI recorded an overall increase in 39 cities, decrease in 8 cities and no change in 3 cities on y-o-y basis.
                                </p>
                                <p>
                                    "Annual growth in HPI ranged from 16.9 per cent in Kolkata to (-) 8.3 per cent in Faridabad at the end of the quarter," the statement said.
                                </p>
                                <p>
                                    Annually, all the 8 Tier 1 cities showed growth with Kolkata (16.9 per cent), followed by Hyderabad (6.3 per cnet), Mumbai (4.1 per cent), Delhi (4.1 per cent), Chennai (3.1 per cent), Pune (2 per cent), Ahmedabad (2 per cent) and Bengaluru (1 per cent).
                                </p>
                                <P>
                                    Among 29 Tier-2 cities, the maximum increase in indices was seen in Indore (9.7 per cent) followed by Dehradun (8.3 per cent) and Lucknow (7.5 per cent), while maximum decrease in indices was seen in Faridabad (-8.3 per cent), Thiruvananthapuram (-7.5 per cent) and Vadodara (-3.9 per cent), on y-o-y basis.
                                </P>
                                <P>
                                    In the 13 Tier-3 cities, the variations ranged from 14.4 per cent in Bidhan Nagar to (–) 4.8 per cent in Howrah on y-o-y basis.
                                </P>
                                <div class="float-left text-hover2 cursor-pointer font10 text-uppercase brand-primary">
                                    <a href="https://economictimes.indiatimes.com/wealth/real-estate/housing-prices-rise-by-up-to-22-in-33-cities-during-april-june-fy19-nhb/articleshow/67359964.cms" target="_blank" class="brand-primary brand-primary-hover">Visit Link</a>
                                </div>
                                <button class="btn float-right mobi-ipad-show back-news-list">
                                    BACK
                                </button>
                            </div>
                        </div>
                        {{---------------------- Tab Content 03 Start ---------------------}}

                        {{---------------------- Tab Content 04 Start ---------------------}}
                        {{--<div class="content-scrollbar pr-3 tab-pane fade" id="new-tab04" role="tabpanel"--}}
                             {{--aria-labelledby="new43-tab">--}}
                            {{--<div class="w-100 float-left pb-2">--}}
                                {{--<img src="{{asset('images/news-img.jpg')}}" class="w-100">--}}
                            {{--</div>--}}
                            {{--<div class="w-100 float-left">--}}
                                {{--<h2 class="mb-0">--}}
                                    {{--1OAK launches a new property in Dubai--}}
                                {{--</h2>--}}
                                {{--<p class="font11 mb-2">--}}
                                    {{--Khaleej Times, 14 Januray 2018, 10.20 am--}}
                                {{--</p>--}}
                                {{--<p>--}}
                                    {{--Pioneering real estate by sculpting aspirational homes. Lorem ipsum dolor sit amet,--}}
                                    {{--consectetur adipiscing elit. Nam luctus convallis rhoncus. Ut sit amet sodales arcu.--}}
                                    {{--Maecenas maximus purus eget arcu pulvinar, ac pharetra leo facilisis. Vestibulum nec--}}
                                    {{--sem ullamcorper, ullamcorper nibh ac, luctus massa. Vestibulum in varius turpis. Ut--}}
                                    {{--lectus augue, bibendum vitae pellentesque ac, egestas nec nibh. Vestibulum auctor--}}
                                    {{--efficitur elementum. Aliquam est augue, luctus at aliquam a, semper in turpis.--}}
                                    {{--Vestibulum quis mauris tempor, volutpat nisi in, molestie leo. Integer imperdiet--}}
                                    {{--convallis nulla. In a velit a purus blandit hendrerit eget sit amet urna. Donec--}}
                                    {{--imperdiet vehicula luctus. Fusce ligula nisi, volutpat fringilla tristique at,--}}
                                    {{--rutrum sit amet risus.--}}
                                {{--</p>--}}
                                {{--<div class="float-left text-hover2 cursor-pointer font10 text-uppercase brand-primary">--}}
                                    {{--Visit Link--}}
                                {{--</div>--}}
                                {{--<button class="btn float-right mobi-ipad-show back-news-list">--}}
                                    {{--BACK--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{---------------------- Tab Content 05 Start ---------------------}}
                        {{--<div class="content-scrollbar pr-3 tab-pane fade" id="new-tab05" role="tabpanel"--}}
                             {{--aria-labelledby="new5-tab">--}}
                            {{--<div class="w-100 float-left">--}}
                                {{--<h2 class="mb-0">--}}
                                    {{--1OAK launches a new property in Dubai--}}
                                {{--</h2>--}}
                                {{--<p class="font11 mb-2">--}}
                                    {{--Khaleej Times, 14 Januray 2018, 10.20 am--}}
                                {{--</p>--}}
                                {{--<p>--}}
                                    {{--Pioneering real estate by sculpting aspirational homes. Lorem ipsum dolor sit amet,--}}
                                    {{--consectetur adipiscing elit. Nam luctus convallis rhoncus. Ut sit amet sodales arcu.--}}
                                    {{--Maecenas maximus purus eget arcu pulvinar, ac pharetra leo facilisis. Vestibulum nec--}}
                                    {{--sem ullamcorper, ullamcorper nibh ac, luctus massa. Vestibulum in varius turpis. Ut--}}
                                    {{--lectus augue, bibendum vitae pellentesque ac, egestas nec nibh. Vestibulum auctor--}}
                                    {{--efficitur elementum. Aliquam est augue, luctus at aliquam a, semper in turpis.--}}
                                    {{--Vestibulum quis mauris tempor, volutpat nisi in, molestie leo. Integer imperdiet--}}
                                    {{--convallis nulla. In a velit a purus blandit hendrerit eget sit amet urna. Donec--}}
                                    {{--imperdiet vehicula luctus. Fusce ligula nisi, volutpat fringilla tristique at,--}}
                                    {{--rutrum sit amet risus.--}}
                                {{--</p>--}}
                                {{--<div class="float-left text-hover2 cursor-pointer font10 text-uppercase brand-primary">--}}
                                    {{--Visit Link--}}
                                {{--</div>--}}
                                {{--<button class="btn float-right mobi-ipad-show back-news-list">--}}
                                    {{--BACK--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
            {{--</div>--}}
        </section>
    </div>

    <script type="text/javascript">
        console.log($(window).width());
        if ($(window).width() < 769) {
//            console.log('test');
            $('#new1-tab').removeClass('active');
            $('#new-tab01').removeClass('active show');
            $('.nav-link').addClass('mobile-news-tab');
        }

        $('#newtest').on("scroll", function () {
            console.log('kjhjhgjhgjh00');
            console.log($(document).scrollTop());
            if
            ($(document).scrollTop() > 50) {
                $("#newtest").addClass("newtest");
            }
            else {
                $("#newtest").removeClass("newtest");

//                $("header").removeClass("shrink");
            }
        });
    </script>
@endsection

@section('scripts')
    <!-- Onload remove register open class Start -->
    <script>
        $(document).ready(function() {
            $('.register-main').removeClass('register-open');
        });
    </script>
    <!-- Onload remove register open class End -->
@endsection