@extends('layouts.app-atmos')

@section('video')
    <div class="home-bg" style="background-image: url('images/project_bg.jpg');"></div>
@endsection

@section('content')
    <div class="page-bg corella-bg page-container align-center fadeIn animated delay-0 mdtp-flex-none">
        <section class="project-details-page bg-color-black-op6 center-middle align-items position-relative fadeInLeft animated delay-4 mdtp-flex-none">

            {{--<div class="w-100 float-left ">--}}
            <div class="inner-logo text-right position-absolute pr-4">
                <a href="{{route('lucknow.index')}}"><img src="{{asset('images/logo2.png')}}" alt="logo"></a>
            </div>
            <div class="col-md-6 pl-0">
                <div class="section-pl-25">
                    <div class="w-100 float-left mdtp-w-70 mdtp-float-right">
                        <div class="w-100 float-left">
                            <h1 class="section-title mt-2 float-left pr-2">
                                ATMOS
                            </h1>
                        </div>
                        <h2 class="font15">LUCKNOW, INDIA  </h2>
                        <p>Atmos is a new landmark redefining the Lucknow’s high rise residential market. Strategically located in the heart of Gomti Nagar, ATMOS is a premium 24 storey development with expansive 3 and 4 bedroom residences. Each house provides its residents absolute privacy, while allowing them to be close to nature.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 pl-0 pr-0 pl-mdtp-2 pr-mdtp-2">
                <div class="w-100 float-left">
                    <div class="tab-content" id="v-pills-tabContent">

                        {{----------------------Floor Plans Tab Content 04 Start ---------------------}}
                        <div class="tab-pane fade show active" id="floorplans-tab04" role="tabpanel"
                             aria-labelledby="floorplans-tab">
                            <div class="w-100 float-left floorplans-main">
                                <h4 class="float-left w-100 mobi-show"> Project Plan</h4>
                                <div class="tab-content">

                                    <div id="site-plan" class="tab-pane fade">
                                        <div id="bedroom-1" class="carousel slide" data-ride="carousel">
                                            <!-- Indicators -->
                                            <ul class="carousel-indicators">
                                                <li data-target="#bedroom-1" data-slide-to="0" class="active"></li>
                                            </ul>
                                            <!-- The slideshow -->
                                            <div class="carousel-inner floor_swipe">
                                                <div class="carousel-item active">
                                                    <img src="{{asset('images/floor-plans/site-plan.jpg')}}" alt="site-plan" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        Carefully planned infrastructure with well-designed and tree-lined roads, lush gardens, and amenities meant to pamper the residents and their guests.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="master-plan" class="tab-pane fade active show">
                                        <div id="bedroom-2" class="carousel slide" data-ride="carousel">
                                            <!-- Indicators -->
                                            <ul class="carousel-indicators">
                                                <li data-target="#bedroom-2" data-slide-to="0" class="active"></li>
                                            </ul>
                                            <!-- The slideshow -->
                                            <div class="carousel-inner floor_swipe">
                                                <div class="carousel-item active">
                                                    <img src="{{asset('images/floor-plans/master-plan.jpg')}}" alt="master-plan" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        Nestled in the heart of 300 acres of planned township, the project will be close to fine dining restaurants, multiplexes, cafes’, boutiques and shopping malls, making it the perfect address.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="floor-plan" class="tab-pane fade">
                                        <div id="floor" class="carousel slide" data-ride="carousel">
                                            <!-- Indicators -->
                                            <ul class="carousel-indicators">
                                                <li data-target="#floor" data-slide-to="0" class="active"></li>
                                                <li data-target="#floor" data-slide-to="1"></li>
                                                <li data-target="#floor" data-slide-to="2"></li>
                                                <li data-target="#floor" data-slide-to="3"></li>
                                                <li data-target="#floor" data-slide-to="4"></li>
                                            </ul>
                                            <!-- The slideshow -->
                                            <div class="carousel-inner floor_swipe" id="floor">
                                                <div class="carousel-item active">
                                                    <img src="{{asset('images/floor-plans/3bhk-full.jpg')}}" alt="3bhk-full" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        Exclusive amenities such as indoor swimming pool and gymnasium, in addition to Club Lounge.
                                                    </div>
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{asset('images/floor-plans/earth-villa-lower-full.jpg')}}" alt="earth-villa-lower" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        The exclusive experience of low density living with 4 residences per floor affording greater privacy to its denizens.
                                                    </div>
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{asset('images/floor-plans/earth-villa-upper-full.jpg')}}" alt="earth-villa-upper" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        Exclusive amenities such as indoor swimming pool and gymnasium, in addition to Club Lounge.
                                                    </div>
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{asset('images/floor-plans/sky-villa-lower-full.jpg')}}" alt="sky-villa-lower" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        The exclusive experience of low density living with 4 residences per floor affording greater privacy to its denizens.
                                                    </div>
                                                </div>
                                                <div class="carousel-item">
                                                    <img src="{{asset('images/floor-plans/sky-villa-upper-full.jpg')}}" alt="sky-villa-upper" class="carousel-imgs">
                                                    <div class="carousel-text color-black font12">
                                                        Exclusive amenities such as indoor swimming pool and gymnasium, in addition to Club Lounge.
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Left and right controls -->
                                            <a class="carousel-control-prev prev previous" href="#floor"
                                               data-slide="prev"
                                               id="previous">
                                                <span class="carousel-control-prev-icon"></span>
                                            </a>
                                            <a class="carousel-control-next next" href="#floor" data-slide="next"
                                               id="next">
                                                <span class="carousel-control-next-icon"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <ul class="nav floorplans-tabs">
                                    <li><a data-toggle="tab" href="#master-plan" class="active show"> Master Plan</a></li>
                                    <li><a data-toggle="tab" href="#site-plan">Site Plan</a></li>
                                    <li class="border-right-0"><a data-toggle="tab"  href="#floor-plan">Floor Plan</a></li>
                                </ul>

                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </section>

        <script type="text/javascript">


            $(document).ready(function () {
//                var country;
//                $.getJSON("http://freegeoip.net/json/", function (data) {
//                    country = data.country_name;
//                    var ip = data.ip;
////                    console.log(country);
//                });
//                if(country='India')
//                {
//                    document.getElementById('mobile_code').value = "+91";
//                    document.getElementById('mobile_code').setAttribute("disabled","disabled");
//                    document.getElementById('mobile_code').setAttribute("checked","checked");
//                }
//                else if(country='Dubai')
//                {
//                    document.getElementById('mobile_code').value = "+972";
//                    document.getElementById('mobile_code').setAttribute("disabled","disabled");
//                    document.getElementById('mobile_code').setAttribute("checked","checked");
//
//                }

                $("#download").click();
                $("#project_form").click(function (e) {
                    var first_name = $('#first_name').val();
                    var last_name = $('#last_name').val();
                    var email = $('#email').val();
                    var mobile_code = $('#mobile_code').val();
                    var mobile_number = $('#mobile_number').val();
                    var radio = $('input[type=radio][name=r1]:checked').attr('id');

                    var ua = navigator.userAgent.toLowerCase();
                    var isAndroid = ua.indexOf("android") > -1;
                    var isIphone = ua.indexOf("iphone") > -1;
                    var isIpod = ua.indexOf("ipod") > -1;
                    var isIpad = ua.indexOf("ipad") > -1;
                    var isBB = ua.indexOf("blackberry") > -1;
                    var isBB2 = ua.indexOf("rim") > -1;
                    var isBB = ua.indexOf("blackberry") > -1;
                    var isBB2 = ua.indexOf("RIM") > -1;
                    var isSymbian = ua.indexOf("symbian") > -1;
                    var isNokia = ua.indexOf("nokia") > -1;
                    if (isNokia || isSymbian || isBB || isBB2 || isIphone || isIpod || isIpad || isAndroid) {
                        var src = "Mobile Website";
                    } else
                        src = "Website";

                    var text = "";
                    $.ajax(
                        {
                            url: '{{route('project-form')}}',
                            data: {
                                first_name: first_name,
                                last_name: last_name,
                                email: email,
                                mobile_code: mobile_code,
                                mobile_number: mobile_number,
                                radio: radio,
                                src:src
                            },
                            type: 'GET',
                            success: function (data) {


                                console.log('success', data);
                                if (data.code === 1) {
//                                    for (i = 0; i < data.message.length; i++) {
//                                        text += data.message[i] + "<br>";
//                                    }
                                    toastr.success("Thank you for your interest. Our team will contact you soon.", {timeOut: 5000});
                                    document.getElementById('download').click();
                                    var frm = document.getElementsByName('download_form')[0];
                                    frm.reset();

                                    var link = document.createElement('a');
                                    link.href = url;
                                    link.download = 'file.pdf';
                                    link.dispatchEvent(new MouseEvent('click'));
                                }

                                else {
                                    for (i = 0; i < data.message.length; i++) {
                                        text += data.message[i] + "<br>";
                                    }
                                    toastr.error(text, {timeOut: 5000})
                                    //toastr.error('You Got Error', 'Inconceivable!', {timeOut: 5000})

                                }
                            },
                            error: function (error) {
                                toastr.error('You Got Error', {timeOut: 5000});
//                                console.log('error');
                            }
                        }
                    );
                });
            });


            //code for floor slider


            var start_x = 0;
            $('.floor_swipe').on('dragstart', function (e) {
                start_x = event.clientX;
            }).on('dragend', function (e) {

                var end_x = event.clientX;
                var direction = start_x - end_x;

                if (direction < 0) {
                    $('.previous').trigger('click');
                } else {
                    $('.next').trigger('click');
                }
            });

            $('.floor_swipe').on('touchstart', function (e) {
                start_x = event.changedTouches[0].clientX;
            }).on('touchend', function (e) {

                var end_x = event.changedTouches[0].clientY;
                var direction = start_x - end_x;

                if (direction < 0) {
                    $('.previous').trigger('click');
                } else {
                    $('.next').trigger('click');
                }
            });

        </script>
    </div>
@endsection

@section('scripts')
    <!-- Onload remove register open class Start -->
    <script>
        $(document).ready(function() {
            $('.register-main').removeClass('register-open');
        });
    </script>
    <!-- Onload remove register open class End -->
@endsection