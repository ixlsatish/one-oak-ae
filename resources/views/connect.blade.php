@extends('layouts.app')


@section('content')

    <div class="page-bg connect-bg page-container align-center fadeIn animated delay-0">
        @if (count($errors) > 0)
            <div class="alert alert-danger font-bold print-error-msg" style="text-transform: capitalize;">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                @endforeach
            </div>
        @endif
        <section class="connect-page bg-color-black-op6 pt-4 pr-2 pb-4 center-middle align-items fadeInLeft animated delay-4 mobi">
            {{--<div class="w-100 float-left ">--}}
                <div class="inner-logo text-center position-absolute pr-4">
                    <a href="{{route('index')}}"><img src="{{asset('images/logo2.png')}}"></a>
                </div>
                <div class="col-md-6 pl-0">
                    <h1 class="section-title mt-4 section-pl-25">
                        Connect
                    </h1>
                    <p class="section-pl-25">
                        To find out how 1OAK can enable you to Live More, please complete the form below and we will contact you at our earliest convenience.
                    </p>
                    <p class="section-pl-25">
                        {{--5TC/G5/5  5th Floor<br>--}}
                        {{--Cyber Heights Vibhuti Khand, Gomtinagar <br>Lucknow, Uttar Pradesh 226021 <br>--}}

                        2501, Burlington Tower, Burj Khalifa District, Business Bay P.O.Box 35624 Dubai, UAE <br>
                        Tel: +971 4 5573318 <br>
                        Email: <a href="mailto:contacts@oneoak.ae" class="text-hover color-white">contact@oneoak.ae</a>
                    </p>
                </div>
                <div class="col-md-6 pl-0 pl-mdtp-4">
                    <form data-parsley-validate method="post" id="connect_form" name="connect_form">
                    <div class="w-100">
                        <a href="https://www.google.com/maps/place/Burlington+Tower/@25.1854698,55.2643244,20z/data=!4m8!1m2!2m1!1sBurlington+Tower!3m4!1s0x0:0xfcdbefed68ed67b7!8m2!3d25.185411!4d55.264542" target="_blank">
                            <img src="{{asset('images/map-img.jpg')}}" class="w-100">
                        </a>
                    </div>
                    <div class="w-100 float-left text-left pl-3 pt-3 pb-2 brand-primary text-uppercase">
                        Send Us A Message
                    </div>
                    <div class="w-100 float-left pb-1">
                        <input data-parsley-required="true"
                               data-parsley-maxlength="10"
                               data-parsley-maxlength-message="Name Should Be Less Than 10 Characters"
                               data-parsley-pattern="^[A-Za-z]*$"
                               data-parsley-pattern-message="Enter Characters Only" type="text" class="form-control"
                               placeholder="Full Name"
                               tabindex="1" name="name" id="name">
                    </div>
                    {{--<div class="w-100 float-left pb-1 pr-1 pr-sm-0 pr-sm-1">--}}
                    <div class="w-100 float-left pb-1 pr-0 pr-sm-0">
                        <div class="w-25 float-left styled-select">
                            <input data-parsley-required="true" class="form-control" tabindex="4" id="mobile_code_h" value="+971">
                        </div>
                        <div class="w-75 float-left pl-1">
                        <input data-parsley-required="true"
                               data-parsley-required-message="Please Enter Mobile Number"
                               data-parsley-type="number"
                               data-parsley-type-message="Please Enter Valid Number"
                               data-parsley-minlength="10"
                               data-parsley-minlength-message="Please Enter Valid Mobile Number"
                               data-parsley-maxlength="10"
                               data-parsley-maxlength-message="Please Enter No More Than 10 Digits" type="text"
                               class="form-control" placeholder="Phone Number"
                               tabindex="2" name="mobile_number" id="mobile_number" max="10">
                        </div>
                    </div>
                    {{--<div class="w-50 float-left pb-1">--}}
                        {{--<input data-parsley-required="true"--}}
                               {{--data-parsley-required-message="Please Enter Email Id"--}}
                               {{--data-parsley-type="email"--}}
                               {{--data-parsley-type-message="Please Enter A Valid Email"--}}
                               {{--type="text" class="form-control"--}}
                               {{--placeholder="Your Email ID" tabindex="3" name="email" id="email">--}}
                    {{--</div>--}}
                    <div class="w-100 float-left">
                        <textarea data-parsley-required="true"
                                  data-parsley-maxlength="50"
                                  data-parsley-maxlength-message="Max Length : 50 Characters"
                                  rows="3" cols="0" placeholder="Message" name="message" id="message"></textarea>
                    </div>
                    <div class="w-100 float-left text-right pt-2">
                        {{csrf_field()}}
                        <button class="btn btn-submit" type="button" id="submit_form">
                            Submit
                        </button>
                    </div>
                    </form>
                </div>
            {{--</div>--}}
        </section>
            <script type="text/javascript">


                $(document).ready(function () {
                    $("#submit_form").click(function (e) {
                        $("#submit_form").attr("disabled", "disabled");
                        setTimeout(function() {
                            $("#submit_form").removeAttr("disabled");
                        }, 6000);
                        var name=$('#name').val();

                        var mobile_number=$('#mobile_number').val();
//                        var email=$('#email').val();
                        var message=$('#message').val();

                        var ua = navigator.userAgent.toLowerCase();
                        var isAndroid = ua.indexOf("android") > -1;
                        var isIphone = ua.indexOf("iphone") > -1;
                        var isIpod = ua.indexOf("ipod") > -1;
                        var isIpad = ua.indexOf("ipad") > -1;
                        var isBB = ua.indexOf("blackberry") > -1;
                        var isBB2 = ua.indexOf("rim") > -1;
                        var isBB = ua.indexOf("blackberry") > -1;
                        var isBB2 = ua.indexOf("RIM") > -1;
                        var isSymbian = ua.indexOf("symbian") > -1;
                        var isNokia = ua.indexOf("nokia") > -1;
                        if (isNokia || isSymbian || isBB || isBB2 || isIphone || isIpod||isIpad||isAndroid) {
                            var src="Mobile Website";
                        } else
                             src="Website";
                        var text="";
                        $.ajax(
                            {
                                url: '{{route('submit-form')}}',
//                                data: {name:name,mobile_number:mobile_number,email:email,message:message,src:src},
                                data: {name:name,mobile_number:mobile_number,message:message,src:src},
                                type: 'GET',
                                success: function (data) {
                                    console.log('success',data);
                                    if(data.code==1) {
//                                    for (i = 0; i < data.message.length; i++) {
//                                        text += data.message[i] + "<br>";
//                                    }
                                        toastr.success("Thank you for your interest. Our team will contact you soon.", {timeOut: 5000});
                                        var frm = document.getElementsByName('connect_form')[0];
                                        frm.reset();
                                    }

                                    else
                                    {
                                        for (i = 0; i < data.message.length; i++) {
                                            text+= data.message[i] + "<br>";
                                        }
                                        toastr.error(text,  {timeOut: 5000});
                                        //toastr.error('You Got Error', 'Inconceivable!', {timeOut: 5000})

                                    }
                                },
                                error: function (error) {
                                    toastr.error('You Got Error', {timeOut: 5000});
//                                    console.log('error');
                                }
                            }
                        );
                    });
                });

            </script>
    </div>
@endsection

@section('scripts')
    <!-- Onload remove register open class Start -->
    <script>
        $(document).ready(function() {
            $('.register-main').removeClass('register-open');
        });
    </script>
    <!-- Onload remove register open class End -->
@endsection