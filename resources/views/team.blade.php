@extends('layouts.app')


@section('content')
    <div class="page-bg team-bg page-container align-center fadeIn animated delay-0">
        <section class="connect-page bg-color-black-op6 pt-4 pr-2 pb-4 center-middle align-items fadeInLeft animated delay-4 mobi">
            {{--<div class="w-100 float-left ">--}}
            <div class="inner-logo text-center position-absolute">
                <a href="{{route('index')}}"><img src="{{asset('images/logo2.png')}}"></a>
            </div>
            <div class="col-md-6 pl-0 pr-5">
                <h1 class="section-title mt-4 section-pl-25">
                    Our Team
                </h1>
                <p class="section-pl-25">
                    Meet the One of a Kind team, united in a common goal - to help you Live More, every day. Passionate, dedicated and here to assist and advise, we are proud to introduce you to the people that make us who we are.
                </p>
            </div>
            <div class="col-md-6 pl-4 ouryteam-main border-left mobi">
                <div class="content-scrollbar">
                    <ul class="nav ourteam-tabs mb-3">
                        <li><a data-toggle="tab" href="#management-content" class="active show" id="management-btn">Management Team</a></li>
                        <li><a data-toggle="tab" href="#advisory-content" id="advisory-btn">Advisory Team</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="management-content" class="tab-pane fade active show">
                            <ul align="center" class="management-list">
                                <li class="zoomIn animated project-delay1" id="management-member-01">
                                    <div class="overlap-div">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing.
                                    </div>
                                    <img src="{{asset('images/team/mt-ceo.jpg')}}">
                                    <div class="member-name">
                                        Sandeep Singh Katiyar
                                    </div>
                                </li>
                                <li  class="zoomIn animated project-delay2" id="management-member-02">
                                    <div class="overlap-div">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing.
                                    </div>
                                    <img src="{{asset('images/team/Faisal.jpg')}}">
                                    <div class="member-name">
                                        Faisal Khan
                                    </div>

                                </li>
                                <li  class="zoomIn animated project-delay3" id="management-member-03">
                                    <div class="overlap-div">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing.
                                    </div>
                                    <img src="{{asset('images/team/a-khan.jpg')}}">
                                    <div class="member-name">
                                        Abdullah Khan
                                    </div>
                                </li>
                                {{--<li  class="zoomIn animated project-delay4" id="management-member-04">--}}
                                    {{--<div class="overlap-div">--}}
                                        {{--Lorem ipsum dolor sit amet, consectetur adipisicing.--}}
                                    {{--</div>--}}
                                    {{--<img src="{{asset('images/team/a-amit.jpg')}}">--}}
                                    {{--<div class="member-name">--}}
                                        {{--Amit Tiwari--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                                {{--<li  class="zoomIn animated project-delay4" id="management-member-04">--}}
                                    {{--<div class="overlap-div">--}}
                                        {{--Lorem ipsum dolor sit amet, consectetur adipisicing.--}}
                                    {{--</div>--}}
                                    {{--<img src="{{asset('images/team/sumeet-dikshit.jpg')}}">--}}
                                    {{--<div class="member-name">--}}
                                        {{--Sumeet Dikshit--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                                <li  class="zoomIn animated project-delay5 d-none" id="management-member-05">
                                    <div class="overlap-div">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing.
                                    </div>
                                    <img src="{{asset('images/team/Shailendra.jpg')}}">
                                    <div class="member-name">
                                        Shailendra Jain
                                    </div>
                                </li>
                                <li  class="zoomIn animated project-delay6 d-none" id="management-member-06">
                                    <div class="overlap-div">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing.
                                    </div>
                                    <img src="{{asset('images/team/member-6.jpg')}}">
                                    <div class="member-name">
                                        Full Name
                                    </div>
                                </li>                                
                            </ul>

                            <div class="w-100 float-left pl-2" id="management-details01">
                                <div class="big-photo_with_name">
                                    <div class="w-100 float-left mb-3">
                                        <img src="{{asset('images/team/mt-ceo.jpg')}}" class="w-100">
                                    </div>
                                    <h4> Sandeep Singh Katiyar</h4>
                                    <h5> CEO - INDIA </h5>
                                </div>

                                <div class="photo-content">
                                    <p>
                                        {{--Sandeep has more than two decades of experience in real estate, financial consulting, retail finance, channel sales & management, and venture funding. He has a strong entrepreneurial experience and has played senior leadership and business head roles across diverse businesses including Century 21 India (as CEO), Citigroup (as VP), Café Coffee Day group, Way2Wealth (as COO). At 1OAK (India) Sandeep is involved in the formulation of corporate strategy and management and concentrates on the growth and diversification plans of the company. Sandeep holds an MBA from IBS Hyderabad and is a Member of Royal Institute of Chartered Surveyor (RICS), U.K. A certified Franchise Trainer from Francorp U.K--}}
                                        Sandeep is responsible for the formulation of corporate strategy and management, and the development of the company’s growth and diversification plans. With more than two decades experience in senior leadership and business head roles, including CEO of Century 21 India, VP at Citigroup, COO of Café Coffee Day Group, Sandeep has developed strong entrepreneurial capabilities across a diverse range of business sectors, including real estate, financial consulting, retail finance, channel sales and management, and venture funding.
                                    </p>
                                    <button class="btn goto-management-team-list">
                                        BACK
                                    </button>
                                </div>
                            </div>
                            
                            <div class="w-100 float-left pl-2" id="management-details02">
                                <div class="big-photo_with_name">
                                    <div class="w-100 float-left mb-3">
                                        <img src="{{asset('images/team/Faisal.jpg')}}" class="w-100">
                                    </div>
                                    <h4> Faisal Khan</h4>
                                    <h5> CEO - DUBAI</h5>
                                </div>

                                <div class="photo-content">
                                    <p>
                                        {{--Faisal has over two decades of experience and has strong entrepreneurial skills. Of this the last 13 years have been focused on investing across the Real Estate space. Faisal has been instrumental in building scalable real estate investment platforms as well as in creating portfolios comprising development assets in the residential and hospitality segments. He also takes pride in his vast operational experience and process transformation by taking an end-to-end approach to achieving excellence in execution. At 1OAK (Dubai) Faisal is involved in every aspect of the real estate operations, including development and execution of broad range of corporate activities. Faisal holds a Bachelor degree from the University of Mumbai and also has a Commercial Pilot qualification. He has worked with world's leading Emirates Airlines looking after Customer Affairs and Service audits.--}}
                                        Faisal is involved in every aspect of 1OAK’s real estate operation. He utilises his vast experience complemented by a passion for creating new avenues for growth to drive on-going projects, coordinate stakeholders and identify new opportunities, taking an end-to-end approach to achieving excellence in execution. A fully qualified commercial pilot, for the last 13 years, Faisal has been focused on investing across the real estate industry and has been instrumental in building scalable real estate investment platforms and creating portfolios of development assets in the residential and hospitality segments.
                                    </p>
                                    <button class="btn goto-management-team-list">
                                        BACK
                                    </button>
                                </div>
                            </div>

                            <div class="w-100 float-left pl-2" id="management-details03">
                                <div class="big-photo_with_name">
                                    <div class="w-100 float-left mb-3">
                                        <img src="{{asset('images/team/a-khan.jpg')}}" class="w-100">
                                    </div>
                                    <h4> Abdullah Khan</h4>
                                    <h5> DIRECTOR</h5>
                                </div>

                                <div class="photo-content">
                                    <p>
                                        {{--Abdullah has over a decade of experience in the real estate and property development. He has led the development of several projects in the affordable housing segment and has a vast experience in construction management. He is currently responsible for construction & development related matters of the Company and handling stakeholder relationships.  He is an alma mater of St Francis college Lucknow and holds a Bachelor’s degree in Commerce from Lucknow University.--}}
                                        Abdullah is responsible for the smooth operation of the company’s construction and development interests, as well as management of stakeholder relationships. With more than 10 years’ experience in the real estate, property development and construction management sector, Abdullah has led the development of several projects in the affordable housing segment.
                                    </p>
                                    <button class="btn goto-management-team-list">
                                        BACK
                                    </button>
                                </div>
                            </div>

                            {{--<div class="w-100 float-left pl-2" id="management-details04">--}}
                                {{--<div class="big-photo_with_name">--}}
                                    {{--<div class="w-100 float-left mb-3">--}}
                                        {{--<img src="{{asset('images/team/a-amit.jpg')}}" class="w-100">--}}
                                    {{--</div>--}}
                                    {{--<h4> Amit Tiwari</h4>--}}
                                    {{--<h5> GM - Sales & Marketing</h5>--}}
                                    {{--<h5> Designation</h5>--}}
                                {{--</div>--}}
                                {{--<div class="photo-content">--}}
                                    {{--<p>--}}
                                        {{--Amit's  top priority is customer satisfaction. With over 16 years of experience in sales & marketing in India, Amit has the repute of going above and beyond to keep his clients informed, comfortable and happy throughout the buying process. As the lead for sales & marketing at 1OAK, Amit is responsible for planning and implementing sales, marketing and product development programs keeping in view the customer service & satisfaction. Amit holds a degree in Law from University of Lucknow.--}}
                                        {{--Leading 1OAK’s sales and marketing activities, Amit is responsible for planning and implementing sales, marketing and product development programmes. With over 16 years’ experience in sales and marketing, Amit's top priority is customer satisfaction and he has built a deserved reputation of going above and beyond to keep clients informed, comfortable and happy throughout the purchasing process.--}}
                                    {{--</p>--}}
                                    {{--<button class="btn goto-management-team-list">--}}
                                        {{--BACK--}}
                                    {{--</button>--}}
                                    {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="w-100 float-left pl-2" id="management-details04">--}}
                                {{--<div class="big-photo_with_name">--}}
                                    {{--<div class="w-100 float-left mb-3">--}}
                                        {{--<img src="{{asset('images/team/sumeet-dikshit.jpg')}}" class="w-100">--}}
                                    {{--</div>--}}
                                    {{--<h4> Sumeet Dikshit</h4>--}}
                                    {{--<h5> V.P- Marketing & Strategy</h5>--}}
                                    {{--<h5> Designation</h5>--}}
                                {{--</div>--}}
                                {{--<div class="photo-content">--}}
                                    {{--<p>--}}
                                        {{--Sumeet is the Go-To -Market key resource for  1OAK. Spearheading sales and marketing of real estate projects from scratch for effective reach and conversion is his forte.Additionally,he is responsible for strategy vertical via scouting investment grade realty assets to suit parent FDI fund portfolio.--}}
                                    {{--</p>--}}
                                    {{--<p>--}}
                                        {{--Post graduation from Delhi University & Masters in Retail Management, Sumeet's career contour started with DLF progressing to Business Head-Franchise India Holdings & BLS International Ltd. He has hands-on experience of - retail leasing, Residential/Commercial sales-Domestic/NRI, brokerage solutions, co-working ecosystem setup & aligning stake holder strategic expansion & financial interests.--}}
                                    {{--</p>--}}
                                    {{--<button class="btn goto-management-team-list">--}}
                                        {{--BACK--}}
                                    {{--</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            <div class="w-100 float-left pl-2" id="management-details05">
                                <div class="big-photo_with_name">
                                    <div class="w-100 float-left mb-3">
                                        <img src="{{asset('images/team/Shailendra.jpg')}}" class="w-100">
                                    </div>
                                    <h4> Shailendra Jain</h4>
                                    <h5> Vice President - <br>
                                        Purchase & Admin </h5>
                                </div>
                                <div class="photo-content">
                                    <p>
                                        {{--Shailendra brings in a vast experience of over 30 years in operations (planning & control) & purchases.  He has held leadership positions in various Government undertakings. He was given a National Award, ELCINA in 1992 for indigenous development of Flat Cable Crimping Tools for Electronics Projects.  At 1OAK he is focused on purchase and vendor management thus helping the Company to achieve project timelines. In his role he is also responsible for day-to-day operations and providing strategic direction to the internal processes and systems. Shailendra is an MBA and has earned his bachelor’s degree in Mechanical from IIT.--}}
                                        Shailendra is responsible for day-to-day operations and providing strategic direction for internal processes and systems, as well as purchase and vendor management, to ensure projects are delivered on time and within budget. An ELCINA award winner, Shailendra brings a vast experience of in operational management and purchasing, developed over more than 30 years in leadership positions within various government undertakings.
                                    </p>
                                    <button class="btn goto-management-team-list">
                                        BACK
                                    </button>
                                </div>
                            </div>

                            
                        </div>
                        <div id="advisory-content" class="tab-pane fade">
                            <ul align="center" class="advisory-list">
                                <li class="zoomIn animated project-delay1" id="advisory-member-01">
                                    <div class="overlap-div">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing.
                                    </div>
                                    <img src="{{asset('images/team/a-roy.jpg')}}">
                                    <div class="member-name">
                                        Amritanshu Roy
                                    </div>
                                </li>
                                <li  class="zoomIn animated project-delay2 d-none" id="advisory-member-02">
                                    <div class="overlap-div">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing.
                                    </div>
                                    <img src="{{asset('images/team/member-2.jpg')}}">
                                    <div class="member-name">
                                        Full Name
                                    </div>
                                </li>
                                <li  class="zoomIn animated project-delay3 d-none" id="advisory-member-03">
                                    <div class="overlap-div">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing.
                                    </div>
                                    <img src="{{asset('images/team/Shailendra.jpg')}}">
                                    <div class="member-name">
                                        Shailendra Jain
                                    </div>
                                </li>
                                <li  class="zoomIn animated project-delay4 d-none" id="advisory-member-04">
                                    <div class="overlap-div">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing.
                                    </div>
                                    <img src="{{asset('images/team/member-4.jpg')}}">
                                    <div class="member-name">
                                        Full Name
                                    </div>
                                </li>
                                <li  class="zoomIn animated project-delay5 d-none" id="advisory-member-05">
                                    <div class="overlap-div">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing.
                                    </div>
                                    <img src="{{asset('images/team/member-5.jpg')}}">
                                    <div class="member-name">
                                        Full Name
                                    </div>
                                </li>
                                <li  class="zoomIn animated project-delay6 d-none" id="advisory-member-06">
                                    <div class="overlap-div">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing.
                                    </div>
                                    <img src="{{asset('images/team/member-6.jpg')}}">
                                    <div class="member-name">
                                        Full Name
                                    </div>
                                </li>
                            </ul>

                            <div class="w-100 float-left pl-2" id="advisory-details">
                                <div class="w-100 float-left mb-3">
                                    <img src="{{asset('images/team/member-big-img.jpg')}}" class="w-100">
                                </div>
                                <h4> Full Name</h4>
                                <h5> Designation</h5>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid amet fugiat quisquam similique. Dolorum illum molestiae perspiciatis porro quas rem!
                                </p>
                                <button class="btn goto-advisoryteam-list">
                                    BACK
                                </button>
                            </div>

                            <div class="w-100 float-left pl-2" id="advisory-details01">
                                <div class="big-photo_with_name">
                                    <div class="w-100 float-left mb-3">
                                        <img src="{{asset('images/team/a-roy.jpg')}}" class="w-100">
                                    </div>
                                    <h4> Amritanshu Roy</h4>
                                    <h5>ADVISOR</h5>
                                </div>
                                <div class="photo-content">
                                    <p>
                                        {{--Amritanshu is a real estate advisor and investment specialist . In his previous role, he has managed the India operations of US based REIT - Alexandria Real Estate Equities Inc. and has worked as a senior associate in transactions practice of  Ernst & Young. At 1OAK he advises the team with market strategy practice, building out the relationships and roadmaps to their corporate objectives. Amritanshu is a Sloan Fellow and graduate of Stanford’s Graduate School of Business, and earned his bachelor’s degree in Economics from University of Delhi. He is also holds a degree from the Indian Chartered Financial Analyst Institute.--}}
                                        Amritanshu is a real estate advisor and investment specialist. He advises the members of the 1OAK team on market strategy best practices, building mutually beneficial relationships and the development of roadmaps to corporate objectives. Having previously worked at several high-profile international real estate investment and advisory institutions, Amritanshu brings extensive experience in financial analysis and transactions practice.
                                    </p>
                                    <button class="btn goto-advisoryteam-list">
                                        BACK
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            {{--</div>--}}
        </section>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            if(window.location.href.indexOf("Sandeep") > -1)
            {
// alert("your url contains the name sandeep");
                $('.management-list').hide();
                $("#management-details01").fadeIn();
            }
            if(window.location.href.indexOf("Faisal") > -1)
            {
// alert("your url contains the name sandeep");
                $('.management-list').hide();
                $("#management-details02").fadeIn();
            }
            if(window.location.href.indexOf("Abdullah") > -1)
            {
// alert("your url contains the name sandeep");
                $('.management-list').hide();
                $("#management-details03").fadeIn();
            }
            if(window.location.href.indexOf("Amit") > -1)
            {
// alert("your url contains the name sandeep");
                $('.management-list').hide();
                $("#management-details04").fadeIn();
            }
            if(window.location.href.indexOf("Shailendra") > -1)
            {
// alert("your url contains the name sandeep");
                $('.management-list').hide();
                $("#management-details05").fadeIn();
            }
        });
    </script>

@endsection

@section('scripts')
    <!-- Onload remove register open class Start -->
    <script>
        $(document).ready(function() {
            $('.register-main').removeClass('register-open');
        });
    </script>
    <!-- Onload remove register open class End -->
@endsection