@extends('layouts.app')

@section('content')
    <div class="page-bg joinus-bg page-container align-center fadeIn animated delay-0">
        <section
                class="joinus-page bg-color-black-op6 pt-4 pr-2 pb-4 center-middle align-items fadeInLeft animated delay-4 mobi">
            {{--<div class="w-100 float-left ">--}}
            <div class="inner-logo text-center position-absolute pr-4">
                <a href="{{route('index')}}"><img src="{{asset('images/logo2.png')}}"></a>
            </div>
            <div class="col-md-6 pl-0">
                <h1 class="section-title mt-4 section-pl-25">
                    Join Us
                </h1>
                <p class="section-pl-25">
                    At 1OAK we believe that going the extra mile for our customers is just the beginning, and if you do too, we would like to hear from you.
                </p>
                <p class="section-pl-25">
                    Whether you are an industry expert, or still studying and looking for an internship, if you are as passionate about life as you are about real estate, it’s time we met.
                </p>
            </div>
            <div class="col-md-6 border-left pl-md-4">
                <div class="w-100 float-left joinus-content pl-1">
                    <h2 class="text-uppercase extralight float-left w-100">Seasoned Professionals</h2>
                    <p class="mb-1">UAE</p>
                    <p class="mb-0">1OAK is always looking for seasoned professionals who want a rewarding career within a dynamic international real estate firm.</p>
                    <p>
                        If you have the integrity, passion and drive to be part of a team that is shaping the future of property development, Please click below to apply.
                    </p>
                    <div class="w-100 float-left text-left mb-5">
                        <button class="btn pt-2 pr-5 pb-2 pl-5 show-joinus-form" id="seasoned-btn">
                            Apply
                        </button>
                    </div>

                    <h2 class="text-uppercase extralight float-left w-100">Internship Programme</h2>
                    <p class="mb-1">UAE</p>
                    <p class="mb-0">1OAK offers 10 to 12 week summer internship programmes, developed to enable tomorrow's real estate professionals to cultivate their business skills.</p>
                    <p>
                        If you are in your junior year of college, third year of university or first year of business school and wish to learn from the best,  Please click below to apply.
                    </p>
                    <div class="w-100 float-left text-left">
                        <button class="btn pt-2 pr-5 pb-2 pl-5 show-joinus-form" id="internship-btn">
                            Apply
                        </button>
                    </div>
                </div>
                <div class="joinus-form w-100 float-left">
                    <form method="post" enctype="multipart/form-data" name="join_form" id="join_form">
                        <h2 class="text-uppercase brand-primary extralight pl-3 float-left show-seasoned pl-mdtp-0"
                            style="display: none">Seasoned Professionals Application</h2>
                        <h2 class="text-uppercase brand-primary extralight pl-3 float-left show-internship pl-mdtp-0"
                            style="display: none">Internship Programme Application</h2>
                        <div class="w-100 float-left pb-1">
                            <input data-parsley-required="true"
                                   data-parsley-maxlength="10"
                                   data-parsley-maxlength-message="Name Should Be Less Than 10 Characters"
                                   data-parsley-pattern="^[A-Za-z]*$"
                                   data-parsley-pattern-message="Enter Characters Only" type="text" class="form-control"
                                   placeholder="Full Name" tabindex="1" name="name" id="name">
                        </div>
                        <div class="w-50 float-left pb-1 pr-1 pr-sm-0 pr-sm-1">
                            <div class="w-25 float-left styled-select">
                                <input data-parsley-required="true" class="form-control" tabindex="4" id="mobile_code"
                                       value="+971">
                            </div>
                            <div class="w-75 float-left pl-1">
                                <input data-parsley-required="true"
                                       data-parsley-required-message="Please Enter Mobile Number"
                                       data-parsley-type="number"
                                       data-parsley-type-message="Please Enter Valid Number"
                                       data-parsley-minlength="10"
                                       data-parsley-minlength-message="Please Enter Valid Mobile Number"
                                       data-parsley-maxlength="10"
                                       data-parsley-maxlength-message="Please Enter No More Than 10 Digits" type="text" pattern= "[0-9]"
                                       class="form-control" placeholder="Phone Number" tabindex="3" name="mobile_number"
                                       id="mobile_number" maxlength="10">
                            </div>
                        </div>
                        <div class="w-50 float-left pb-1">
                            <input data-parsley-required="true"
                                   data-parsley-required-message="Please Enter Email Id"
                                   data-parsley-type="email"
                                   data-parsley-type-message="Please Enter A Valid Email" type="text"
                                   class="form-control" placeholder="Your Email ID" tabindex="4" name="email"
                                   id="email">
                        </div>

                        <div class="w-100 float-left pb-1">
                            <div class="input-group  position-relative">
                                {{--<div class="attachment-icon"></div>--}}
                                <label class="input-group-btn p-2 bg-color-white color-black mb-0 font11"
                                       style="color: #6c757d">
                                    Upload File (Optional)
                                </label>
                                <label class="attachment-icon cursor-pointer">
                                    <input type="file" style="display: none;" id="fileUpload">
                                </label>
                                <input type="text" placeholder="pdf file (max size 2mb)" class="form-control pr-5"
                                       readonly>
                            </div>
                        </div>

                        <h2 class="text-uppercase brand-primary extralight pl-3 float-left mb-0 mt-3 pl-mdtp-0">Tell
                            us</h2>
                        <div class="w-100 float-left text-left pl-3 pb-2 brand-primary text-uppercase pl-mdtp-0">
                            why would you like to work with 1OAK
                        </div>
                        <div class="w-100 float-left">
                        <textarea data-parsley-required="true"
                                  data-parsley-maxlength="50" rows="6" cols="0" placeholder="Message"
                                  id="message"></textarea>
                        </div>
                        <div class="w-100 float-left pt-2">
                            {{--<div class="float-left text-decoration text-hover cursor-pointer pt-2" id="back-joinus">--}}
                            {{--Back--}}
                            {{--</div>--}}
                            <button class="btn float-left" id="back-joinus" type="button">
                                BACK
                            </button>
                            {{csrf_field()}}
                            <button class="btn pt-2 pr-5 pb-2 pl-5 float-right" type="button" id="join_us_form">
                                Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
            {{--</div>--}}
        </section>
        <script type="text/javascript">


            $(document).ready(function () {
                $("#join_us_form").click(function (e) {
                    $("#join_us_form").attr("disabled", "disabled");
                    setTimeout(function() {
                        $("#join_us_form").removeAttr("disabled");
                    }, 6000);
                    fsize = 0;
                    var file_upload = $('#fileUpload').val();
//                    alert(file_upload);
                    if (file_upload !== "") {
                        fsize = $('#fileUpload')[0].files[0].size;
                    }

                    if (fsize > 2097152) //do something if file size more than 1 mb (1048576)
                    {
                        toastr.error("Please Upload PDF File (Max Size : 2MB)", {timeOut: 5000});
//                        alert(fsize + " bites\nToo big!");
                    }

                    else {

                        var name = $('#name').val();
                        var mobile_code = $('#mobile_code').val();
                        var mobile_number = $('#mobile_number').val();
                        var email = $('#email').val();
                        var message = $('#message').val();
//                    var file =$('#fileUpload').files;
                        var file = document.getElementById('fileUpload').files[0];
                        console.log(file);
                        var fd = new FormData();
                        fd.append('uploaded_file', file);
                        fd.append('name', name);
                        fd.append('mobile_code', mobile_code);
                        fd.append('mobile_number', mobile_number);
                        fd.append('email', email);
                        fd.append('message', message);
                        console.log(fd);
                        var text = "";
                        $.ajax(
                            {
//                    {name:name,mobile_number:mobile_number,email:email,message:message}
                                url: '{{route('join-us-form')}}',
                                data: fd,
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                success: function (data) {
                                    console.log('success', data);
                                    if (data.code == 1) {
//                                    for (i = 0; i < data.message.length; i++) {
//                                        text += data.message[i] + "<br>";
//                                    }

                                        toastr.success("Thank you for your interest. Our team will contact you soon.", {timeOut: 5000});
                                        var frm = document.getElementsByName('join_form')[0];
                                        frm.reset();
                                    }

                                    else {
                                        for (i = 0; i < data.message.length; i++) {
                                            text += data.message[i] + "<br>";
                                        }
                                        toastr.error(text, {timeOut: 5000})
                                        //toastr.error('You Got Error', 'Inconceivable!', {timeOut: 5000})

                                    }
                                },
                                error: function (error) {
                                    toastr.error('You Got Error',  {timeOut: 5000});
//                                    console.log('error'.error);
                                }
                            }
                        );
                    }
                });
                $("#back-joinus").click(function (e) {
                    var frm = document.getElementsByName('join_form')[0];
                    frm.reset();
                });

            });

        </script>
    </div>
@endsection

@section('scripts')
    <!-- Onload remove register open class Start -->
    <script>
        $(document).ready(function() {
            $('.register-main').removeClass('register-open');
        });
    </script>
    <!-- Onload remove register open class End -->
@endsection


